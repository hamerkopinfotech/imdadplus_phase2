var gmarkers1 = [];
var tempmarkers = [];
var markers1 = [];
var pointers = [];
var breadcrumb = [];
var infowindow = new google.maps.InfoWindow({content: ''});
var bounds = new google.maps.LatLngBounds();
var filters = [];
var markerOpacity = markerOpacityIncrement = 0.05;

var iconBase = '/imdadplus_phase2/img/markers/';
var icons = {
    "country": {
        icon: iconBase + 'marker-blue.png'
    },
    city: {
        icon: iconBase + 'marker-blue.png'
    }
};

//var json = JSON.parse($.getJSON({'url': "/imdadplus_phase2/api/report/", 'async': false}).responseText);
//var json = JSON.parse($.getJSON({'url': "data.php", 'async': false}).responseText);
var json = JSON.parse($.getJSON({'url': "/imdadplus_phase2/json/data.json", 'async': false}).responseText);

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/**
 * Function to init map
 */

function initialize() {
    var mecoord = new google.maps.LatLng(23.8859, 45.0792);
    var center = new google.maps.LatLng(31.7917, 7.0926);
    var mapOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        scrollwheel: false,
        disableDoubleClickZoom: true, // <---
        panControl: false,
        streetViewControl: false,
        // disableDefaultUI: true
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    map.setCenter(mecoord);


    setTimeout(function () {
        map.panTo(mecoord);
        load_markers('gcc', 'gcc');
    }, 2000);
    $('#filterview').hide();
    breadcrumb.push("gcc");

}

goback();

function goback() {
    jQuery("#goback").on("click", function () {
        jQuery(".clinicblue").hide();
        jQuery(".clinicbluekuwait").hide();
        jQuery(".blue").show();
        console.log(breadcrumb);
        console.log('breadcrumb.length'+breadcrumb.length);
        var lentemp = breadcrumb.length;
        if (lentemp > 4) {
            breadcrumb[3] = breadcrumb[lentemp-1];
            breadcrumb = breadcrumb.slice(0, 4);
        }
        $('#myInput').val('');
        switch (breadcrumb.length) {
            case 4:
                delete_markers();
                countryslugback = breadcrumb[1]
                cityslugback = breadcrumb[2]
                var zoom = eval("json.countries." + countryslugback + ".cities." + cityslugback + ".zoom");

                console.log("Back Country Zoom", "From : " + map.getZoom() + " To : " + zoom);

                load_previous_markers("city", cityslugback);


                set_center(eval("json.countries." + countryslugback + ".cities." + cityslugback + ".center_lat") + "," + eval("json.countries." + countryslugback + ".cities." + cityslugback + ".center_lng"));

                breadcrumb.pop();
                jQuery("#city_count").html("");
                break;
            case 3:
                delete_markers();
                countryslugback = breadcrumb[1]

                var zoom = eval("json.markers." + countryslugback + ".zoom");


                console.log("Back Country Zoom", "From : " + map.getZoom() + " To : " + zoom);

                load_previous_markers("country", countryslugback);


                set_center(eval("json.countries." + countryslugback + ".center_lat") + "," + eval("json.countries." + countryslugback + ".center_lng"));

                breadcrumb.pop();
                break;
            case 2:
                delete_markers();
                initialize();

                // center map
                var mecoord = new google.maps.LatLng(23.8859, 45.0792);
                map.setCenter(mecoord);

                jQuery(".mapview-label").html("Middle East");
                jQuery(".mapviewshow-label").html("Middle East");
                breadcrumb = ["gcc"];
                break;
            default:
        }
        check_val();
        
        var str1 = $('.mapview-label').text();
        var str2 = "Kuwait";
        if(str1.indexOf(str2) !== -1) {
            // For device filter area  
            $("#maintab_candela").hide();
            $("#candela").hide();
            $("#device_16-picoplus").show();
            $("#device_06-clarity").show();
            $("#candela").find(".form-check-input"). prop("checked", false);


//            if($("#lutronics").is(":hidden")) {
//              $("#maintab_lutronics > a").click();                 
//            }
            if ($('.tab-pane').is(':visible') == false ) {
               $("#maintab_lutronics > a").click(); 
            }
        } else {
            // For device filter area
            $("#maintab_candela").show();
            $("#device_16-picoplus").hide();
            $("#device_06-clarity").hide();
            $("#16-picoplus"). prop("checked", false);
            $("#06-clarity"). prop("checked", false);
//            if($("#candela").is(":hidden")) {
//                $("#maintab_candela > a").click();
//                $("#candela").show();
//            }
        }
        filterDeviceCall();
    });
}


function check_val() {
    var val = $('#mapview').find('span.mapview-label').text();
    console.log(val);
    if (val === 'Middle East') {
        $('#goback').hide();
    } else {
        $('#goback').show();
    }
}

check_val();
initialize();

// FloatingPopup.populate_popup(json.report.alldevice);
setTimeout(function () {
    FloatingPopup.populate_popup(json.report.alldevice);
}, 1000);


jQuery(function ($) {

    var link = $("ul.imdad-tab li a");
    link.on('click', function (e) {
        e.preventDefault();
        var a = $(this).attr("href");
        $(a).slideToggle('fast');
        $(".imdad-tab-content .tab-pane").not(a).slideUp('fast');
    });


    $('.form-check-input').on('click', function (e) {
        filterDeviceClickCall();
    });

});


function filterDeviceClickCall() {
        filters = [];
        filter_counter = 0;
        var deviceselectedtext = '';
        var devicename, device_id;
        jQuery("input[type='checkbox']:checked").each(function (i) {
            device_id = jQuery(this).val();
            filters.push("device[]=" + device_id);
            filter_counter++;
            var device = $(this).attr("id");
            device = device.split('-');
            var devicefullname = '';
            for (i = 1; i < device.length; i++) {
                devicefullname += device[i] + '-';
            }
            devicefullname = devicefullname + '~@#&@';
            devicefullname = devicefullname.replace("-~@#&@", "");
            devicename = jsUcfirst(devicefullname);
            deviceselectedtext = deviceselectedtext + '' + devicename + ', ';
        });
        if (filter_counter > 1) {
            jQuery(".loading-box").show();
            jQuery(".loading-box-overlay").show();
        }
        if (deviceselectedtext != '') {
            $('#filterview').show();
            deviceselectedtext = deviceselectedtext + '~@#&@';
            deviceselectedtext = deviceselectedtext.replace(", ~@#&@", "");
            $('#devices_selected').text(deviceselectedtext);
        } else {
            $('#devices_selected').text(deviceselectedtext);
            $('#filterview').hide();
        }

        var ajax_url = "/imdadplus_phase2/json/data.json";
        if (filter_counter > 0) {
            if (filter_counter == 1) {
                ajax_url = "/imdadplus_phase2/json/data-" + device_id + ".json";
            } else {
                ajax_url = "/imdadplus_phase2/api/report/?" + filters.join("&");
            }
        }
        
        jQuery.get(ajax_url, function (response) {
            json = response;
            reload_map();
            if (filter_counter > 1) {
                jQuery(".loading-box").hide();
                jQuery(".loading-box-overlay").hide();
            }
        });
}

function filterDeviceCall() {
        filters = [];
        filter_counter = 0;
        var deviceselectedtext = '';
        var devicename, device_id;
        jQuery("input[type='checkbox']:checked").each(function (i) {
            device_id = jQuery(this).val();
            filters.push("device[]=" + device_id);
            filter_counter++;
            var device = $(this).attr("id");
            device = device.split('-');
            var devicefullname = '';
            for (i = 1; i < device.length; i++) {
                devicefullname += device[i] + '-';
            }
            devicefullname = devicefullname + '~@#&@';
            devicefullname = devicefullname.replace("-~@#&@", "");
            devicename = jsUcfirst(devicefullname);
            deviceselectedtext = deviceselectedtext + '' + devicename + ', ';
        });

        if (deviceselectedtext != '') {
            $('#filterview').show();
            deviceselectedtext = deviceselectedtext + '~@#&@';
            deviceselectedtext = deviceselectedtext.replace(", ~@#&@", "");
            $('#devices_selected').text(deviceselectedtext);
        } else {
            $('#devices_selected').text(deviceselectedtext);
            $('#filterview').hide();
        }

}

function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function slugify(string) {
    return string
            .toString()
            .trim()
            .toLowerCase()
            .replace(/\s+/g, "-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");
}
$(function () {

    $("#myInput").autocomplete({
        maxResults: 5,
        source: function (request, response) {
            filters = [];
            filter_counter = 0;
            jQuery("input[type='checkbox']:checked").each(function (i) {
                filters.push("device[]=" + jQuery(this).val());
                filter_counter++;
            });

            var label = $('.mapview-label').html();
            var labelsplit = label.split(",");
            var len = labelsplit.length;
            var availableTags;
            if (len == 1 && labelsplit[0] == 'Middle East') { // region view
                availableTags = JSON.parse($('#clinicdata').val());
            } else if (len == 1 && labelsplit[0] != 'Middle East') { // country view
                labelsplit[0] = slugify(labelsplit[0]);
                available = JSON.parse($('#clinicdatacountry').val());
                availableTags = available[labelsplit[0].toLowerCase().trim()];
            } else if (len == 2) { // city view
                labelsplit[0] = slugify(labelsplit[0]);
                labelsplit[1] = slugify(labelsplit[1]);
                available = JSON.parse($('#clinicdatacountry').val());
                availableTags = available[labelsplit[1].toLowerCase().trim()];
            } else if (len == 3) { // clinic view
                labelsplit[0] = slugify(labelsplit[0]);
                labelsplit[1] = slugify(labelsplit[1]);
                labelsplit[2] = slugify(labelsplit[2]);
                available = JSON.parse($('#clinicdatacountry').val());
                availableTags = available[labelsplit[2].toLowerCase().trim()];
            }
            var results = $.ui.autocomplete.filter(availableTags, request.term.trim());

            response(results.slice(0, this.options.maxResults));
        }
        ,
        select: function (event, ui) {
            var country = document.getElementById('country_' + ui.item.value).value;
            var city = document.getElementById('city_' + ui.item.value).value;
            var clinic = document.getElementById('clinic_' + ui.item.value).value;
            
            $('#market_share').hide().removeClass('col-lg-4').addClass('col-lg-6');
            $('#equipment').removeClass('col-lg-4').addClass('col-lg-6');
            $('#clients').removeClass('col-lg-4').addClass('col-lg-6');
            $('.orange').show();
            $('.red').show();
            $('.light-blue').hide();

            if (country == 'kuwait') {
                $(".clinicbluekuwait").show();
                $(".clinicblue").hide(); 
                // For device filter area  
                $("#maintab_candela").hide();
                $("#candela").hide();
                $("#device_16-picoplus").show();
                $("#device_06-clarity").show();

                $("#candela").find(".form-check-input"). prop("checked", false);                
//                if($("#lutronics").is(":hidden")) {
//                  $("#maintab_lutronics > a").click();                 
//                }
                  if ($('.tab-pane').is(':visible') == false ) {
                     $("#maintab_lutronics > a").click(); 
                  }
            } else {
                $(".clinicbluekuwait").hide();
                $(".clinicblue").show();
                // For device filter area
                $("#maintab_candela").show();
                $("#device_16-picoplus").hide();
                $("#device_06-clarity").hide();
                $("#16-picoplus"). prop("checked", false);
                $("#06-clarity"). prop("checked", false);

//                if($("#candela").is(":hidden")) {
//                    $("#maintab_candela > a").click();
//                    $("#candela").show();
//                }
            }
            filterDeviceCall();
            $(".card.blue").hide();
            
            load_markers_indirect('city', country, city, clinic);
        }
    });
});


