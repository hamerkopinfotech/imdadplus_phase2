function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    _val = n.toString();
    _found = _val.indexOf(".");
    if( _found > -1 ){
        return true;
    }
    return false;
    return Number(n) === n && n % 1 !== 0;
}

FloatingPopup = {

    __containers: [
            'gentle_laster'               ,
            'ultraformer'                 ,
            'spectra'                     ,
            'enCurve'                     ,
            'Infini'                      ,
            'eCo2'                        ,
            'ActionII'                    ,
            'artas'                       ,  
            'lasemd'                      ,
            'picoway'                     ,
            'vbeam'                       ,
            'genius'                   	  ,
            'ultraformer_ku'              ,
            'spectra_ku'                  ,
            'enCurve_ku'                  ,
            'Infini_ku'                   ,
            'eCo2_ku'                     ,
            'ActionII_ku'                 ,
            'artas_ku'                    ,
            'clarity_ku'                  ,
            'lasemd_ku'                   ,
            'picoplus_ku'                 ,
            'genius_ku'                   ,
            'clients'                     ,
            'solutions'                   ,
            'market_share'                ,
            'avg_repair_time_12h_warranty',
            'total_repair_calls_warranty' ,
            'avg_repair_time_12h_all'     ,
            'total_repair_calls_all'      ,
            'first_time_fix_rate'         ,
            'ftfr_total_repair_calls_all' ,
            'up_time'                     ,
            'doctors_trained'             ,
            'nurses_trained'              ,
            'e_learning_course_delivered' ,
            'patient_leads'               ,
            'device_name'                 ,
            'device_id'                   ,             
            'microsite_visitors'          ,             
            'locator_actions'             ,             
            'qualified_leads'             ,
            'facebook_likes'              ,             
            'instagram_likes'             ,
        ],
    // loading report to the floating poup sidebar
    populate_popup: function( creport ){
        console.log ( "Report v2: " , creport );
        jQuery.each(this.__containers,function(i,item){
            console.log();
            if(typeof creport[item] != 'undefined'){
                    console.log(item);
                    console.log( "isFloat for " + item + " : " , isFloat( eval("creport."+item) ));
                    //var __value = ( isFloat( eval( "creport."+item ) ) ) ? parseFloat( eval( "creport."+item ) ).format(1) : parseInt( eval( "creport."+item ) ).format();
                    var __value = ( isFloat( eval( "creport."+item ) ) ) ? parseFloat( eval( "creport."+item ) ).format(1) : eval( "creport."+item );
                jQuery("#"+item + " span").html( __value );
            }

        });
    },

    animate_numbers: function(){
    }
}

//FloatingPopup.populate_popup();






