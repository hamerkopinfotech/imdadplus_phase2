var gmarkers1 = [];
var markers1 = [];
var pointers = [];
var infowindow = new google.maps.InfoWindow({
    content: ''
});
var bounds = new google.maps.LatLngBounds();

// Our markers
markers1 = [
    ['0', 'Cascade Park - 130 Lynn Dr, Hudson, OH 44236', 41.234830, -81.453479], //updated with guesses
    ['1', 'Solon Community Park - 33955 Sherbrook Park Dr, Solon, OH 44139', 41.402313, -81.437936], //updated with guesses
    ['2', 'Middleton Park - 1708 Middleton Rd, Hudson, OH 44236', 41.268963, -81.450649], //updated with guesses
    ['3', 'Silver Springs Park - 5027 Stow Rd Stow, OH 44224', 41.196766, -81.417072], //updated with guesses
    ['4', 'Liberty Park - 9385 Liberty Rd Twinsburg, OH 44087', 41.317218, -81.419225], //updated with guesses
    ['5', 'Sunny Lake Park - 885 E Mennonite Rd Aurora, OH 44202', 41.290323, -81.318730], //updated with guesses
    ['6', 'Aurora Premium Outlets - 549 S Chillicothe Rd, Aurora, OH 44202', 41.301904, -81.341941] //updated with guesses
];


var iconBase = '/img/';
        var icons = {
          "country": {
            icon: iconBase + 'orange-marker.svg'
          },
          city: {
            icon: iconBase + 'orange-marker.svg'
          }
        };

var items = [

[1,1,1,1,0,1,0,1,1,0,0,1,1,0,0,0,1],
[1,1,1,1,1,1,1,0,1,0,1,1,1,0,0,1,0],
[1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,0,1],
[1,1,1,1,1,0,1,0,1,0,1,1,0,0,0,1,1],
[1,1,1,1,0,1,0,1,1,0,0,1,1,0,0,0,1],
[1,1,1,1,0,1,0,1,1,1,0,1,0,0,1,1,1],
[1,1,1,1,0,1,0,1,0,0,0,1,1,1,1,1,0]


];

var json = JSON.parse($.getJSON({'url': "data.php", 'async': false}).responseText);


function loadMarkers( type, slug ){
    switch(type){
        default:
                markers1 = [];
                $.each(json.markers,function(i,item){
                    addLocationMarker(item);
                    pointers.push(item);                    
                });
            break;            
        case 'country':
            markers1 = [];
            $.each(eval("json.countries."+slug+".cities"),function(i,item){
                console.log("Clicked");
                console.log(item.markers);
                addLocationMarker(item.markers);
                pointers.push(item);                    
            });
            break;
        case 'city':
            // markers1 = [];
            // $.each(eval("json.countries."+slug+".cities"),function(i,item){
            //     console.log("Clicked");
            //     console.log(item.markers);
            //     addLocationMarker(item.markers);
            //     pointers.push(item);                    
            // });
            break;            
            
    }
    //console.log(pointers);
}

//[0] show marker
//[1] has playground
//[2] has baby swing 
//[3] has belt swing
//[4] has handicap swing
//[5] toddler friendly
//[6] RF ground
//[7] WC ground
//[8] Pavillion
//[9] Grill
//[10] ramps
//[11] restrooms
//[12] close restrooms
//[13] nursing station
//[14] changing table
//[15] stroller friendly
//[16] walking trails

/**
 * Function to init map
 */

function initialize() {
    var center = new google.maps.LatLng(41.334830, -81.453479);
    var mapOptions = {
        zoom: 4,
        center: center,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDefaultUI: true,
        style: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]

    };
    
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    loadMarkers('gcc','gcc');
    map.fitBounds(bounds);
    //(optional) restore the zoom level after the map is done scaling
    var listener = google.maps.event.addListener(map, "idle", function () {
        map.setZoom(5);
        google.maps.event.removeListener(listener);
    });    
    // for (i = 0; i < pointers.length; i++) {
    //     addMarker(markers1[i]);
    // }
    
    // for (i = 0; i < markers1.length; i++) {
    //     addMarker(markers1[i]);
    // }
}




/**
 * Function to add marker to map
 */

function addLocationMarker(marker) {
    var title = marker.label;
    //console.log(parseFloat(marker.lng));
    var pos = new google.maps.LatLng(parseFloat(marker.lat), parseFloat(marker.lng));
    var content = marker.sku;
    

    var icon = {
        url: icons[marker.type].icon, // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    marker1 = new google.maps.Marker({
        title: title,
        position: pos,
        label: {text: parseInt((Math.random() * 10)).toString(), color: "white"} ,
        icon: icon,
        map: map
    });
    bounds.extend(marker1.position);
    
    marker1['data'] = marker;

    //console.log(marker1.slug);

    gmarkers1.push(marker1);

    //console.log(gmarkers1);
    // Marker click listener
    google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
        return function () {
            console.log(marker1.data);
            console.log(marker1.data.type);
            markers = [];
            loadMarkers(marker1.data.type, marker1.data.slug);
            map.setZoom(9);
            // console.log(this.sku);
            // infowindow.setContent(content);
            // infowindow.open(map, marker1);
            //map.setCenter(new google.maps.LatLng(mylat,mylong));
            map.panTo(this.getPosition());
        }
    })(marker1, content));

    
}


// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < gmarkers1.length; i++) {
        gmarkers1[i].setMap(map);
    }
}


/**
 * Function to add marker to map
 */

function addMarker(marker) {
    var title = marker[1];
    var pos = new google.maps.LatLng(marker[2], marker[3]);
    var content = marker[1];
    
    marker1 = new google.maps.Marker({
        title: title,
        position: pos,
        map: map
    });

    gmarkers1.push(marker1);

    // Marker click listener
    google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
        return function () {
            console.log('Gmarker 1 gets pushed');
            infowindow.setContent(content);
            infowindow.open(map, marker1);
            map.panTo(this.getPosition());
        }
    })(marker1, content));
}

/**
 * Function to filter markers by category
 */


filterMarkersPG = function (categorypg) {

		//set all to view
		for (i = 0; i < markers1.length; i++) {
        items[i][0] = 1;
        marker = gmarkers1[i];
        marker.setVisible(true);
        //window.alert("setting all to yes");
    }

	//import checkbox values
	var PlaygroundCheck = document.getElementById("playground").checked;
    var BabySwingCheck = document.getElementById("babyswing").checked;
    var BeltSwingCheck = document.getElementById("beltswing").checked;
    var HandicapSwingCheck = document.getElementById("handicapswing").checked;
    var ToddlerFriendlyCheck = document.getElementById("toddlerfriendly").checked;
    var RFGroundCheck = document.getElementById("rfground").checked;
    var WCGroundCheck = document.getElementById("wcground").checked;
    var PavillionCheck = document.getElementById("pavillion").checked;
    var GrillCheck = document.getElementById("grill").checked;
    var RampsCheck = document.getElementById("ramps").checked;
    var RestroomsCheck = document.getElementById("restrooms").checked;
    var CloseRestroomsCheck = document.getElementById("closerestrooms").checked;
    var NursingStationCheck = document.getElementById("nursingstation").checked;
    var ChangingTableCheck = document.getElementById("changingtable").checked;
    var StrollerFriendlyCheck = document.getElementById("strollerfriendly").checked;
    var WalkingTrailsCheck = document.getElementById("walkingtrails").checked;
    
    //test that it still works
    //window.alert(PlaygroundCheck);
    //window.alert(BabySwingCheck);
    
    //test if playground checkbox is checked
    if (PlaygroundCheck == true) {    
    
    		//for all sites, loop through playground marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get playground binary
            HasPlayground = items[i][1];
            marker = gmarkers1[i];

						//test if it has a playground
            if (HasPlayground == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of playground check
    
    
    //test if babyswing checkbox is checked
    if (BabySwingCheck == true) {    
    
    		//for all sites, loop through babyswing marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get babyswing binary
            HasBabySwing = items[i][2];
            marker = gmarkers1[i];

						//test if it has a babyswing
            if (HasBabySwing == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of baby swing test
    
    
    //test if beltswing checkbox is checked
    if (BeltSwingCheck == true) {    
    
    		//for all sites, loop through babyswing marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get beltswing binary
            HasBeltSwing = items[i][3];
            marker = gmarkers1[i];

						//test if it has a beltswing
            if (HasBeltSwing == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of belt swing test
    
    //test if handicapswing checkbox is checked
    if (HandicapSwingCheck == true) {    
    
    		//for all sites, loop through babyswing marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get handicapswing binary
            HasHandicapSwing = items[i][4];
            marker = gmarkers1[i];

						//test if it has a handicapswing
            if (HasHandicapSwing == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of handicap swing test
    
    //test if toddlerfriendly checkbox is checked
    if (ToddlerFriendlyCheck == true) {    
    
    		//for all sites, loop through babyswing marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get handicapswing binary
            HasToddlerFriendly = items[i][5];
            marker = gmarkers1[i];

						//test if it has a handicapswing
            if (HasToddlerFriendly == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of ToddlerFriendly swing test
    
    //test if rf ground checkbox is checked
    if (RFGroundCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasRFGround = items[i][6];
            marker = gmarkers1[i];

						//test if it has it
            if (HasRFGround == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of RFGround test
    
    //test if wc ground checkbox is checked
    if (WCGroundCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasWCGround = items[i][7];
            marker = gmarkers1[i];

						//test if it has it
            if (HasWCGround == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of WCGround test
    
    //test if pavillion checkbox is checked
    if (PavillionCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasPavillion = items[i][8];
            marker = gmarkers1[i];

						//test if it has it
            if (HasPavillion == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of pavillion test
    
     //test if grill checkbox is checked
    if (GrillCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasGrill = items[i][9];
            marker = gmarkers1[i];

						//test if it has it
            if (HasGrill == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of grill test
    
     //test if ramp checkbox is checked
    if (RampsCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasRamps = items[i][10];
            marker = gmarkers1[i];

						//test if it has it
            if (HasRamps == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of ramps test
    
     //test if restrooms checkbox is checked
    if (RestroomsCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasRestrooms = items[i][11];
            marker = gmarkers1[i];

						//test if it has it
            if (HasRestrooms == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of restrooms test
    
     //test if close restrooms checkbox is checked
    if (CloseRestroomsCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasCloseRestrooms = items[i][12];
            marker = gmarkers1[i];

						//test if it has it
            if (HasCloseRestrooms == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of close restrooms test
    
    //test if nursingstation checkbox is checked
    if (NursingStationCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasRamps = items[i][13];
            marker = gmarkers1[i];

						//test if it has it
            if (HasRamps == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of ramps test
    
    //test if changing table checkbox is checked
    if (ChangingTableCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasChangingTable = items[i][14];
            marker = gmarkers1[i];

						//test if it has it
            if (HasChangingTable == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of changing table test
    
    //test if stroller friendly checkbox is checked
    if (StrollerFriendlyCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasStrollerFriendly = items[i][15];
            marker = gmarkers1[i];

						//test if it has it
            if (HasStrollerFriendly == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of changing table test
    
    //test if walking trails checkbox is checked
    if (WalkingTrailsCheck == true) {    
    
    		//for all sites, loop through marker
    		for (i = 0; i < markers1.length; i++) {  
        
        	//if it is still enabled (we dont want to reenable a previously disabled)
        	if (items[i][0] == 1){
          	
            //get binary
            HasWalkingTrails = items[i][16];
            marker = gmarkers1[i];

						//test if it has it
            if (HasWalkingTrails == 1){
            		//set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
            		//set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
         	}
          
        }
    
    } //end of walking Trails test
		
    
}


// Init map
initialize();