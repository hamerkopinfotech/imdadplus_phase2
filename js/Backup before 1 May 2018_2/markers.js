
var infoWindow = null;
var defaultJSON = {"installation_base":0,"avg_repair_time":0,"repair_time_24h":0,"calls_solved_12h":0,"fix_rate":0,"spare_parts_hub":0,"doctors_trained":"0","nurses_trained":"0","elearning_courses":0,"campaigns_running":0,"microsite_visitors":"0","clinic_locator_actions":0,"patient_leads":0,"device_name":"","device_id":0,"clients":0,"solutions":"0","market_share":0,"avg_repair_time_12h_warranty":0,"total_repair_calls_warranty":"0","avg_repair_time_12h_all":0,"total_repair_calls_all":"0","first_time_fix_rate":0,"ftfr_total_repair_calls_all":"0","up_time":"0","e_learning_course_delivered":"0","locator_actions":"0","qualified_leads":"0"}
// the smooth zoom function
function smoothZoom (map, max, cnt) {

    
    if (cnt > max) {
        return;
    }    
    else {
        //console.log("Zooming to : " + cnt);
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function(){map.setZoom(cnt)}, 80); 
    }
}  





function smoothZoomOut (map, max, cnt) {
    //console.log("Max : " , max , "Cnt: " , cnt);
    if (cnt < max) {
        return;
    }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            smoothZoom( map, max, cnt - 1 );
        });
        setTimeout(function(){map.setZoom(cnt)}, 60); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}  



function fit_bounds_markers_out(zoom_level){

    //map.fitBounds(bounds);
    //(optional) restore the zoom level after the map is done scaling    
    // var listener = google.maps.event.addListener(map, "idle", function () {
    //     // map.setZoom(zoom_level);        
    //     google.maps.event.removeListener(listener);
    // });  
    // smoothZoomOut(map, zoom_level, map.getZoom());   
    smoothZoomV2(map, zoom_level, map.getZoom(), false);
    
}


function fit_bounds_markers(zoom_level){

    
    //(optional) restore the zoom level after the map is done scaling    
    var listener = google.maps.event.addListener(map, "idle", function () {
        google.maps.event.removeListener(listener);
    });      
    smoothZoom(map, zoom_level, map.getZoom() );   
    //render_markers();
    
}

function set_center(address) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
      }
    })
  }


/**
 * Function to load all markers for country or city from the json file
 * @param string type 
 * @param string  slug 
 */
function load_previous_markers( type, slug, update_breadcrumb ){
    delete_markers();
    
   
    switch(type){
        default:

            // bounds = new google.maps.LatLngBounds();
            // $.each(json.markers,function(i,item){
            //     add_location_marker(item);
            // });                
               

            // // fit_bounds_markers_out(7);
            // //smoothZoomV2(map, 7, map.getZoom(), false);
            // fit_bounds_markers_out( eval("json.countries."+slug+".zoom" ) );

            // // now populate the markers in the zoomed map
            // setTimeout(function(){
            //     render_markers();            
            // },500);
            
            // //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            // //set_center(  "Kingdom of Saudi Arabia" ); // ksa center;
            // // resets the te    mporary markers so we can use it again
            // //reset_temp_markers();



            // console.log("Initialize");
            bounds = new google.maps.LatLngBounds();

            jQuery.each(json.markers,function(i,item){
                add_location_marker(item);
            });    

            // console.log("GCC MARKERS",tempmarkers);
            fit_bounds_markers(6);

            // map.fitBounds(bounds);

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);

            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            set_center(  "Kingdom of Saudi Arabia" ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();
            FloatingPopup.populate_popup(eval("json.report.alldevice")); 

        case 'city':
            bounds = new google.maps.LatLngBounds();


            //console.log("Countries zoom :", eval("json.countries."+slug+".zoom" ));
            fit_bounds_markers_out( eval("json.countries."+breadcrumb[1]+".cities." + slug + ".zoom" ) );
            console.log( "json.clinics."+breadcrumb[1]+".cities." + slug );
            $.each(eval("json.clinics."+breadcrumb[1]+"." + slug),function(i,item){
                //console.log("Country", item.markers);
                add_location_marker(item);

            });

            // load service hubs, asked to hide this for country 1620102
            //load_service_hubs(slug,null);


            

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);
            
            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            //set_center(  eval("json.countries."+slug+".label" ) ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();

            set_center(
                eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lat") + "," + 
                eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lng") 
            );


            FloatingPopup.populate_popup(eval("json.countries."+breadcrumb[1]+".cities."+slug+".report"));
                        
            jQuery("#mapview span.mapview-label").html(
                eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + ", " + 
                eval("json.countries." + breadcrumb[1] + ".label"));
            jQuery("#city_count").html( "<span class='seperator'></span> Cities : " + eval("json.countries." + breadcrumb[1] + ".city_count") );
            // fit_bounds_markers(6);           
            break;            
            
        case 'country':
            bounds = new google.maps.LatLngBounds();


            //console.log("Countries zoom :", eval("json.countries."+slug+".zoom" ));
            fit_bounds_markers_out( eval("json.countries."+slug+".zoom" ) );

            $.each(eval("json.countries."+slug+".cities"),function(i,item){
                //console.log("Country", item.markers);
                add_location_marker(item.markers);

            });

            // load service hubs, asked to hide this for country 1620102
            //load_service_hubs(slug,null);


            

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);
            
            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            //set_center(  eval("json.countries."+slug+".label" ) ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();

            set_center(eval("json.countries."+slug+".center_lat") + "," + eval("json.countries."+slug+".center_lng") );


            FloatingPopup.populate_popup(eval("json.countries."+slug+".report"));
                        
            jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
            jQuery("#city_count").html( "<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count") );
            // fit_bounds_markers(6);           
            break;
        case 'gcc':   

            bounds = new google.maps.LatLngBounds();
            $.each(json.markers,function(i,item){
                add_location_marker(item);
            });                        



            fit_bounds_markers_out( 6 );

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);
            
            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            //set_center(  "Kingdom of Saudi Arabia" ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();


            

            FloatingPopup.populate_popup(eval("json.report"));       
            jQuery("#mapview span.mapview-label").html("Middle East");
            jQuery("#city_count").html( "<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count"));
            break;            
            
    }
    //console.log(breadcrumb);
    //console.log(pointers);
}


function reload_map(){
    //console.log("Load markers");
    //console.log("Reload Breadcurmbs", breadcrumb);

    switch(breadcrumb.length){
        case 4:
            clinicslug = breadcrumb[3];
            breadcrumb.pop();
            load_markers('clinic', clinicslug);
            break;        
        case 3:
            cityslug = breadcrumb[2];
            breadcrumb.pop();
            load_markers('city',cityslug);
            break;
        case 2: 
            countryslug = breadcrumb[1];
            breadcrumb.pop();
            //console.log("Country Slug: ", countryslug);
            //console.log("Breadcrumb After Pop: ", breadcrumb);
            load_markers('country',countryslug);
            break;

        default:
            breadcrumb = ["gcc"];
            load_markers('gcc',"gcc");
    }
}



/**
 * Function to load all markers for country or city from the json file
 * @param string type 
 * @param string  slug 
 */
function load_markers( type, slug, update_breadcrumb ){
    delete_markers();

    var updatebc = true;

    switch(type){
        default:

            // console.log("Initialize");
            bounds = new google.maps.LatLngBounds();

            jQuery.each(json.markers,function(i,item){
                add_location_marker(item);
            });    
            
            // console.log("GCC MARKERS",tempmarkers);
            fit_bounds_markers(6);

            // map.fitBounds(bounds);

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);
            
            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            set_center(  "Kingdom of Saudi Arabia" ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();
            FloatingPopup.populate_popup(eval("json.report.alldevice"));
            jQuery("#city_count").html( "<span class='seperator'></span> Cities : " + eval("json.city_count") );



            break;            
        case 'country':            
            bounds      = new google.maps.LatLngBounds();            
            breadcrumb.push(slug);   
            //console.log("Reset","json.countries."+slug+".cities");                     

            // loading city markers
         

            // loading service hubs for city
            //load_service_hubs(slug,null); 

            // center map based on the available marker area
            

            //console.log( "COUNTRY CENTER >>" , eval("json.countries."+slug+".center_lat") + "," + eval("json.countries."+slug+".center_lng") );
            
            //console.log("set center; : ", eval("json.countries."+slug+".label")); // ksa center;
            // if( slug == "uae" ){
            //     //set_center("United Arab Emirates"); // ksa center;
            //     set_center(eval("json.countries."+slug+".center_lat") + "," + eval("json.countries."+slug+".center_lng") );
            // }else if( slug == "ksa" ){
            //     set_center("Kingdom of Saudi Arabia"); // ksa center;
            // }else{
            //     set_center(eval("json.countries."+slug+".center_lat") + "," + eval("json.countries."+slug+".center_lng") );
            // }
            // 
            try{
                if( "undefined" != typeof eval("json.countries."+slug+".report") ){

                    $.each(eval("json.countries."+slug+".cities"),function(i,item){
                        add_location_marker(item.markers);
                    });   

                    fit_bounds_markers(eval("json.countries."+slug+".zoom"));

                    FloatingPopup.populate_popup(eval("json.countries."+slug+".report"));

                    set_center(eval("json.countries."+slug+".center_lat") + "," + eval("json.countries."+slug+".center_lng") );

                    jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
                    jQuery("#city_count").html( "<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count"));

                }else{
                    FloatingPopup.populate_popup(defaultJSON);           
                }
            }catch(e){
                FloatingPopup.populate_popup(defaultJSON);
            }
            
            

            
            
            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);

            //console.log( "COUNTRY ZOOM >> " , map.getZoom() );
            

            // resets the temporary markers so we can use it again
            //reset_temp_markers();

            // jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label") + "Zoom : " + map.getZoom());
            break;
        case 'city':   

            breadcrumb.push(slug);
            //console.log("Breadcrumbs - City: ",breadcrumb);


            try{
                if( "undefined" != typeof eval("json.countries."+breadcrumb[1]+".cities."+slug+".report") ){
                    FloatingPopup.populate_popup(eval("json.countries."+breadcrumb[1]+".cities."+slug+".report"));       
                }else{
                    FloatingPopup.populate_popup(defaultJSON);           
                }
            }catch(e){
                FloatingPopup.populate_popup(defaultJSON);
            }

            // console.log("json.countries."+breadcrumb[1]+".cities."+slug+".report");
            

            // loading service hubs for city // asked to hide for now. 
            //load_service_hubs(breadcrumb[1], slug);   

            // loading clinics for city
            load_clinics(breadcrumb[1], slug, null);


            try{
                if( "undefined" != typeof eval("json.countries."+breadcrumb[1]+".cities."+slug+".report") ){
                    // bound centered area
                    fit_bounds_markers(eval("json.countries."+breadcrumb[1]+".cities."+slug+".zoom"));
                    //console.log( "CITY CENTER" , eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lat") + ", " + eval("json.countries."+breadcrumb[1]+".center_lng") );
                    //console.log( "CITY LOCATION: ", "json.countries."+breadcrumb[1]+".cities."+slug+".center_lat" );
                    set_center(eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lat") + ", " + eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lng") ); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                }else{
                    FloatingPopup.populate_popup(defaultJSON);    
                }
            }catch(e){
                FloatingPopup.populate_popup(defaultJSON);
            }
            // geoCenter(eval("json.countries."+breadcrumb[1]+".cities."+slug+".label") + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);   
            
            //console.log( "CITY ZOOM >> " , map.getZoom() );
            // now populate the markers in the zoomed map
            //render_markers();            

            // resets the temporary markers so we can use it again
            // reset_temp_markers();

            try{
                if( "undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities."+ slug + ".label") ){
                    jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities."+ slug + ".label") + ", " + eval("json.countries."+breadcrumb[1]+".label"));
                }
            }catch( e ){

            }
            jQuery("#city_count").html("");
            //jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities."+ slug + ".label") + " Zoom: " + map.getZoom());
            break;  
        case 'clinic':   
            console.log( "Map laoding clinic");
            breadcrumb.push(slug);
            //console.log("Breadcrumbs - City: ",breadcrumb);

            console.log("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics."+slug+".report");
            try{
                if( "undefined" != typeof eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics." + slug + ".report") ){
                    FloatingPopup.populate_popup(eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics." + slug + ".report"));       
                }else{
                    FloatingPopup.populate_popup(defaultJSON);           
                }
            }catch(e){
                FloatingPopup.populate_popup(defaultJSON);
            }
            
            //FloatingPopup.populate_popup(eval("json.report"));       

            // loading service hubs for city // asked to hide for now. 
            //load_service_hubs(breadcrumb[1], slug);   

            // loading clinics for city
            console.log( 'Load marker clinic' , slug );
            load_clinics(breadcrumb[1], breadcrumb[2], slug );

            // bound centered area
            // fit_bounds_markers(13); // static for now. 
            try{
                if( "undefined" != eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics." + slug + ".zoom") )   {
                    fit_bounds_markers(eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics." + slug + ".zoom"));        
                    set_center(eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics." + slug + ".center_lat") + ", " + eval("json.countries."+breadcrumb[1]+".cities."+breadcrumb[2]+".clinics."+ slug+ ".center_lng") ); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                }
            }catch(e){
                console.log( e.toString() );
            }
            
            //
            //console.log( "CITY CENTER" , eval("json.countries."+breadcrumb[1]+".cities."+slug+".center_lat") + ", " + eval("json.countries."+breadcrumb[1]+".center_lng") );
            //console.log( "CITY LOCATION: ", "json.countries."+breadcrumb[1]+".cities."+slug+".center_lat" );
            
            // geoCenter(eval("json.countries."+breadcrumb[1]+".cities."+slug+".label") + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;

            // now populate the markers in the zoomed map
            setTimeout(function(){
                render_markers();            
            },500);   
            
            //console.log( "CITY ZOOM >> " , map.getZoom() );
            // now populate the markers in the zoomed map
            //render_markers();            

            // resets the temporary markers so we can use it again
            // reset_temp_markers();

            try{
                if( "undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities."+ breadcrumb[2] + ".clinics."+ slug +".label") ) {
                    jQuery("#mapview span.mapview-label").html(
                        eval("json.countries." + breadcrumb[1] + ".cities."+ breadcrumb[2] + ".clinics."+ slug +".label") + ", "  + 
                        eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + ", " + 
                        eval("json.countries." + breadcrumb[1] + ".label" )
                    );                                                
                }
            }catch(e){
                console.log("Data not found");
            }
            // jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities."+ breadcrumb[2] + ".clinics."+ slug +".label") + ", " + eval("json.countries."+breadcrumb[1]+".label"));
            jQuery("#city_count").html("");
            //jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities."+ slug + ".label") + " Zoom: " + map.getZoom());
            break;                      
            
    }

    //console.log("Updated Breadcurmbs", breadcrumb);
}


function load_service_hubs(country, city){
    closeInfoWindow();
    //console.log("Service Hubs, Country", country, " City: ", city);
    if( country != null && country != "undefined" && country !="" ){
        if( city != null && city != "undefined" && city !="" ){
            //console.log("Service Hubs", "json.service_hubs." + country + "." + city);
            $.each(eval("json.service_hubs." + country + "." + city),function(i,item){
                //console.log( "Service Hub Marker", item );
                add_location_marker(item);
            });

        }else{
            $.each(eval("json.service_hubs." + country),function(i,item){
                $.each(item, function(i,childitem){
                    add_location_marker(childitem);
                });                
            });
        }
    }    
}



function load_clinics(country, city, clinic){

    //console.log("Service Hubs, Country", country, " City: ", city);
    closeInfoWindow();
    if( country != null && country != "undefined" && country !="" ){
        if( city != null && city != "undefined" && city !="" ){
            if( clinic != null && clinic != "undefined" && clinic != "" ){
                try{
                    if( "undefined" != typeof eval("json.countries." + country + ".cities."+ city + ".clinics." + clinic + ".markers") ){
                        add_location_marker(eval("json.countries." + country + ".cities."+ city + ".clinics." + clinic + ".markers"));      
                    }
                }catch(e){
                    console.log("Markers not found");
                }
                
                // $.each(eval("json.countries." + country + ".cities."+ city + ".clinics." + clinic + ".markers"),function(i,item){
                //     console.log( "Clinics Marker", item );
                //     add_location_marker(item);
                // });                                
            }else{
                // city
                $.each(eval("json.clinics." + country + "." + city),function(i,item){
                    //console.log( "Clinics Marker", item );
                    add_location_marker(item);
                });                
            }
        }else{ // do nothing as we don't need to show clinic for country view but only city. 
            // $.each(eval("json.clinics." + country),function(i,item){
            //     $.each(item, function(i,childitem){
            //         add_location_marker(childitem);
            //     });                
            // });
        }
    }    
}

function closeInfoWindow() {
    if (infoWindow !== null) {
        google.maps.event.clearInstanceListeners(infoWindow);  // just in case handlers continue to stick around
        infoWindow.close();
        infoWindow = null;
    }
}

function add_location_marker(marker){
    tempmarkers.push(marker);
    set_map_bounds(marker);
}

function set_map_bounds(marker){
    bounds = new google.maps.LatLngBounds();  
    var pos = new google.maps.LatLng(marker.lat, marker.lng);
    bounds.extend(pos);
}

function reset_temp_markers(){
    tempmarkers = [];
}

/**
 * Function to add marker to map
 */

function render_markers() {
    closeInfoWindow();
    console.log("TEMP MARKERS",tempmarkers);
    for( i = 0; i < tempmarkers.length; i++ ){
        marker = tempmarkers[i];


        // var title = marker.label;
        // console.log(marker.lat, marker.lng);
        var pos = new google.maps.LatLng(marker.lat, marker.lng);
        // var content = '<div style="width:200px;height:50px;text-align:center;"><br /><h4>' + marker.label + '</h4></div>';

        var content = '<div style="width: 300px;text-align: left;"><h4 style="font-size:15px!important;font-weight: bold;line-height: 20px;margin: 25px 0 10px;">' + marker.label + '</h4></div>'
        
        var icon;
        console.log( "Marker type : ", marker.type);
        if( marker.type == "service_hub"){
            icon = {
                url: iconBase + marker.icon, // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0,0), // origin
                anchor: new google.maps.Point(30, 30) // anchor
            };
                
        }
        else if( marker.type == "clinic" || marker.type == "clinic_single" ){
            icon = {
                url: iconBase + 'blue.png', // url
                scaledSize: new google.maps.Size(40, 40), // scaled size
                origin: new google.maps.Point(0,0), // origin
                anchor: new google.maps.Point(30, 30) // anchor
            };
                
        }
        else{
            icon = {
                url: icons[marker.type].icon, // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0,0), // origin
                anchor: new google.maps.Point(30, 30) // anchor
            };
                
        }

        if( marker.type == "service_hub"){
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                icon: icon,
                // animation: google.maps.Animation.DROP,
                map: map
            });
        }else{
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                // label: {text: parseInt((Math.random() * 10)).toString(), color: "white"} ,
                icon: icon,
                // animation: google.maps.Animation.DROP,
                map: map
            });
            
        }
        marker1.setOpacity(0);
        //bounds.extend(marker1.position);
        
        marker1['data'] = marker;

        //console.log(marker1.slug);

        gmarkers1.push(marker1);

        //console.log(gmarkers1);
        // Marker click listener

        if( "clinic_single" == marker.type ){
            console.log("I am here inside clinic single");
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker1);
                    map.panTo(this.getPosition());
                    check_val();  
                }
            })(marker1, content));
        }

        if( "clinic" == marker.type ){
            console.log("I am here inside clinic");

            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {

                    delete_markers();
                    //console.log( "marker1.data.type " , marker1.data.type );
                    load_markers(marker1.data.type, marker1.data.slug);
                    var zoom = 10;
                    if( marker1.data.type == "city"){
                        zoom = 13;
                    }


                    // map.setZoom(zoom)
                    // map.setZoom(parseInt(marker1.data.zoom));
                    //console.log("Test", marker1.data.type);
                    //infowindow.setContent(content);
                    //infowindow.open(map, marker1);
                    //map.setCenter(new google.maps.LatLng(mylat,mylong));
                    //map.fitBounds(bounds);
                    //(optional) restore the zoom level after the map is done scaling
                    //marker1.setMap(map);
                    //map.fitBounds(bounds);
                    map.panTo(this.getPosition());
                    

                    
                    
                    check_val();  
                    
                }
            })(marker1, content));
        }

        if( marker.type != "service_hub" && marker.type != "clinic" && marker.type != "clinic_single"){
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {

                    delete_markers();
                    load_markers(marker1.data.type, marker1.data.slug);
                    var zoom = 10;
                    if( marker1.data.type == "city"){
                        zoom = 13;
                    }
                    // map.setZoom(zoom)
                    // map.setZoom(parseInt(marker1.data.zoom));
                    // console.log(this.sku);
                    // infowindow.setContent(content);
                    // infowindow.open(map, marker1);
                    //map.setCenter(new google.maps.LatLng(mylat,mylong));
                    //map.fitBounds(bounds);
                    //(optional) restore the zoom level after the map is done scaling
                    //marker1.setMap(map);
                    //map.fitBounds(bounds);
                    map.panTo(this.getPosition());
                    

                    
                    
                    check_val();  
                    
                }
            })(marker1, content));
        }
    }
    setTimeout(function() {
        fadeInMarkers(gmarkers1);                
        reset_temp_markers();
    }, 200);    
    
}




// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < gmarkers1.length; i++) {
        gmarkers1[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clear_markers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function show_markers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function delete_markers() {
    clear_markers();
    gmarkers1 = [];
}

google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};



function fadeInMarkers(markers) {

    if (markerOpacity <= 1) {

        for (var i = 0, len = markers.length; i < len; ++i) {
            markers[i].setOpacity(markerOpacity);
        }

        // increment opacity
        markerOpacity += markerOpacityIncrement;

        // call this method again
        setTimeout(function() {
            fadeInMarkers(markers);
        }, 50);

    } else {
        markerOpacity = markerOpacityIncrement; // reset for next use
    }
    
}
