ALTER TABLE `tbl_country` ADD `sitecore_lat` VARCHAR(40) NOT NULL AFTER `country_centre_long`, ADD `sitecore_lng` VARCHAR(40) NOT NULL AFTER `sitecore_lat`, ADD `sitecore_zoom` SMALLINT(2) NOT NULL AFTER `sitecore_lng`;

UPDATE `tbl_country` SET `sitecore_lat` = `Country_Latitude`, `sitecore_lng` = `Country_Longitude`, `sitecore_zoom` = `Country_Zoom`;

ALTER TABLE `tbl_city` ADD `Nearest_city_id` INT(11) NULL DEFAULT NULL AFTER `Country_Id`;

ALTER TABLE `tbl_city` ADD `sitecore_lat` VARCHAR(40) NOT NULL AFTER `city_centre_long`, ADD `sitecore_lng` VARCHAR(40) NOT NULL AFTER `sitecore_lat`, ADD `sitecore_zoom` SMALLINT(2) NOT NULL AFTER `sitecore_lng`;

SELECT * FROM `tbl_city` WHERE `Country_Id` = 1;

UPDATE `tbl_city` SET `Nearest_city_id` = '36' WHERE `tbl_city`.`Country_Id` = 1;
UPDATE `tbl_city` SET `Nearest_city_id` = '47' WHERE `tbl_city`.`Country_Id` = 2;
UPDATE `tbl_city` SET `Nearest_city_id` = '62' WHERE `tbl_city`.`Country_Id` = 3;
UPDATE `tbl_city` SET `Nearest_city_id` = '67' WHERE `tbl_city`.`Country_Id` = 4;
UPDATE `tbl_city` SET `Nearest_city_id` = '69' WHERE `tbl_city`.`Country_Id` = 5;
UPDATE `tbl_city` SET `Nearest_city_id` = '74' WHERE `tbl_city`.`Country_Id` = 6;
UPDATE `tbl_city` SET `Nearest_city_id` = '77' WHERE `tbl_city`.`Country_Id` = 7;
UPDATE `tbl_city` SET `Nearest_city_id` = '82' WHERE `tbl_city`.`Country_Id` = 8;

UPDATE `tbl_city` SET `sitecore_lat` = `City_Latitude`, `sitecore_lng` = `City_Longitude`, `sitecore_zoom` = `City_Zoom`;