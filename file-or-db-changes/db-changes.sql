-- 11/06/2018

ALTER TABLE `tbl_map_report` CHANGE `Clinic_Id` `Clinic_Id` INT(6) NOT NULL COMMENT 'Not Using', CHANGE `Clients` `Clients` INT(6) NOT NULL COMMENT 'Not Using', CHANGE `Solutions` `Solutions` INT(6) NOT NULL COMMENT 'Installation base', CHANGE `Market_Share` `Market_Share` DECIMAL(5,2) NOT NULL COMMENT 'Total Installation base', CHANGE `Avg_Repair_Time_12h_Warranty` `Avg_Repair_Time_12h_Warranty` DECIMAL(5,2) NOT NULL COMMENT 'Under warranty Avergae repair time < 12 hours', CHANGE `Total_Repair_Calls_Warranty` `Total_Repair_Calls_Warranty` INT(6) NOT NULL COMMENT 'Under Warranty Average repair time', CHANGE `Avg_Repair_Time_12h_All` `Avg_Repair_Time_12h_All` DECIMAL(5,2) NOT NULL COMMENT 'Avergae repair time < 12 hours - All', CHANGE `Total_Repair_Calls_All` `Total_Repair_Calls_All` INT(6) NOT NULL COMMENT 'Average repair time - All', CHANGE `First_Time_Fix_Rate` `First_Time_Fix_Rate` DECIMAL(5,2) NOT NULL COMMENT 'First time Call', CHANGE `FTFR_Total_Repair_Calls_All` `FTFR_Total_Repair_Calls_All` INT(6) NOT NULL COMMENT 'Total repair calls All';

ALTER TABLE `tbl_map_report` CHANGE `Market_Share` `Market_Share` INT(6) NOT NULL COMMENT 'Total Installation base', CHANGE `Avg_Repair_Time_12h_Warranty` `Avg_Repair_Time_12h_Warranty` INT(6) NOT NULL COMMENT 'Under warranty Avergae repair time < 12 hours', CHANGE `Avg_Repair_Time_12h_All` `Avg_Repair_Time_12h_All` INT(6) NOT NULL COMMENT 'Avergae repair time < 12 hours - All', CHANGE `First_Time_Fix_Rate` `First_Time_Fix_Rate` INT(6) NOT NULL COMMENT 'First time Call', CHANGE `Up_Time` `Up_Time` INT(6) NOT NULL;

ALTER TABLE `tbl_map_report`
  DROP `Clinic_Id`,
  DROP `Clients`;
  
-- 17/06/2018
  
  ALTER TABLE `tbl_map_report` CHANGE `Report_Id` `Report_Id` INT(6) NOT NULL AUTO_INCREMENT, CHANGE `Solutions` `Installation_Base` INT(6) NOT NULL COMMENT 'Installation base', CHANGE `Market_Share` `Total_Installation_Base` INT(6) NOT NULL COMMENT 'Total Installation base', CHANGE `Avg_Repair_Time_12h_Warranty` `U_W_AVG_Repair_Time_12_h` INT(6) NOT NULL COMMENT 'Under warranty Avergae repair time < 12 hours', CHANGE `Total_Repair_Calls_Warranty` `U_W_AVG_Repair_Time` INT(6) NOT NULL COMMENT 'Under Warranty Average repair time', CHANGE `Avg_Repair_Time_12h_All` `AVG_Repair_Time_12_h` INT(6) NOT NULL COMMENT 'Avergae repair time < 12 hours - All', CHANGE `Total_Repair_Calls_All` `AVG_Repair_Time_All` INT(6) NOT NULL COMMENT 'Average repair time - All', CHANGE `First_Time_Fix_Rate` `First_Time_Call` INT(6) NOT NULL COMMENT 'First time Call', CHANGE `FTFR_Total_Repair_Calls_All` `Total_Repair_Calls_All` INT(6) NOT NULL COMMENT 'Total repair calls All';

-- 18/06/2018

CREATE TABLE `tbl_version` (
  `Version_Id` int(6) NOT NULL,
  `Version` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Updated_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_version`
--

INSERT INTO `tbl_version` (`Version_Id`, `Version`, `Updated_Date`) VALUES
(1, 'Version 1', '2018-06-10'),
(2, 'Version 2', '2018-06-18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_version`
--
ALTER TABLE `tbl_version`
  ADD PRIMARY KEY (`Version_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_version`
--
ALTER TABLE `tbl_version`
  MODIFY `Version_Id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;



------14/04/2019----------------------------
ALTER TABLE `tbl_devices` ADD `clustermap_date` DATETIME NOT NULL AFTER `date`;
----------------------------------------

------16/04/2019----------------------------
ALTER TABLE `tbl_city` ADD `sitecore_lat` VARCHAR(225) NOT NULL AFTER `city_centre_long`;
ALTER TABLE `tbl_city` ADD `sitecore_lng` VARCHAR(225) NOT NULL AFTER `sitecore_lat`;
ALTER TABLE `tbl_city` ADD `sitecore_zoom` VARCHAR(225) NOT NULL AFTER `sitecore_lng`;

UPDATE tbl_city SET sitecore_lat = city_centre_lat;
UPDATE tbl_city SET sitecore_lng = city_centre_long;
UPDATE tbl_city SET sitecore_zoom = City_Zoom;


ALTER TABLE `tbl_country` ADD `sitecore_lat` VARCHAR(225) NOT NULL AFTER `country_centre_long`;
ALTER TABLE `tbl_country` ADD `sitecore_lng` VARCHAR(225) NOT NULL AFTER `sitecore_lat`;
ALTER TABLE `tbl_country` ADD `sitecore_zoom` VARCHAR(225) NOT NULL AFTER `sitecore_lng`;

UPDATE tbl_country SET sitecore_lat = country_centre_lat;
UPDATE tbl_country SET sitecore_lng = country_centre_long;
UPDATE tbl_country SET sitecore_zoom = Country_Zoom;

-------17/04/2019-----------------------------------------------------

ALTER TABLE `tbl_city` ADD `Nearest_city_id` INT NOT NULL DEFAULT '36' AFTER `Country_Id`;
UPDATE `tbl_city` SET `Nearest_city_id`=47 WHERE `Country_Id` = 2;
UPDATE `tbl_city` SET `Nearest_city_id`=62 WHERE `Country_Id` = 3;
UPDATE `tbl_city` SET `Nearest_city_id`=67 WHERE `Country_Id` = 4;
UPDATE `tbl_city` SET `Nearest_city_id`=69 WHERE `Country_Id` = 5;
UPDATE `tbl_city` SET `Nearest_city_id`=74 WHERE `Country_Id` = 6;
UPDATE `tbl_city` SET `Nearest_city_id`=77 WHERE `Country_Id` = 7;
UPDATE `tbl_city` SET `Nearest_city_id`=82 WHERE `Country_Id` = 8;

----------------------------------------------------------------------------------------------






