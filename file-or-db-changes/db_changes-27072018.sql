ALTER TABLE `tbl_map_report_marketing` ADD `City_Id` INT(6) NOT NULL AFTER `Country_Id`;
ALTER TABLE `tbl_map_report_marketing` ADD INDEX(`City_Id`);
ALTER TABLE `tbl_city` ADD `marker_type` ENUM('nothing','office','service_hub','both') NOT NULL AFTER `city_centre_long`;

UPDATE `tbl_country` SET `Country_Latitude` = '23.885942', `Country_Longitude` = '45.079162', `country_centre_lat` = '26.152254', `country_centre_long` = '45.813284' WHERE `tbl_country`.`Country_Id` = 1;
UPDATE `tbl_country` SET `Country_Latitude` = '24.250095', `Country_Longitude` = '54.170351', `country_centre_lat` = '24.250095', `country_centre_long` = '54.170351' WHERE `tbl_country`.`Country_Id` = 2;
UPDATE `tbl_country` SET `Country_Latitude` = '29.499976', `Country_Longitude` = '47.666493', `country_centre_lat` = '29.499976', `country_centre_long` = '47.666493' WHERE `tbl_country`.`Country_Id` = 3;
UPDATE `tbl_country` SET `Country_Latitude` = '25.535394', `Country_Longitude` = '51.655781', `country_centre_lat` = '25.396095022940987', `country_centre_long` = '51.1820672734375' WHERE `tbl_country`.`Country_Id` = 4;
UPDATE `tbl_country` SET `Country_Latitude` = '20.885758', `Country_Longitude` = '56.575469', `country_centre_lat` = '20.991375', `country_centre_long` = '56.18441' WHERE `tbl_country`.`Country_Id` = 5;
UPDATE `tbl_country` SET `Country_Latitude` = '26.118192', `Country_Longitude` = '50.533124', `country_centre_lat` = '26.145472545799883', `country_centre_long` = '50.54629690077968' WHERE `tbl_country`.`Country_Id` = 6;
UPDATE `tbl_country` SET `Country_Latitude` = '31.589357', `Country_Longitude` = '36.885350', `country_centre_lat` = '31.282527755156103', `country_centre_long` = '36.28675094955679' WHERE `tbl_country`.`Country_Id` = 7;
INSERT INTO `tbl_country` (`Country_Id`, `Country_Name`, `Country_Latitude`, `Country_Longitude`, `Country_Zoom`, `country_centre_lat`, `country_centre_long`) VALUES (NULL, 'Lebanon', '33.689210', '35.797435', '8', '33.689210', '35.797435');

UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 44;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 47;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 6;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 25;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 26;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 30;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 36;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 62;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 67;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 69;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 77;

-- By Nishad 05/08/2018

-- Country Coordinates

UPDATE `tbl_country` SET `Country_Latitude` = '23.805942', `Country_Longitude` = '45.509162' WHERE `tbl_country`.`Country_Id` = 1
UPDATE `tbl_country` SET `Country_Latitude` = '23.000095', `Country_Longitude` = '54.600351' WHERE `tbl_country`.`Country_Id` = 2
UPDATE `tbl_country` SET `Country_Latitude` = '29.409976', `Country_Longitude` = '47.806493' WHERE `tbl_country`.`Country_Id` = 3
UPDATE `tbl_country` SET `Country_Latitude` = '24.625394', `Country_Longitude` = '51.405781' WHERE `tbl_country`.`Country_Id` = 4
UPDATE `tbl_country` SET `Country_Latitude` = '20.605758', `Country_Longitude` = '56.805469' WHERE `tbl_country`.`Country_Id` = 5
UPDATE `tbl_country` SET `Country_Latitude` = '26.108192', `Country_Longitude` = '50.803124' WHERE `tbl_country`.`Country_Id` = 6
UPDATE `tbl_country` SET `Country_Latitude` = '30.809357', `Country_Longitude` = '36.925350' WHERE `tbl_country`.`Country_Id` = 7
UPDATE `tbl_country` SET `Country_Latitude` = '33.65978251', `Country_Longitude` = '36.10206026' WHERE `tbl_country`.`Country_Id` = 8

-- Country Coordinates


-- By Nishad 05/08/2018
