
ALTER TABLE `tbl_city` ADD `density_point_1_lat` VARCHAR(50) NOT NULL AFTER `city_centre_long`;
ALTER TABLE `tbl_city` ADD `density_point_1_range` INT NOT NULL AFTER `density_point_1_lat`;
ALTER TABLE `tbl_city` ADD `density_point_2_lat` VARCHAR(50) NOT NULL AFTER `density_point_1_range`;
ALTER TABLE `tbl_city` ADD `density_point_2_range` INT(6) NOT NULL AFTER `density_point_2_lat`;
ALTER TABLE `tbl_city` ADD `density_point_3_lat` VARCHAR(50) NOT NULL AFTER `density_point_2_range`;
ALTER TABLE `tbl_city` ADD `density_point_3_range` INT(6) NOT NULL AFTER `density_point_3_lat`;

ALTER TABLE `tbl_city` ADD `density_point_1_lng` VARCHAR(50) NOT NULL AFTER `density_point_1_lat`;
ALTER TABLE `tbl_city` ADD `density_point_2_lng` VARCHAR(50) NOT NULL AFTER `density_point_2_lat`;
ALTER TABLE `tbl_city` ADD `density_point_3_lng` VARCHAR(50) NOT NULL AFTER `density_point_3_lat`;




ALTER TABLE `tbl_city` ADD `low_density` INT NOT NULL AFTER `marker_type`;
ALTER TABLE `tbl_city` ADD `medium_density` INT NOT NULL AFTER `low_density`;
ALTER TABLE `tbl_city` ADD `high_density` INT NOT NULL AFTER `medium_density`;

UPDATE `tbl_city` SET `high_density` = '50';
UPDATE `tbl_city` SET `medium_density` = '20';
UPDATE `tbl_city` SET `low_density` = '0';

ALTER TABLE `tbl_country` ADD `capital` VARCHAR(255) NOT NULL AFTER `sitecore_zoom`;

UPDATE `tbl_country` SET `capital` = 'Riyadh' WHERE `tbl_country`.`Country_Id` = 1;
UPDATE `tbl_country` SET `capital` = 'Abu Dhabi' WHERE `tbl_country`.`Country_Id` = 2;
UPDATE `tbl_country` SET `capital` = 'Kuwait' WHERE `tbl_country`.`Country_Id` = 3;
UPDATE `tbl_country` SET `capital` = 'Doha' WHERE `tbl_country`.`Country_Id` = 4;
UPDATE `tbl_country` SET `capital` = 'Muscat' WHERE `tbl_country`.`Country_Id` = 5;
UPDATE `tbl_country` SET `capital` = 'Manama' WHERE `tbl_country`.`Country_Id` = 6;
UPDATE `tbl_country` SET `capital` = 'Amman' WHERE `tbl_country`.`Country_Id` = 7;
UPDATE `tbl_country` SET `capital` = 'Beirut' WHERE `tbl_country`.`Country_Id` = 8;