ALTER TABLE `tbl_supplier` ADD `type` ENUM('all','kuwait','both') NOT NULL AFTER `supplier_logo`;
ALTER TABLE `tbl_devices` ADD `type` ENUM('all','kuwait','both') NOT NULL AFTER `device_logo`;
INSERT INTO `tbl_devices` (`Device_Id`, `Supplier_Id`, `Device_Name`, `device_logo`, `type`, `date`) VALUES (NULL, '2', '13- Lasemd', 'lasemd_logo.png', 'both', NULL), (NULL, '2', '14- Picoplus', 'picoplus_logo.png', 'kuwait', NULL);
INSERT INTO `tbl_devices` (`Device_Id`, `Supplier_Id`, `Device_Name`, `device_logo`, `type`, `date`) VALUES (NULL, '1', '15- Picoway', 'picoway_logo.png', 'all', NULL);
INSERT INTO `tbl_devices` (`Device_Id`, `Supplier_Id`, `Device_Name`, `device_logo`, `type`, `date`) VALUES (NULL, '1', '16- VBeam', 'vbeam_logo.png', 'all', NULL);