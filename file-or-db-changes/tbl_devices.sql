-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2019 at 03:25 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imdad_phase2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_devices`
--

CREATE TABLE `tbl_devices` (
  `Device_Id` int(6) NOT NULL,
  `Supplier_Id` int(6) NOT NULL,
  `Device_Name` varchar(100) NOT NULL,
  `device_logo` varchar(20) NOT NULL,
  `type` enum('all','kuwait','both') NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_devices`
--

INSERT INTO `tbl_devices` (`Device_Id`, `Supplier_Id`, `Device_Name`, `device_logo`, `type`, `date`) VALUES
(1, 1, '01- Hair Removal', 'gentle-laster.jpg', 'all', '2019-01-03 14:42:56'),
(2, 2, '02- Spectra', 'Spectra_logo.png', 'both', '2019-01-03 14:43:00'),
(3, 2, '03- Action II', 'ActionII_logo.png', 'both', '2019-01-03 14:43:03'),
(4, 2, '04- eCO2', 'eCo2_logo.png', 'both', '2019-01-03 14:43:05'),
(5, 2, '05- Infini', 'Infini_logo.png', 'both', '2019-01-03 14:43:08'),
(6, 2, '06- Clarity', 'Clarity_logo.png', 'both', '2019-01-03 14:43:10'),
(7, 2, '12- enCurve', 'enCurve_logo.png', 'both', '2019-01-03 14:43:12'),
(8, 3, '09- Artas', 'artas_logo.jpg', 'both', '2019-01-03 14:43:13'),
(9, 4, '08- Ultraformer', 'ultraformer.jpg', 'both', '2019-01-03 14:43:15'),
(10, 2, '13- Lasemd', 'lasemd_logo.png', 'both', '2019-01-11 00:00:00'),
(11, 2, '14- Picoplus', 'picoplus_logo.png', 'kuwait', '2019-01-11 00:00:00'),
(12, 1, '15- Picoway', 'picoway_logo.png', 'all', '2019-01-11 00:00:00'),
(13, 1, '16- VBeam', 'vbeam_logo.png', 'all', '2019-01-11 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_devices`
--
ALTER TABLE `tbl_devices`
  ADD PRIMARY KEY (`Device_Id`),
  ADD KEY `Supplier_Id` (`Supplier_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_devices`
--
ALTER TABLE `tbl_devices`
  MODIFY `Device_Id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
