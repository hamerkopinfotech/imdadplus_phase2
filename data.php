<?php
header('Content-Type: application/json');
    $countries = array(
        "label"     => "GCC",
        "report"    => array( 
                                "alldevice"=>array("installation_base"=>"5000","clients"=>"1150","visit_patients_website"=>"2516000","clinic_locator_actions"=>"11000","cities_served"=>"81","repair_time_less_than_24h"=>"83","first_titme_fix_rate"=>"90","warehouses"=>"12","doctors_trained"=>"13000","nurses_trained"=>"3000", "elearning_delivered"=>"1200","market_share"=>"90","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                                "device1"=> array("installation_base"=>"5000","clients"=>"1150","visit_patients_website"=>"2516000","clinic_locator_actions"=>"11000","cities_served"=>"81","repair_time_less_than_24h"=>"83","first_titme_fix_rate"=>"90","warehouses"=>"12","doctors_trained"=>"13000","nurses_trained"=>"8000","elearning_delivered"=>"1200","market_share"=>"60","uptime"=>"99","leads"=>"180","marketshare"=>"300"), 
                                "device2"=> array("installation_base"=>"5000","clients"=>"1150","visit_patients_website"=>"2516000","clinic_locator_actions"=>"11000","cities_served"=>"81","repair_time_less_than_24h"=>"83","first_titme_fix_rate"=>"90","warehouses"=>"12","doctors_trained"=>"12000","nurses_trained"=>"3000", "elearning_delivered"=>"1200","market_share"=>"70","uptime"=>"99","leads"=>"180","marketshare"=>"300"), 
                                "device3"=> array("installation_base"=>"5000","clients"=>"1150","visit_patients_website"=>"2516000","clinic_locator_actions"=>"11000","cities_served"=>"81","repair_time_less_than_24h"=>"83","first_titme_fix_rate"=>"90","warehouses"=>"12","doctors_trained"=>"16000","nurses_trained"=>"7000", "elearning_delivered"=>"1200","market_share"=>"80","uptime"=>"99","leads"=>"180","marketshare"=>"300")),
        "markers"   => array(
                "ksa"   => array("lat"=>23.8859,"lng"=>45.0792,"label"=>"Kingdom of Saudi Arabia","slug"=>"ksa", "type"=>"country", "zoom"=>7),
                "uae"   => array("lat"=>25.1941384,"lng"=>55.2736491,"label"=>"United Arab Emirates","slug"=>"uae","type"=>"country", "zoom"=>7),
                "kuwait"=> array("lat"=>29.3279245,"lng"=>47.6757891,"label"=>"Kuwait","slug"=>"kuwait","type"=>"country", "zoom"=>7),
                "jordan"=> array("lat"=>30.5852,"lng"=>36.2384,"label"=>"Jordan","slug"=>"jordan","type"=>"country", "zoom"=>7),
                "oman"=> array("lat"=>21.4735,"lng"=>55.9754,"label"=>"Oman","slug"=>"oman","type"=>"country", "zoom"=>7)
        ),
        "zoom" => 4,
        "service_hubs"=> array( 
                "ksa"=>array( 
                            "riyadh" => array( 
                                array("label"=>"Service Hub 1","lat"=>"24.4016221","lng"=>"45.411795","icon"=>"orange.png","type"=>"service_hub"),
                                array("label"=>"Service Hub 2","lat"=>"26.0856153","lng"=>"43.9067724","icon"=>"orange.png","type"=>"service_hub"),
                                array("label"=>"Service Hub 3","lat"=>"25.8373709","lng"=>"43.4344462","icon"=>"orange.png","type"=>"service_hub"),
                                array("label"=>"Service Hub 4","lat"=>"27.5261061","lng"=>"41.5347497","icon"=>"orange.png","type"=>"service_hub")  
                            )
                         ),
                "uae"=>array( 
                    "abudhabi" => array( 
                        array("label"=>"Service Hub 1","lat"=>"24.3870789","lng"=>"54.4185366","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 2","lat"=>"24.4422567","lng"=>"54.5050539","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 3","lat"=>"24.506431","lng"=>"54.4143309","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 4","lat"=>"24.4209426","lng"=>"54.6326842","icon"=>"blue.png","type"=>"service_hub")  
                    ),
                    "dubai" => array( 
                        array("label"=>"Service Hub 5","lat"=>"25.2146","lng"=>"55.3033","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 6","lat"=>"25.1514","lng"=>"55.2508","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 7","lat"=>"25.0838","lng"=>"55.2204","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 8","lat"=>"25.2788","lng"=>"55.3309","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 8","lat"=>"24.9857","lng"=>"55.0273","icon"=>"blue.png","type"=>"service_hub")  
                    ),
                    "sharjah" => array( 
                        array("label"=>"Service Hub 9","lat"=>"25.317634","lng"=>"55.4405673","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 10","lat"=>"25.317634","lng"=>"55.4405673","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 11","lat"=>"25.317634","lng"=>"55.4405673","icon"=>"blue.png","type"=>"service_hub"),
                    ),
                    "alain" => array( 
                        array("label"=>"Service Hub 9","lat"=>"24.1342","lng"=>"55.8283","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 10","lat"=>"24.1332","lng"=>"55.8043","icon"=>"blue.png","type"=>"service_hub"),
                        array("label"=>"Service Hub 11","lat"=>"24.1322","lng"=>"55.8073","icon"=>"blue.png","type"=>"service_hub"),
                    )

                    
                    
                ),
                         
        ),
        "countries" => array(
                "ksa" => array(
                        "label"=>"Kingdom of Saudi Arabia",
                        "zoom" => 7,
                        "report"=>array("installation_base"=>"6000","clients"=>"1350","visit_patients_website"=>"3516000","clinic_locator_actions"=>"13000","cities_served"=>"81","repair_time_less_than_24h"=>"43","first_titme_fix_rate"=>"30","warehouses"=>"16","doctors_trained"=>"13000","nurses_trained"=>"3000","elearning_delivered"=>"1400","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                        "cities" => array(
                            "riyadh" => array( 
                                    "zoom" => 9,
                                    "label"=>"Riyadh", 
                                    "report"=>array("installation_base"=>"2,000+","clients"=>"850","visit_patients_website"=>"1,516,000","clinic_locator_actions"=>"7,000+","cities_served"=>"52","repair_time_less_than_24h"=>"83%","first_titme_fix_rate"=>"93%","warehouses"=>"10","doctors_nurses"=>"10,000+","elearning_delivered"=>"1100", "market_share"=>"80","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                                    "markers"=>array( "lat"=> 24.7136, "lng" => 46.6753, "slug"=>"riyadh", "type"=>"city", "label"=>"Riyadh" ), 
                                    "data"=>array(
                                            "device1" => array(
                                                "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                        array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                        array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                        array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                        array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                        array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                        array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                    )
                                                ),
                                                "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce"),
                                                "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                                "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                                "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                                "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                                "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                                "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                                "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" )
                                            ), // device 1 end
                                            "device2" => array(
                                                "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                                "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                                "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                                "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                                "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                                "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                                "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                                "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                                "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" )
                                            ), // device 2 end 
                                            "device3" => array(
                                                "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                                "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                                "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                                "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                                "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                                "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                                "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                                "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                                "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" )
                                            ) // device 3 end 
                                        ) // city data end                                                                                                
                            ) //end city                                                                                                    
                        ) // end cities
                ),
                "uae" => array(
                    "label"=>"United Arab Emirates",
                    "zoom" => 7,
                    "report"=>array("installation_base"=>"3000","clients"=>"1850","visit_patients_website"=>"2315000","clinic_locator_actions"=>"9000","cities_served"=>"72","repair_time_less_than_24h"=>"87","first_titme_fix_rate"=>"91","warehouses"=>"17","doctors_nurses"=>"15000","doctor_nurses_split"=>"(6,000 dcotors, 9,000 nurses)","elearning_delivered"=>"1600","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                    "cities" => array(
                        "dubai" => array( 
                                "label"=>"Dubai", 
                                "zoom" => 9,
                                "markers"=>array( "lat"=> "25.2048", "lng" => "55.2708", "slug"=>"dubai", "type"=>"city", "label"=>"Dubai" ), 
                                "report"=>array("installation_base"=>"2000","clients"=>"1350","visit_patients_website"=>"315000","clinic_locator_actions"=>"3000","cities_served"=>"12","repair_time_less_than_24h"=>"37","first_titme_fix_rate"=>"21","warehouses"=>"7","doctors_trained"=>"3000","nurses_trained"=>"3000","elearning_delivered"=>"600","marketshare"=>"300"),
                                "data"=>array(
                                        "device1" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce"),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 60
                                        ), // device 1 end
                                        "device2" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 30
                                        ), // device 2 end 
                                        "device3" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 40
                                        ) // device 3 end 
                                    ) // city data end                                                                                                
                        ), //end city                                                                                                    
                        "sharjah" => array( 
                            "label"=>"Sharjah", 
                            "zoom" => 9,
                            "markers"=>array( "lat"=> "25.317634", "lng" => "55.4405675", "slug"=>"dubai", "type"=>"city", "label"=>"Sharjah" ), 
                            "report"=>array("installation_base"=>"2000","clients"=>"1350","visit_patients_website"=>"315000","clinic_locator_actions"=>"3000","cities_served"=>"12","repair_time_less_than_24h"=>"37","first_titme_fix_rate"=>"21","warehouses"=>"7","doctors_trained"=>"3000","nurses_trained"=>"3000","elearning_delivered"=>"600","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                            "data"=>array(
                                    "device1" => array(
                                        "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                            )
                                         ),
                                        "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                        "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                        "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                        "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                        "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                        "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                        "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                        "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                        "total"                         => 60
                                    ), // device 1 end
                                    "device2" => array(
                                        "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                        "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                        "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                        "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                        "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                        "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                        "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                        "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                        "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                        "total"                         => 30
                                    ), // device 2 end 
                                    "device3" => array(
                                        "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                        "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                        "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                        "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                        "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                        "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                        "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                        "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                        "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                        "total"                         => 40
                                    ) // device 3 end 
                                ) // city data end                                                                                                
                            ), // end city   
                            "abudhabi" => array( 
                                "label"=>"Abudhabi", 
                                "zoom" => 9,
                                "markers"=>array( "lat"=> "24.4539", "lng" => "54.3773", "slug"=>"abudhabi", "type"=>"city", "label"=>"Abudhabi" ), 
                                "report"=>array("installation_base"=>"2000","clients"=>"1350","visit_patients_website"=>"315000","clinic_locator_actions"=>"3000","cities_served"=>"12","repair_time_less_than_24h"=>"37","first_titme_fix_rate"=>"21","warehouses"=>"7","doctors_trained"=>"3000","nurses_trained"=>"3000","elearning_delivered"=>"600","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                                "data"=>array(
                                        "device1" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                    array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                    array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                    array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                    array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                    array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                    array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                )
                                             ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 60
                                        ), // device 1 end
                                        "device2" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 30
                                        ), // device 2 end 
                                        "device3" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 40
                                        ) // device 3 end 
                                    ) // city data end                                                                                                
                            ), // end city   
                            "alain" => array( 
                                "label"=>"Al Ain", 
                                "zoom" => 9,
                                "markers"=>array( "lat"=> "24.1302", "lng" => "55.8023", "slug"=>"alain", "type"=>"city", "label"=>"Al Ain" ), 
                                "report"=>array("installation_base"=>"2000","clients"=>"1350","visit_patients_website"=>"315000","clinic_locator_actions"=>"3000","cities_served"=>"12","repair_time_less_than_24h"=>"37","first_titme_fix_rate"=>"21","warehouses"=>"7","doctors_trained"=>"3000","nurses_trained"=>"3000","elearning_delivered"=>"600","marketshare"=>"300"),
                                "data"=>array(
                                        "device1" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce"),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 60
                                        ), // device 1 end
                                        "device2" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 30
                                        ), // device 2 end 
                                        "device3" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 40
                                        ) // device 3 end 
                                    ) // city data end 
                           ),
                        "rasalkhaimah" => array( 
                                "label"=>"Ras Al Khaimah", 
                                "zoom" => 9,
                                "markers"=>array( "lat"=> "25.6741", "lng" => "55.9804", "slug"=>"rasalkhaimah", "type"=>"city", "label"=>"Ras Al Khaimah" ), 
                                "report"=>array("installation_base"=>"3200","clients"=>"3350","visit_patients_website"=>"115000","clinic_locator_actions"=>"2000","cities_served"=>"11","repair_time_less_than_24h"=>"27","first_titme_fix_rate"=>"12","warehouses"=>"7","doctors_trained"=>"2000","nurses_trained"=>"2000","elearning_delivered"=>"600","marketshare"=>"400"),
                                "data"=>array(
                                        "device1" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce"),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 60
                                        ), // device 1 end
                                        "device2" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 30
                                        ), // device 2 end 
                                        "device3" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce" ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 40
                                        ) // device 3 end 
                                    ) // city data end 
                           )                             
                    ) // end cities

                ),
                "kuwait" => array(
                    "label"=>"Kuwait",
                    "zoom" => 7,
                    "report"=>array("installation_base"=>"4000","clients"=>"1850","visit_patients_website"=>"3315000","clinic_locator_actions"=>"11000","cities_served"=>"62","repair_time_less_than_24h"=>"97","first_titme_fix_rate"=>"96","warehouses"=>"27","doctors_trained"=>"13000","nurses_trained"=>"3000","elearning_delivered"=>"1900","uptime"=>"99","leads"=>"180","marketshare"=>"300"),
                    "cities" => array(
                        "alahmadi" => array( 
                                "label"=>"Al Ahmadi", 
                                "markers"=>array( "lat"=> 25.2048, "lng" => 55.2708, "slug"=>"dubai", "type"=>"city", "label"=>"Dubai" ), 
                                "data"=>array(
                                        "device1" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                    array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                    array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                    array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                    array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                    array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                    array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                )
                                            ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                    array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                    array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                    array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                    array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                    array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                    array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                )
                                             ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 40
                                        ), // device 1 end
                                        "device2" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                    array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                    array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                    array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                    array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                    array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                    array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                )
                                             ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 30
                                        ), // device 2 end 
                                        "device3" => array(
                                            "installation_base"             => array( "label" => "Installation Base", "value" =>30, "datasource" => "Salesforce", "markers"=>array(                                                 
                                                    array("lat"=>"26.094088", "lng"=>"43.973454", "text"=>"InterContinental Hotel, Riyadh"),
                                                    array("lat"=>"24.66646", "lng"=>"46.691358", "text"=> "Four Seasons Hotel, Riyadh"),
                                                    array("lat"=>"24.711477", "lng"=>"46.674765", "text"=> "Marhaba Banquet Hall, Riyadh"),   
                                                    array("lat"=>"24.67665", "lng"=>"46.69187", "text" => "Al Faisaliah Hotel, King Fahad Road, Olaya, Riyadh"),  
                                                    array("lat"=>"24.674303", "lng"=>"46.715942", "text"=>"Riyadh Exhibition Center (Old), Murooj area, Olaya St , Riyadh"),  
                                                    array("lat"=>"24.690267", "lng"=>"46.684813", "text"=>"Riyadh International Convention & Exhibition Cente")
                                                )
                                             ),
                                            "average_repair_time"           => array( "label" => "Average Repair Time", "value" =>2, "datasource" => "Salesforce" ),
                                            "repair_time_less_than_24h"     => array( "label" => "Repair Time in Less Than 24h", "value" =>3, "datasource" => "Salesforce" ),
                                            "emergency_calls_solved_12h"    => array( "label" => "Emergency calls solved in less than 12h", "value" =>2, "datasource" => "Salesforce" ),
                                            "first_time_fix_rate"           => array( "label" => "First time fix rate", "value" =>5, "datasource" => "Salesforce" ),
                                            "spare_parts_hubs"              => array( "label" => "Spare Parts Hub", "value" =>5, "datasource" => "Salesforce" ),
                                            "doctors_trained"               => array( "label" => "Doctors Trained", "value" =>15, "datasource" => "Salesforce" ),
                                            "nurses_trained"                => array( "label" => "Nurses Trained", "value" =>7, "datasource" => "Salesforce" ),
                                            "elearning_courses_delivered"   => array( "label" => "E-Learning Courses Delivered", "value" =>7, "datasource" => "Salesforce" ),
                                            "total"                         => 20
                                        ) // device 3 end 
                                    ) // city data end                                                                                                
                        ) //end city                                                                                                    
                    ) // end cities
                )
        )

);


print_r(json_encode($countries));
exit;