<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:03 AM
 */

class Clinics_model extends CI_Model
{
    
    /**
     * Get all clinics data based on filter
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getAll( $filters = array() ){

        $sql = "SELECT  cl.Clinic_Id, cl.Clinic_Name,ct.City_Name,cr.Country_Name FROM tbl_clinics as cl join tbl_city as ct on cl.City_Id = ct.City_Id join tbl_country as cr on cl.Country_Id = cr.Country_Id";
        $query = $this->db->query( $sql );

        // echo $sql;die;

        return $query->result();
    }

    public function isAvailable($val, $code){
        
        $result_id = 0;
        $sql = "SELECT Clinic_id FROM tbl_clinics WHERE Clinic_Code = \"".$code."\"";
        
        $query = $this->db->query($sql);

        $result = $query->result();
        $result_id = (count($result) > 0)?$result[0]->Clinic_id:0;
        
        return $result_id;
        
    }
    
    public function getAllClinics(){

        $sql = "SELECT c.Clinic_Id, c.Clinic_Name FROM tbl_clinics AS c";
        $query = $this->db->query( $sql );
		$result = $query->result();
		
		$clinic_arr = array();
		foreach($result as $val_clinic) {
			$clinic_arr[strtolower($val_clinic->Clinic_Name)] = $val_clinic->Clinic_Id;
		}

        return $clinic_arr;
    }
    
    public function isClinicExist($clinic_name, $all_clinics) {
		$clinic_id = 0;
		$clinic_name = strtolower($clinic_name);
        if (array_key_exists($clinic_name, $all_clinics)) {
			$clinic_id = $all_clinics[$clinic_name];
		}
        
        return $clinic_id;
        
   }
   
       public function getLatLng($city_id) {
        $sql = '';
        $details = array();
        if (isset($city_id)) {
        $sql = "SELECT  cl.Clinic_Id as id, cl.Clinic_Latitude as lat, cl.Clinic_Longitude as lng, ct.city_centre_lat as center_lat, ct.city_centre_long as center_lng, ct.sitecore_zoom as zoom "
                . "FROM tbl_clinics as cl join tbl_city as ct on cl.City_Id = ct.City_Id WHERE cl.City_id = $city_id";
        //echo $sql; 
        $query = $this->db->query( $sql );
        }
        
        if ($sql == '') {
            return $details;
        }

        $result = $query->result();


        if (count($result)) {
            foreach ($result as $det) {
                $details[] = array(
                   // 'id' => $det->id,
                    'lat' => $det->lat,
                    'lng' => $det->lng,
                    'zoom' => $det->zoom,
                    'center_lat' => $det->center_lat,
                    'center_lng' => $det->center_lng
                );
            }
        }

        return $details;
    } 
   
   
   
}