<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:22 AM
 */
class ME_model extends CI_Model {

    /**
     * Columns that needs to be ignored in reports i.e. while fetching from tbl_map_report
     * @var array
     */
    protected $ignore_report_cols = array('Report_Id', 'Country_Id', 'City_Id', 'Device_Id');

    /**
     * Default report payload values
     * @var array
     */
    protected $default_report_payload = array();

    public function __construct() {

        // set default payload values
        $this->setDefaultReportPayload();
        $this->report_arr = $this->report_arr_mkt = $this->report_arr_up = array();
    }

    public function isAvailable($val) {
        $device = array();
        $result_id = 0;
//        $device = explode("-",$val);
//        $val = trim($device[0]);
        $sql = "SELECT Device_id FROM tbl_devices WHERE Device_Name LIKE \"%" . $val . "%\"";
        $query = $this->db->query($sql);

        $result = $query->result();
        $result_id = (count($result) > 0) ? $result[0]->Device_id : 0;

        return $result_id;
    }

    public function insertClinicsAndDevice($excel_data) {

        $i = $j = 0;
        $sql_ins_device = array();
        foreach ($excel_data as $val_clinic) {
            if ($val_clinic['clinic'] != "") {
                $clinic_count = $this->getClinicCountByName($val_clinic['clinic']);
                if ($clinic_count > 0) {
                   $city_name = $this->City_model->getCityName($val_clinic['city']);
                   $val_clinic['clinic'] = $val_clinic['clinic']." | ".$city_name; 
                }
                $sql_ins_clinic = "INSERT INTO tbl_clinics (Country_id, City_id, Clinic_Code, Clinic_Name, Clinic_Latitude, Clinic_Longitude) VALUES
				({$val_clinic['country']}, {$val_clinic['city']}, '{$val_clinic['clinic_code']}', '{$val_clinic['clinic']}', '{$val_clinic['latitude']}', '{$val_clinic['longitude']}')";
                $this->db->query($sql_ins_clinic);
                $clinic_id_insert = $this->db->insert_id();

                foreach ($val_clinic['device'] as $val_device) {
                    $sql_ins_device[$j][] = "($clinic_id_insert, $val_device), ";

                    $i++;
                    if ($i % 400 == 0) {
                        $j++;
                    }
                }
            }
        }

        foreach ($sql_ins_device as $val_ins) {
            $sql_ins_qry = "INSERT INTO tbl_device_install (Clinic_id, Device_id) VALUES ";
            foreach ($val_ins as $val_ins_inner) {
                $sql_ins_qry .= $val_ins_inner;
            }
            $sql_ins_qry = $sql_ins_qry . "~~~";
            $sql_ins_qry = str_replace(", ~~~", "", $sql_ins_qry);

            $this->db->query($sql_ins_qry);
        }
    }

    public function insertClinicsAndDeviceInstall($contents) {
        $initContent = $contents['clinic'];
        $contents['clinic'] = array_unique($contents['clinic']);
        $clinicidArray = array();
        foreach ($initContent as $k => $v) {
            $country_id = (isset($contents['country'][$k]) && $contents['country'][$k] != "") ? $contents['country'][$k] : 0;
            $city_id = (isset($contents['city'][$k]) && $contents['city'][$k] != "") ? $contents['city'][$k] : 0;
            $clinic_name = (isset($initContent[$k]) && $initContent[$k] != "") ? $initContent[$k] : 0;
            $Device_id = (isset($contents['devices'][$k]) && $contents['devices'][$k] != "") ? $contents['devices'][$k] : 0;
            $latitude = (isset($contents['latitude'][$k]) && $contents['latitude'][$k] != "") ? $contents['latitude'][$k] : 0;
            $longitude = (isset($contents['longitude'][$k]) && $contents['longitude'][$k] != "") ? $contents['longitude'][$k] : 0;

            if (isset($contents['clinic'][$k])) {
                $clinic_name = str_replace('"', '', $clinic_name);
                $clinic_name = str_replace("'", "", $clinic_name);
                $sql = "INSERT INTO tbl_clinics (Country_id, City_id, Clinic_Name, Clinic_Latitude, Clinic_Longitude) VALUES ($country_id, $city_id, \"" . $clinic_name . "\", $latitude, $longitude)";
                $query = $this->db->query($sql);
                $clinicidArray[$clinic_name] = $this->db->insert_id();
            }

            /* $sql = "SELECT Clinic_id FROM tbl_clinics WHERE Clinic_Name LIKE '".$clinic_name."'";
              $query = $this->db->query($sql);

              $result = $query->result();
              $clinic_id = $result[0]->Clinic_id; */

            /* $this->db->select('Clinic_id');
              $this->db->from('tbl_clinics');
              $this->db->like('Clinic_Name', $clinic_name);
              $result = $this->db->get()->result_array();
              $clinic_id = $result[0]->Clinic_id; */

//            $result = $this->db->select('Clinic_id')->from('tbl_clinics')
//            ->where("Clinic_Name LIKE '%$clinic_name%'")->get()->result_array();
//            echo '<pre>';
//                print_r($result);
//            exit;
            if (isset($clinicidArray[$clinic_name])) {
                $clinic_id = $clinicidArray[$clinic_name];

                $sql = "INSERT INTO tbl_device_install (Clinic_id, Device_id) VALUES ($clinic_id, $Device_id)";
                $query = $this->db->query($sql);
            }
        }
    }

    public function insertClinics($contents) {
        $contents['clinic'] = array_unique($contents['clinic']);

        foreach ($contents['clinic'] as $k => $v) {
            $country_id = (isset($contents['country'][$k]) && $contents['country'][$k] != "") ? $contents['country'][$k] : 0;
            $city_id = (isset($contents['city'][$k]) && $contents['city'][$k] != "") ? $contents['city'][$k] : 0;
            $clinic_name = (isset($contents['clinic'][$k]) && $contents['clinic'][$k] != "") ? $contents['clinic'][$k] : 0;
            $latitude = (isset($contents['latitude'][$k]) && $contents['latitude'][$k] != "") ? $contents['latitude'][$k] : 0;
            $longitude = (isset($contents['longitude'][$k]) && $contents['longitude'][$k] != "") ? $contents['longitude'][$k] : 0;

            $sql = "INSERT INTO tbl_clinics (Country_id, City_id, Clinic_Name, Clinic_Latitude, Clinic_Longitude) VALUES ($country_id, $city_id, \"" . $clinic_name . "\", $latitude, $longitude)";
            $query = $this->db->query($sql);
        }
    }

    public function insertDeviceInstall($contents) {
        $clinic_id = 0;

        foreach ($contents['devices'] as $k => $v) {
            $Device_id = (isset($contents['devices'][$k]) && $contents['devices'][$k] != "") ? $contents['devices'][$k] : 0;
            $clinic_name = (isset($contents['clinic'][$k]) && $contents['clinic'][$k] != "") ? $contents['clinic'][$k] : 0;

            $sql = "SELECT Clinic_id FROM tbl_clinics WHERE Clinic_Name LIKE \"" . $clinic_name . "\"";
            $query = $this->db->query($sql);

            $result = $query->result();
            $clinic_id = $result[0]->Clinic_id;

            $sql = "INSERT INTO tbl_device_install (Clinic_id, Device_id) VALUES ($clinic_id, $Device_id)";
            $query = $this->db->query($sql);
        }
    }

    public function insertMapReport($contents) {

        foreach ($contents['country'] as $j => $v) {

            $country = (isset($contents['country'][$j]) && $contents['country'][$j] != "") ? $contents['country'][$j] : 0;
            $city = (isset($contents['city'][$j]) && $contents['city'][$j] != "") ? $contents['city'][$j] : 0;
            $device = (isset($contents['devices'][$j]) && $contents['devices'][$j] != "") ? $contents['devices'][$j] : 0;
            $installation_base = (isset($contents['installation_base'][$j]) && $contents['installation_base'][$j] != "") ? $contents['installation_base'][$j] : 0;
            $total_installation_base = (isset($contents['total_installation_base'][$j]) && $contents['total_installation_base'][$j] != "") ? $contents['total_installation_base'][$j] : 0;
            $U_W_AVG_Repair_Time_12_h = (isset($contents['U_W_AVG_Repair_Time_12_h'][$j]) && $contents['U_W_AVG_Repair_Time_12_h'][$j] != "") ? $contents['U_W_AVG_Repair_Time_12_h'][$j] : 0;
            $U_W_AVG_Repair_Time = (isset($contents['U_W_AVG_Repair_Time'][$j]) && $contents['U_W_AVG_Repair_Time'][$j] != "") ? $contents['U_W_AVG_Repair_Time'][$j] : 0;
            $AVG_Repair_Time_12_h = (isset($contents['AVG_Repair_Time_12_h'][$j]) && $contents['AVG_Repair_Time_12_h'][$j] != "") ? $contents['AVG_Repair_Time_12_h'][$j] : 0;
            $AVG_Repair_Time_All = (isset($contents['AVG_Repair_Time_All'][$j]) && $contents['AVG_Repair_Time_All'][$j] != "") ? $contents['AVG_Repair_Time_All'][$j] : 0;
            $First_Time_Call = (isset($contents['First_Time_Call'][$j]) && $contents['First_Time_Call'][$j] != "") ? $contents['First_Time_Call'][$j] : 0;
            $Total_Repair_Calls_All = (isset($contents['Total_Repair_Calls_All'][$j]) && $contents['Total_Repair_Calls_All'][$j] != "") ? $contents['Total_Repair_Calls_All'][$j] : 0;
            $Up_Time = (isset($contents['Up_Time'][$j]) && $contents['Up_Time'][$j] != "") ? $contents['Up_Time'][$j] : 0;
            $Doctors_Trained = (isset($contents['Doctors_Trained'][$j]) && $contents['Doctors_Trained'][$j] != "") ? $contents['Doctors_Trained'][$j] : 0;
            $Nurses_Trained = (isset($contents['Nurses_Trained'][$j]) && $contents['Nurses_Trained'][$j] != "") ? $contents['Nurses_Trained'][$j] : 0;
            $E_Learning_Course_Delivered = (isset($contents['E_Learning_Course_Delivered'][$j]) && $contents['E_Learning_Course_Delivered'][$j] != "") ? $contents['E_Learning_Course_Delivered'][$j] : 0;


            $sql = "INSERT INTO tbl_map_report (Country_Id, City_Id, Device_Id, Installation_Base, Total_Installation_Base, U_W_AVG_Repair_Time_12_h, U_W_AVG_Repair_Time, AVG_Repair_Time_12_h, AVG_Repair_Time_All, First_Time_Call, Total_Repair_Calls_All, Up_Time, Doctors_Trained, Nurses_Trained, E_Learning_Course_Delivered)"
                    . "VALUES ($country, $city, $device, $installation_base, $total_installation_base, $U_W_AVG_Repair_Time_12_h, $U_W_AVG_Repair_Time, $AVG_Repair_Time_12_h, $AVG_Repair_Time_All, $First_Time_Call, $Total_Repair_Calls_All, $Up_Time, $Doctors_Trained, $Nurses_Trained, $E_Learning_Course_Delivered)";

            $query = $this->db->query($sql);
        }
    }

    public function insertDeviceCount($contents) {

        foreach ($contents['quantity'] as $j => $v) {

            $clinic = (isset($contents['clinic'][$j]) && $contents['clinic'][$j] != "") ? $contents['clinic'][$j] : 0;
            $device = (isset($contents['devices'][$j]) && $contents['devices'][$j] != "") ? $contents['devices'][$j] : 0;
            $quantity = (isset($contents['quantity'][$j]) && $contents['quantity'][$j] != "") ? $contents['quantity'][$j] : 0;
            $U_W_AVG_Repair_Time_12_h = (isset($contents['U_W_AVG_Repair_Time_12_h'][$j]) && $contents['U_W_AVG_Repair_Time_12_h'][$j] != "") ? $contents['U_W_AVG_Repair_Time_12_h'][$j] : 0;
            $U_W_AVG_Repair_Time = (isset($contents['U_W_AVG_Repair_Time'][$j]) && $contents['U_W_AVG_Repair_Time'][$j] != "") ? $contents['U_W_AVG_Repair_Time'][$j] : 0;
            $AVG_Repair_Time_12_h = (isset($contents['AVG_Repair_Time_12_h'][$j]) && $contents['AVG_Repair_Time_12_h'][$j] != "") ? $contents['AVG_Repair_Time_12_h'][$j] : 0;
            $AVG_Repair_Time_All = (isset($contents['AVG_Repair_Time_All'][$j]) && $contents['AVG_Repair_Time_All'][$j] != "") ? $contents['AVG_Repair_Time_All'][$j] : 0;
            $First_Time_Call = (isset($contents['First_Time_Call'][$j]) && $contents['First_Time_Call'][$j] != "") ? $contents['First_Time_Call'][$j] : 0;
            $Total_Repair_Calls_All = (isset($contents['Total_Repair_Calls_All'][$j]) && $contents['Total_Repair_Calls_All'][$j] != "") ? $contents['Total_Repair_Calls_All'][$j] : 0;
            $Up_Time = (isset($contents['Up_Time'][$j]) && $contents['Up_Time'][$j] != "") ? $contents['Up_Time'][$j] : 0;
            $Doctors_Trained = (isset($contents['Doctors_Trained'][$j]) && $contents['Doctors_Trained'][$j] != "") ? $contents['Doctors_Trained'][$j] : 0;
            $Nurses_Trained = (isset($contents['Nurses_Trained'][$j]) && $contents['Nurses_Trained'][$j] != "") ? $contents['Nurses_Trained'][$j] : 0;
            $E_Learning_Course_Delivered = (isset($contents['E_Learning_Course_Delivered'][$j]) && $contents['E_Learning_Course_Delivered'][$j] != "") ? $contents['E_Learning_Course_Delivered'][$j] : 0;

            $sql = "INSERT INTO  tbl_clinic_device (Clinic_Id, Device_Id, Quantity, U_W_AVG_Repair_Time_12_h, U_W_AVG_Repair_Time, AVG_Repair_Time_12_h, AVG_Repair_Time_All, First_Time_Call, Total_Repair_Calls_All, Up_Time, Doctors_Trained, Nurses_Trained, E_Learning_Course_Delivered)"
                    . "VALUES ($clinic, $device, $quantity, $U_W_AVG_Repair_Time_12_h, $U_W_AVG_Repair_Time, $AVG_Repair_Time_12_h, $AVG_Repair_Time_All, $First_Time_Call, $Total_Repair_Calls_All, $Up_Time, $Doctors_Trained, $Nurses_Trained, $E_Learning_Course_Delivered)";

            $query = $this->db->query($sql);
        }
    }

    public function insertReportMarketing($contents) {
        foreach ($contents['country'] as $j => $v) {

            $country = (isset($contents['country'][$j]) && $contents['country'][$j] != "") ? $contents['country'][$j] : 0;
            $device = (isset($contents['devices'][$j]) && $contents['devices'][$j] != "") ? $contents['devices'][$j] : 0;
            $Microsite_Visitors = (isset($contents['Microsite_Visitors'][$j]) && $contents['Microsite_Visitors'][$j] != "") ? $contents['Microsite_Visitors'][$j] : 0;
//            $Locator_Actions = (isset($contents['Locator_Actions'][$j]) && $contents['Locator_Actions'][$j] != "") ? $contents['Locator_Actions'][$j] : 0;
//            $Qualified_Leads = (isset($contents['Qualified_Leads'][$j]) && $contents['Qualified_Leads'][$j] != "") ? $contents['Qualified_Leads'][$j] : 0;


//            $sql = "INSERT INTO  tbl_map_report_marketing (Country_Id, Device_Id, Microsite_Visitors, Locator_Actions, Qualified_Leads)"
//                    . "VALUES ($country, $device, $Microsite_Visitors, $Locator_Actions, $Qualified_Leads)";
            $sql = "INSERT INTO  tbl_map_report_marketing (Country_Id, Device_Id, Microsite_Visitors)"
                    . "VALUES ($country, $device, $Microsite_Visitors)";            

            $query = $this->db->query($sql);
        }
    }

    public function clearClinicDeviceInstall() {
        $sql = "TRUNCATE TABLE tbl_clinics";
        $query = $this->db->query($sql);
        $sql = "TRUNCATE TABLE tbl_device_install";
        $query = $this->db->query($sql);
    }

    public function clearMapReport() {
        $sql = "TRUNCATE TABLE tbl_map_report";
        $query = $this->db->query($sql);
    }

    public function clearReportMarketing() {
        $sql = "TRUNCATE TABLE tbl_map_report_marketing";
        $query = $this->db->query($sql);
    }
    public function clearReportSocialMedia() {
        $sql = "TRUNCATE TABLE tbl_map_report_socialmedia";
        $query = $this->db->query($sql);
    }
    public function clearDeviceCount() {
        $sql = "TRUNCATE TABLE tbl_clinic_device";
        $query = $this->db->query($sql);
    }

    public function getClinicCount() {
        $sql = "SELECT * FROM tbl_clinics";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }
    
    public function getClinicCountByName($clinic) {
        $sql = "SELECT * FROM tbl_clinics where Clinic_Name = '".$clinic."'";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }

    public function getClinicDeviceInstallCount() {
        $sql = "SELECT * FROM tbl_device_install";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }

    public function getMapReportCount() {
        $sql = "SELECT * FROM tbl_map_report";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }
    
    public function getMapReportSocialMediaCount() {
        $sql = "SELECT * FROM tbl_map_report_socialmedia";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }

    public function getDeviceCount() {
        $sql = "SELECT * FROM tbl_clinic_device";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }

    public function getReportMarketingCount() {
        $sql = "SELECT * FROM tbl_map_report_marketing";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }
    
   public function getReportSocialMediaCount() {
        $sql = "SELECT * FROM tbl_map_report_socialmedia";
        $query = $this->db->query($sql);
        $result = $query->result();
        $total = count($result);
        return $total;
    }

    public function updateVersionDate() {

        $date = date("Y-m-d");
        $sql = "SELECT `Version_Id`,`Updated_Date` FROM `tbl_version` ORDER BY `Updated_Date` DESC LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->result();
        $Updated_Date = (count($result) > 0) ? $result[0]->Updated_Date : 0;
        $Version_Id = (count($result) > 0) ? $result[0]->Version_Id : 0;

        if ($date > $Updated_Date) {
            $sql = "UPDATE tbl_version SET Updated_Date = '$date' WHERE Version_Id = $Version_Id";
            $query = $this->db->query($sql);
        }
    }

    /**
     * Find the summary report from database
     *
     * @param array $data Summary filter type e.g. array( 'country_id' => 10, 'city_id' => 5, 'device_id' => array(1,2,3) );
     * @return array
     */
    public function getDeviceSummaryReport($data = array()) {

        set_time_limit(0);

        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;
        $clinic_id = isset($data['clinic_id']) ? (int) $data['clinic_id'] : 0;
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        $where = array();

        if ($country_id > 0) {
            $where[] = 'Country_Id = ' . $country_id;
        }

        if ($city_id) {
            $where[] = 'City_Id = ' . $city_id;
        }

//        if ($clinic_id > 0) {
//            $where[] = 'Clinic_Id = ' . $clinic_id;
//        }

        if (is_array($device_id) && count($device_id) > 0) {
            $where[] = 'Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $clinics = $this->getUniqueClinics($where, $clinic_id);

        // set default payload value to 0
        $payload = $this->default_report_payload;

        $payload['clients'] = number_format(count($clinics));

        $arr_idx = strtolower(str_replace(" ", "_", $where));
        $arr_idx = strtolower(str_replace("=", "_", $arr_idx)) . "_";
//		echo $arr_idx."<br>";

        if (array_key_exists($arr_idx, $this->report_arr)) {
            $result = $this->report_arr[$arr_idx];
            $result_uptime = $this->report_arr_up[$arr_idx];
        } else {

            $sql = "SELECT sum(Installation_Base) as Solutions,sum(Total_Installation_Base) as Market_Share,"
                    . " sum(U_W_AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_Warranty, sum(U_W_AVG_Repair_Time) as Total_Repair_Calls_Warranty,"
                    . " sum(AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_All, sum(AVG_Repair_Time_All) as Total_Repair_Calls_All,"
                    . " sum(First_Time_Call) as First_Time_Fix_Rate, sum(Total_Repair_Calls_All) as FTFR_Total_Repair_Calls_All, sum(Doctors_Trained) as Doctors_Trained,"
                    . " sum(Nurses_Trained) as Nurses_Trained, sum(E_Learning_Course_Delivered) as E_Learning_Course_Delivered"
                    . " FROM tbl_map_report $where";

//        	echo $sql."<br>";
//        	die;

            $query = $this->db->query($sql);

            $result = $query->result();

//			echo "<pre>";print_r($result);

            $this->report_arr[$arr_idx] = $result;

            $where_up = $where ? $where . " AND Up_Time > 0" : "WHERE Up_Time > 0";
            $sql_uptime = "SELECT sum(Up_Time) as Up_Time, sum(8760 - Up_Time) as Down_Time FROM tbl_map_report $where_up";

//        	echo $sql."<br>";
//        	die;

            $query_uptime = $this->db->query($sql_uptime);

            $result_uptime = $query_uptime->result();

//			echo "<pre>";print_r($result_uptime);

            $this->report_arr_up[$arr_idx] = $result_uptime;
            
                    $socialmediasql = "SELECT sum(facebook_likes) as facebook_likes,sum(instagram_likes) as instagram_likes
                            FROM tbl_map_report_socialmedia where device_id = $device_id";
        }

        /* Set social media likes value in inner pages like clinics and countries */
        $socialmediaresult = array();
        if($device_id > 0) { 
            $socialmediasql = "SELECT sum(facebook_likes) as facebook_likes,sum(instagram_likes) as instagram_likes
                    FROM tbl_map_report_socialmedia where device_id = $device_id";
            $socialmediaquery = $this->db->query($socialmediasql);

            $socialmediaresult = $socialmediaquery->result();      

        } else {
            $socialmediasql = "SELECT sum(facebook_likes) as facebook_likes,sum(instagram_likes) as instagram_likes
                    FROM tbl_map_report_socialmedia";
            $socialmediaquery = $this->db->query($socialmediasql);

            $socialmediaresult = $socialmediaquery->result(); 
        }
        /* Set social media likes value in inner pages like clinics and countries */
        if (count($result)) {

            foreach ($result as $k => $data) {
//                echo "<pre>";print_r($data);
//                die;

                $payload['solutions'] = $data->Solutions ? number_format($data->Solutions) : 0;
                $payload['market_share'] = 0;
                if ($data->Market_Share > 0) {
                    $payload['market_share'] = round(($data->Solutions / $data->Market_Share) * 100);
                }

                $payload['total_repair_calls_warranty'] = $data->Total_Repair_Calls_Warranty ? number_format($data->Total_Repair_Calls_Warranty) : 0;

                $payload['avg_repair_time_12h_warranty'] = 0;
                if ($data->Total_Repair_Calls_Warranty > 0) {
                    $payload['avg_repair_time_12h_warranty'] = round(($data->Avg_Repair_Time_12h_Warranty / $data->Total_Repair_Calls_Warranty) * 100);
                }

                $payload['total_repair_calls_all'] = $data->Total_Repair_Calls_All ? number_format($data->Total_Repair_Calls_All) : 0;

                $payload['avg_repair_time_12h_all'] = 0;
                if ($data->Total_Repair_Calls_All > 0) {
                    $payload['avg_repair_time_12h_all'] = round(($data->Avg_Repair_Time_12h_All / $data->Total_Repair_Calls_All) * 100);
                }

                $payload['ftfr_total_repair_calls_all'] = $data->FTFR_Total_Repair_Calls_All ? number_format($data->FTFR_Total_Repair_Calls_All) : 0;

                $payload['first_time_fix_rate'] = 0;
                if ($data->FTFR_Total_Repair_Calls_All > 0) {
                    $payload['first_time_fix_rate'] = round(($data->First_Time_Fix_Rate / $data->FTFR_Total_Repair_Calls_All) * 100);
                }

                $up_pl_down = $result_uptime[0]->Up_Time + $result_uptime[0]->Down_Time;
                $payload['up_time'] = 0;
                if ($up_pl_down > 0) {
                    $payload['up_time'] = round(( $result_uptime[0]->Up_Time / $up_pl_down * 100), 1);
                }

                $payload['doctors_trained'] = $data->Doctors_Trained ? number_format($data->Doctors_Trained) : 0;
                $payload['nurses_trained'] = $data->Nurses_Trained ? number_format($data->Nurses_Trained) : 0;
                $payload['e_learning_course_delivered'] = $data->E_Learning_Course_Delivered ? number_format($data->E_Learning_Course_Delivered) : 0;
                $payload['e_learning_course_delivered'] = $data->E_Learning_Course_Delivered ? number_format($data->E_Learning_Course_Delivered) : 0;
                if(isset($socialmediaresult)){
                    $payload['facebook_likes'] = $socialmediaresult[0]->facebook_likes ? number_format($socialmediaresult[0]->facebook_likes) : 0;
                    $payload['instagram_likes'] = $socialmediaresult[0]->instagram_likes ? number_format($socialmediaresult[0]->instagram_likes) : 0;  
                } else {
                    $payload['facebook_likes'] = 0;
                    $payload['instagram_likes'] = 0;                      
                }

                
                
            }
        }

        if ($clinic_id > 0) {

            if (is_array($device_id) && count($device_id) > 0) {
                $where = ' AND Device_Id IN (' . implode(',', $device_id) . ')';
            } elseif ($device_id > 0) {
                $where = ' AND Device_Id = ' . $device_id;
            } else {
                $where = "";
            }

            $clinic_sql = "SELECT"
                    . " sum(U_W_AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_Warranty, sum(U_W_AVG_Repair_Time) as Total_Repair_Calls_Warranty,"
                    . " sum(AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_All, sum(AVG_Repair_Time_All) as Total_Repair_Calls_All,"
                    . " sum(First_Time_Call) as First_Time_Fix_Rate, sum(Total_Repair_Calls_All) as FTFR_Total_Repair_Calls_All,"
                    . " sum(Up_Time) as Up_Time, if(Up_Time > 0, sum(8760 - Up_Time), 0) as Down_Time, sum(Doctors_Trained) as Doctors_Trained,"
                    . " sum(Nurses_Trained) as Nurses_Trained, sum(E_Learning_Course_Delivered) as E_Learning_Course_Delivered"
                    . " FROM tbl_clinic_device WHERE Clinic_Id=$clinic_id $where";

//echo $clinic_sql; exit;
            //  $clinic_sql = "SELECT sum(Quantity) as Solutions FROM tbl_clinic_device WHERE Clinic_Id=$clinic_id $where";

            $clinic_query = $this->db->query($clinic_sql);

            $clinic_result = $clinic_query->result();

            if (count($clinic_result)) {

                foreach ($clinic_result as $k => $data) {

                    $payload['solutions'] = 0;

                    $payload['market_share'] = 0;

                    $payload['total_repair_calls_warranty'] = $data->Total_Repair_Calls_Warranty ? number_format($data->Total_Repair_Calls_Warranty) : 0;

                    $payload['avg_repair_time_12h_warranty'] = 0;
                    if ($data->Total_Repair_Calls_Warranty > 0) {
                        $payload['avg_repair_time_12h_warranty'] = round(($data->Avg_Repair_Time_12h_Warranty / $data->Total_Repair_Calls_Warranty) * 100);
                    }

                    $payload['total_repair_calls_all'] = $data->Total_Repair_Calls_All ? number_format($data->Total_Repair_Calls_All) : 0;

                    $payload['avg_repair_time_12h_all'] = 0;
                    if ($data->Total_Repair_Calls_All > 0) {
                        $payload['avg_repair_time_12h_all'] = round(($data->Avg_Repair_Time_12h_All / $data->Total_Repair_Calls_All) * 100);
                    }

                    $payload['ftfr_total_repair_calls_all'] = $data->FTFR_Total_Repair_Calls_All ? number_format($data->FTFR_Total_Repair_Calls_All) : 0;

                    $payload['first_time_fix_rate'] = 0;
                    if ($data->FTFR_Total_Repair_Calls_All > 0) {
                        $payload['first_time_fix_rate'] = round(($data->First_Time_Fix_Rate / $data->FTFR_Total_Repair_Calls_All) * 100);
                    }

                    $up_pl_down = $data->Up_Time + $data->Down_Time;
                    $payload['up_time'] = 0;
                    if ($up_pl_down > 0) {
                        $payload['up_time'] = round(( $data->Up_Time / $up_pl_down * 100), 1);
                    }

                    $payload['doctors_trained'] = $data->Doctors_Trained ? number_format($data->Doctors_Trained) : 0;
                    $payload['nurses_trained'] = $data->Nurses_Trained ? number_format($data->Nurses_Trained) : 0;
                    $payload['e_learning_course_delivered'] = $data->E_Learning_Course_Delivered ? number_format($data->E_Learning_Course_Delivered) : 0;
                }
            }

            $gentle_laster = $this->clinicDeviceCount(1, 'all', $clinic_id);
            $spectra = $this->clinicDeviceCount(2, 'all',  $clinic_id);
            $ActionII = $this->clinicDeviceCount(3, 'all', $clinic_id);
            $eCo2 = $this->clinicDeviceCount(4, 'all', $clinic_id);
            $Infini = $this->clinicDeviceCount(5, 'all', $clinic_id);
            $enCurve = $this->clinicDeviceCount(7, 'all', $clinic_id);
            $artas = $this->clinicDeviceCount(8, 'all', $clinic_id);
            $ultraformer = $this->clinicDeviceCount(9, 'all', $clinic_id);
            $lasemd = $this->clinicDeviceCount(10, 'all', $clinic_id);
            $picoway = $this->clinicDeviceCount(12, 'all', $clinic_id);
            $vbeam = $this->clinicDeviceCount(13, 'all', $clinic_id);
            $genius = $this->clinicDeviceCount(14, 'all', $clinic_id);

            $payload['gentle_laster'] = (isset($gentle_laster->devicequantity) && $gentle_laster->devicequantity !=0) ? number_format($gentle_laster->devicequantity) : "-";
            $payload['ultraformer'] = (isset($ultraformer->devicequantity) && $ultraformer->devicequantity !=0) ? number_format($ultraformer->devicequantity) : "-";
            $payload['spectra'] = (isset($spectra->devicequantity) && $spectra->devicequantity !=0) ? number_format($spectra->devicequantity) : "-";
            $payload['enCurve'] = (isset($enCurve->devicequantity) && $enCurve->devicequantity !=0) ? number_format($enCurve->devicequantity) : "-";
            $payload['Infini'] = (isset($Infini->devicequantity)  && $Infini->devicequantity !=0) ? number_format($Infini->devicequantity) : "-";
            $payload['eCo2'] = (isset($eCo2->devicequantity) && $eCo2->devicequantity !=0) ? number_format($eCo2->devicequantity) : "-";
            $payload['ActionII'] = (isset($ActionII->devicequantity) && $ActionII->devicequantity !=0) ? number_format($ActionII->devicequantity) : "-";
            $payload['artas'] = (isset($artas->devicequantity) && $artas->devicequantity !=0) ? number_format($artas->devicequantity) : "-";
            $payload['lasemd'] = (isset($lasemd->devicequantity) && $lasemd->devicequantity !=0) ? number_format($lasemd->devicequantity) : "-";
            $payload['picoway'] = (isset($picoway->devicequantity) && $picoway->devicequantity !=0) ? number_format($picoway->devicequantity) : "-";
            $payload['vbeam'] = (isset($vbeam->devicequantity) && $vbeam->devicequantity !=0) ? number_format($vbeam->devicequantity) : "-";
            $payload['genius'] = (isset($genius->devicequantity) && $genius->devicequantity !=0) ? number_format($genius->devicequantity) : "-";
            
            $spectra_ku = $this->clinicDeviceCount(2, 'kuwait', $clinic_id);
            $ActionII_ku = $this->clinicDeviceCount(3, 'kuwait', $clinic_id);
            $eCo2_ku = $this->clinicDeviceCount(4, 'kuwait', $clinic_id);
            $Infini_ku = $this->clinicDeviceCount(5, 'kuwait', $clinic_id);
            $clarity_ku = $this->clinicDeviceCount(6, 'kuwait', $clinic_id);
            $enCurve_ku = $this->clinicDeviceCount(7, 'kuwait', $clinic_id);
            $artas_ku = $this->clinicDeviceCount(8, 'kuwait', $clinic_id);
            $ultraformer_ku = $this->clinicDeviceCount(9, 'kuwait', $clinic_id);
            $lasemd_ku = $this->clinicDeviceCount(10, 'kuwait', $clinic_id);
            $picoplus_ku = $this->clinicDeviceCount(11, 'kuwait', $clinic_id);
            $genius_ku = $this->clinicDeviceCount(14, 'kuwait', $clinic_id);

            $payload['ultraformer_ku'] = (isset($ultraformer_ku->devicequantity) && $ultraformer_ku->devicequantity !=0) ? number_format($ultraformer_ku->devicequantity) : "-";
            $payload['spectra_ku'] = (isset($spectra_ku->devicequantity) && $spectra_ku->devicequantity !=0) ? number_format($spectra_ku->devicequantity) : "-";
            $payload['enCurve_ku'] = (isset($enCurve_ku->devicequantity) && $enCurve_ku->devicequantity !=0) ? number_format($enCurve_ku->devicequantity) : "-";
            $payload['Infini_ku'] = (isset($Infini_ku->devicequantity)  && $Infini_ku->devicequantity !=0) ? number_format($Infini_ku->devicequantity) : "-";
            $payload['eCo2_ku'] = (isset($eCo2_ku->devicequantity) && $eCo2_ku->devicequantity !=0) ? number_format($eCo2_ku->devicequantity) : "-";
            $payload['ActionII_ku'] = (isset($ActionII_ku->devicequantity) && $ActionII_ku->devicequantity !=0) ? number_format($ActionII_ku->devicequantity) : "-";
            $payload['artas_ku'] = (isset($artas_ku->devicequantity) && $artas_ku->devicequantity !=0) ? number_format($artas_ku->devicequantity) : "-";
            $payload['clarity_ku'] = (isset($clarity_ku->devicequantity) && $clarity_ku->devicequantity !=0) ? number_format($clarity_ku->devicequantity) : "-";
            $payload['lasemd_ku'] = (isset($lasemd_ku->devicequantity) && $lasemd_ku->devicequantity !=0) ? number_format($lasemd_ku->devicequantity) : "-";
            $payload['picoplus_ku'] = (isset($picoplus_ku->devicequantity) && $picoplus_ku->devicequantity !=0) ? number_format($picoplus_ku->devicequantity) : "-";
            $payload['genius_ku'] = (isset($genius_ku->devicequantity) && $genius_ku->devicequantity !=0) ? number_format($genius_ku->devicequantity) : "-";

            if(isset($socialmediaresult)){
                $payload['facebook_likes'] = $socialmediaresult[0]->facebook_likes ? number_format($socialmediaresult[0]->facebook_likes) : 0;
                $payload['instagram_likes'] = $socialmediaresult[0]->instagram_likes ? number_format($socialmediaresult[0]->instagram_likes) : 0;  
            } else {
                $payload['facebook_likes'] = 0;
                $payload['instagram_likes'] = 0;                      
            }
            
        }
//        echo "<pre>";print_r( $payload );
//        die;

        return $payload;
    }

    public function getUniqueClinics($where = '', $clinic_id) {
        $where = str_replace('City_Id', 'tbl_clinics.City_Id', $where);
        $where = str_replace('Country_Id', 'tbl_clinics.Country_Id', $where);
        $where = str_replace('Device_Id', 'tbl_device_install.Device_Id', $where);
        $where = str_replace('Clinic_Id', 'tbl_clinics.Clinic_ID', $where);
        if ($clinic_id > 0) {
            $where = " WHERE tbl_clinics.Clinic_ID = $clinic_id";
        }

        $sql = "SELECT count(tbl_clinics.Clinic_ID) as total_clinics FROM tbl_clinics LEFT JOIN tbl_device_install ON tbl_device_install.Clinic_ID = tbl_clinics.Clinic_ID $where GROUP BY tbl_clinics.Clinic_ID";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function getMarketingReport($data = array()) {
        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        //$city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;
        // can be array fo devices
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        $where = array();



        if ($country_id > 0) {
            $where[] = 'Country_Id = ' . $country_id;
        }
//        if($city_id > 0){
//            $where[] = 'City_Id = ' . $city_id;
//        }


        if (is_array($device_id) && count($device_id) > 0) {
            // $device_where = array();
            // foreach( $device_id as $device ){
            //     $device_where[] = 'Device_Id = '.$device;
            // }
            // $where[] = implode(' AND ', $device_where );

            $where[] = 'Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $arr_idx_mkt = strtolower(str_replace(" ", "_", $where));
        $arr_idx_mkt = strtolower(str_replace("=", "_", $arr_idx_mkt)) . "_";
//		echo $arr_idx_mkt."<br>";

        if (array_key_exists($arr_idx_mkt, $this->report_arr_mkt)) {
            $result = $this->report_arr_mkt[$arr_idx_mkt];
        } else {

            $sql = "SELECT sum(Microsite_Visitors) as Microsite_Visitors,sum(Locator_Actions) as Locator_Actions, sum(Qualified_Leads) as Qualified_Leads
			FROM tbl_map_report_marketing $where";

//			echo $sql."<br>";

        $query = $this->db->query($sql);

        $result = $query->result();

            $this->report_arr_mkt[$arr_idx_mkt] = $result;
        }

//		echo '<pre>'; print_r($result); echo '</pre>';
//		echo '<pre>'; print_r($this->report_arr); echo '</pre>';
//		exit;

        $payload = array('microsite_visitors' => 0, 'locator_actions' => 0, 'qualified_leads' => 0);

        if (count($result)) {

            foreach ($result as $k => $data) {

                foreach ($data as $key => $value) {
                    $payload[strtolower($key)] = ($value == null ) ? 0 : number_format($value);
                }
            }
        }
//echo '<pre>'; print_r($payload); exit
        return $payload;
    }
    
    public function getSocialMediaReport($data = array()) {

       $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;
        if (isset($data['devices_id'])) {

            $where = 'WHERE device_id = ' . $device_id;
        } else {
            $where = '';
        }

            $sql = "SELECT sum(facebook_likes) as facebook_likes,sum(instagram_likes) as instagram_likes
			FROM tbl_map_report_socialmedia $where";
        $query = $this->db->query($sql);

        $result = $query->result();

        $payload = array('facebook_likes' => 0, 'instagram_likes' => 0);

        if (count($result)) {

            foreach ($result as $k => $data) {

                foreach ($data as $key => $value) {
                    $payload[strtolower($key)] = ($value == null ) ? 0 : number_format($value);
                }
            }
        }
        
       //echo '<pre>'; print_r($payload); exit;
        return $payload;
    }    

    public function getDeviceSummaryGroupedReport($data = array(), $group_by = '') {

        if (!$group_by)
            return false;

        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;

        // can be array fo devices
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        if ($country_id > 0) {
            $this->db->where('Country_Id', $country_id);
        }

        if ($city_id) {
            $this->db->where('City_Id', $city_id);
        }

        if (is_array($device_id) && count($device_id) > 0) {
            $this->db->where_in('tbl_map_report.Device_Id', $device_id);
        } elseif ($device_id > 0) {
            $this->db->where('tbl_map_report.Device_Id', $device_id);
        }

        // fetch the device name if being grouped by device id
        $ignore_report_cols = $this->ignore_report_cols;
        if ($group_by == 'Device_Id') {
            $this->db->join('tbl_devices', 'tbl_map_report.Device_Id = tbl_devices.Device_Id');

            // ignore cols
            $ignore_report_cols[] = 'Supplier_Id';
            unset($ignore_report_cols['Device_Id']);
        }

        $query = $this->db->get('tbl_map_report');

//        echo $this->db->last_query();

        $result = $query->result();

//        print_r( $result );die;

        $payload = array();

        if (count($result)) {

            foreach ($result as $k => $data) {

                foreach ($data as $key => $value) {

                    if (in_array($key, $ignore_report_cols))
                        continue;

                    if ($group_by == 'Device_Id') {
                        if ($key == 'Device_Name') {
                            $payload[$data->{$group_by}][strtolower($key)] = number_format($value);
                        } else {
                            $payload[$data->{$group_by}][strtolower($key)] = isset($payload[$data->{$group_by}][strtolower($key)]) ? number_format($payload[$data->{$group_by}][strtolower($key)] + $value) : number_format($value);
                        }
                    } else {
                        $payload[$data->{$group_by}][strtolower($key)] = isset($payload[$data->{$group_by}][strtolower($key)]) ? number_format($payload[$data->{$group_by}][strtolower($key)] + $value) : number_format($value);
                    }
                }
            }
        }
//        print_r( $payload );die;

        return $payload;
    }

    public function getDeviceClinicReport($filters) {
        if (isset($filters['devices_id']) && $filters['devices_id'] > 0) {
            $this->db->join('tbl_device_install', 'tbl_device_install.Clinic_Id = tbl_clinics.Clinic_Id');

            if (is_array($filters['devices_id']) && count($filters['devices_id']) > 0) {
                // $device_where = array();
                // foreach( $filters['devices_id'] as $device ){
                //     $this->db->where('tbl_device_install.Device_Id', $device );
                //     // $device_where[] = 'Device_Id = '.$device;
                // }
                // $where[] = implode(' AND ', $device_where );

                $this->db->where_in('tbl_device_install.Device_Id', $filters['devices_id']);
            } elseif ($filters['devices_id'] > 0) {
                $this->db->where('tbl_device_install.Device_Id', $filters['devices_id']);
            }
        }

        if (isset($filters['city_id'])) {
            $this->db->where('tbl_clinics.City_Id', $filters['city_id']);
        }

        $query = $this->db->get('tbl_clinics');

//        echo $this->db->last_query();
//        echo '<pre>';print_r($query->result());echo '</pre>';

        return $query->result();
    }

    public function getServiceHubReportByCity($city_id = 0) {

        if (!$city_id > 0)
            return array();

        $this->db->where('City_Id', $city_id);

        $query = $this->db->get('tbl_service_hubs');

        return $query->result();
    }

    /**
     * Fetch all suppliers
     * @return mixed
     */
    public function getSuppliers() {
        $query = $this->db->get('tbl_supplier');

        return $query->result();
    }

    /**
     * Get devices by supplier
     *
     * @param int $supplier_id
     * @return array
     */
    public function getDeviceBySupplier($supplier_id = 0) {
        if ($supplier_id < 1)
            return array();

        $this->db->where('Supplier_Id', $supplier_id);

        $query = $this->db->get('tbl_devices');

        return $query->result();
    }

    protected function setIgnoreCols($additional_cols = array()) {
        if (count($additional_cols)) {
            $this->ignore_report_cols = array_merge($this->ignore_report_cols, $additional_cols);
        }
    }

    protected function removeIgnoreCols($cols = array()) {

        if (count($cols)) {
            foreach ($cols as $col) {
                foreach ($this->ignore_report_cols as $key => $value) {
                    if ($value == $col) {
                        unset($this->ignore_report_cols[$key]);
                    }
                }
            }
        }
    }

    /**
     * Set default report payload values
     */
    protected function setDefaultReportPayload() {


        $this->default_report_payload = array(
            'installation_base' => 0,
            'avg_repair_time' => 0,
            'repair_time_24h' => 0,
            'calls_solved_12h' => 0,
            'fix_rate' => 0,
            'spare_parts_hub' => 0,
            'doctors_trained' => 0,
            'nurses_trained' => 0,
            'elearning_courses' => 0,
            'campaigns_running' => 0,
            'microsite_visitors' => 0,
            'clinic_locator_actions' => 0,
            'patient_leads' => 0,
            'facebook_likes' => 0,
            'instagram_likes' => 0,            
            'device_name' => '',
            'device_id' => 0,
        );
    }

    public function getVersion() {
        $sql = "SELECT * FROM `tbl_version` ORDER BY `tbl_version`.`Version_Id` DESC LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();
    }
    
    public function getTableCount($database){
        $sql = "USE $database";
        $query = $this->db->query($sql);
        $sql = "SHOW TABLES";
        $query = $this->db->query($sql);
        $sql = "SELECT FOUND_ROWS()";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['FOUND_ROWS()'];
    }
    
    public function dropTablesExceptUser ($database){
        $tableArray = ['tbl_city',
                       'tbl_clinics',
                       'tbl_clinic_device',
                       'tbl_country',
                       'tbl_devices',
                       'tbl_device_install',
                       'tbl_map_report',
                       'tbl_map_report_marketing',
                       'tbl_service_hubs',
                       'tbl_supplier',
                       'tbl_version'
                     ];
        foreach ($tableArray as $key => $value) {
            $sql = "DROP TABLE IF EXISTS $value";
            $query = $this->db->query($sql);                        
        }
        
        
    }
    
    public function clinicDeviceCount($device_id, $clinic_type, $clinic_id){
        $count = $this->db->query("SELECT sum(tbl_clinic_device.Quantity) as devicequantity "
                . "FROM  tbl_clinic_device "
                . "LEFT JOIN tbl_devices ON tbl_clinic_device.Device_Id = tbl_devices.Device_Id "
                . "WHERE tbl_clinic_device.Clinic_Id= $clinic_id "
                . "AND tbl_devices.Device_Id = $device_id AND (tbl_devices.type='both' OR tbl_devices.type='$clinic_type')")->row();
        
        return $count;
    }
    
    
        public function getDeviceClinicClusterMapReport($filters) {
           // echo '<pre>'; print_r($filters); exit;
        if (isset($filters['devices_id']) && $filters['devices_id'] > 0) {
            $this->db->join('tbl_device_install', 'tbl_device_install.Clinic_Id = tbl_clinics.Clinic_Id');

            if (is_array($filters['devices_id']) && count($filters['devices_id']) > 0) {

                $this->db->where_in('tbl_device_install.Device_Id', $filters['devices_id']);
            } elseif ($filters['devices_id'] > 0) {
                $this->db->where('tbl_device_install.Device_Id', $filters['devices_id']);
            }
        }

//        if (isset($filters['city_id'])) {
//            $this->db->where('tbl_clinics.City_Id', $filters['city_id']);
//        }

        $query = $this->db->get('tbl_clinics');

//        echo $this->db->last_query();
//        echo '<pre>';print_r($query->result());echo '</pre>';

        return $query->result();
    }
    
        public function insertMapReportSocialMedia($contents) {
            
          // echo '<pre>'; print_r($contents); exit;
            foreach ($contents['devices'] as $j => $v) {
                $device_id = (isset($contents['devices'][$j]) && $contents['devices'][$j] != "") ? $contents['devices'][$j] : 0;
                $facebook_likes = (isset($contents['facebook_likes'][$j]) && $contents['facebook_likes'][$j] != "") ? $contents['facebook_likes'][$j] : 0;
                $instagram_likes = (isset($contents['instagram_likes'][$j]) && $contents['instagram_likes'][$j] != "") ? $contents['instagram_likes'][$j] : 0;

                $sql = "INSERT INTO  tbl_map_report_socialmedia (device_id, facebook_likes, instagram_likes)"
                        . "VALUES ($device_id, $facebook_likes, $instagram_likes)";            
            
            $query = $this->db->query($sql);
            }
     //   }
    }

}
