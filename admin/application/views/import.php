 
<?php 
$this->load->view('header');

//$datacount = 0;

if($type == "clinics-device"){
    $form = "import_clinics_device";
}
if($type == "map-report"){
    $form = "import_map_report";
}
if($type == "device-count"){
    $form = "import_device_count";
}
if($type == "report-marketing"){
    $form = "import_report_marketing";
}
if($type == "report-socialmedia"){
    $form = "import_report_socialmedia";
}
?>
<script type="text/javascript">

function validate(){ 
    var importVal = $("#excel").val();
    if (importVal == "") {
        document.getElementById('excel').focus();
        $("#import-error").text('Required');

        return false;
    } else {
        var str = importVal;
        var strArr = str.split('.');
        if (strArr[1] != 'xls' && strArr[1] != 'xlsx') {
            document.getElementById('excel').focus();
            $("#import-error").text('* Please upload excel in xls  or xlsx format');


            return false;
        } else {
            $("#import-error").text('');
        }
        $("#<?php echo $form; ?>").submit();
    }

};
function showalert(){ 
$("#myModal").modal('show');
}
function cleardata(){ 
    $("#cleardata").submit();
}
</script>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
<!--              <div class="title_left">
                <h3>Import</h3>
              </div>-->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
<!--                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>-->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="cleardata" action="cleardata" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <input type="hidden" name="import-type" value="<?php echo $type; ?>"/>
                        <input type="hidden" name="cleardata" value="1"/>
                    </form>
                    
                   <?php if(isset($datacount) && ($datacount ==0)) {?>
                    
                    <form id="<?php echo $form; ?>" action="<?php echo $form; ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                      <div class="form-group">
                            <?php 
                               //if(isset($success)){ ?>
<!--                            <div id="data-count" style="color:green;margin-left: 292px;">
                                <?php 
                                //echo $success;
                                ?>
                            </div>-->
                            <?php //} ?>
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="file-upload" style="padding-left: 100px;">Upload File<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" id="excel" type="file" name="excel" value="" style="width: 50%;"/><a href="<?php  echo base_url() ?>sample-files/<?php echo  $sample_file; ?>" style>Download Sample File</a>
                            
                          <input type="hidden" name="import-type" value="<?php echo $type; ?>"/>
                        </div>
                          
              
                      </div>
<!--                      <div class="ln_solid"></div>-->
                    </form>
                        <?php } ?>
<!--                    <div class="col-md-12">

                         <?php 
                           // if(isset($datacount) && ($datacount > 0)){ ?>
                         <?php 
                           // if(isset($success)){ ?>
                         <div id="data-count" style="color:green;">
                             <h2>
                             <?php 
                            // echo $success;
                             ?>
                             </h2>
                         </div>
                         <?php //} ?>

                         <?php //} ?>
                        
                    </div>-->
                    <?php 
                if(isset($datacount) && ($datacount > 0)){ ?>
                    <div class="col-md-12">
                        <div class="col-md-3">&nbsp;</div>
                        <div class="col-md-6" style="text-align: center;line-height: 40px;">
                            <div id="data-count" style="color:grey;">
                                <h3>
                                   <?php 
                                    echo $datacount_title;
                                    ?>
                                </h3>
                                                            
                                <button class="btn btn-primary" type="reset" onclick="showalert()" id="import-reset"  style="background-color:#f6b40e;border-color: white">Clear Data</button>
                            </div>
                        </div>

                        <div class="col-md-3">&nbsp;</div>

                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<!--                          <button class="btn btn-primary" type="button" id="import-cancel">Cancel</button>-->
                                <?php 
                               if(isset($datacount) || (count($errors)>0)){
                                   if($datacount == 0){ ?>
                        <button type="submit" class="btn btn-success" id="import-submit" onclick="validate()">Import Data</button>
                               <?php    }  }?>

                          
                          
                        </div>

                      </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?php 
                                if(isset($errors) && (count($errors)>0)){ ?>
                            <div id="import-error" style="color:red;">
  

                           <?php foreach($errors as $key => $val){
                                    echo "<br>".$val."<br>";
                                } ?>
                           </div> 
                          <?php     }
                                ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </div>
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content" style="width:20%; margin-left: 550px;margin-top: 300px;">
    <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete" onclick="cleardata()" style="background-color:#f6b40e;border-color: white">Clear Data</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
  </div>

</div>
            </div>
        
        <!-- /page content -->

<?php 
$this->load->view('footer');
?>