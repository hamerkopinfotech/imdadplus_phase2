<?php 
$this->load->view('header');
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&libraries=places&callback"></script>
        <style type="text/css">
          #map{ width:700px; height: 500px; padding-left:400px; }
        </style>
        <h1>Select a location!</h1>
        <p>Click on a location on the map to select it. Drag the marker to change location.</p>
        
        <!--map div-->
        <div id="map"></div>
        
        <!--our form-->
        <h2>Chosen Location</h2>
        <form method="post">
            <input type="text" id="lat" readonly="yes"><br>
            <input type="text" id="lng" readonly="yes">
        </form>
<?php 

        $host = $_SERVER['HTTP_HOST'];
      
        if($host == "localhost" || $host == 'localhost:8080'){ 
            $radiusviewpercent = .50;
            
        } else {
            $radiusviewpercent = .26;
        }
            if (isset($_SERVER['HTTPS']) &&
                ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
              $protocol = 'https://';
            }
            else {
              $protocol = 'http://';
            }
            if($host == 'ewaantech.com'){
                
                $url_starts = $protocol.$host.'/imdadplus';
            } else if($host == 'imdadplus.com'){
                $url_starts = $protocol.$host;
            } else {
                $url_starts = $protocol.$host.'/imdadplus_phase2';
            }
//$this->load->view('headermapload');
            //echo '<pre>'; print_r($citys); exit;
?>
<script>
var map; //Will contain map object.
var markers = false; ////Has the user plotted their location marker? 
var marker = false; ////Has the user plotted their location marker?
localStorage.setItem("clicked", "no");        
        
//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
 
    var markers = <?php echo $json_to_display; ?>;
    var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['city_centre_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['city_centre_long'] ?>)};
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom: 7 //The zoom value.
    };
    
 //       //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);   
    var bounds = new google.maps.LatLngBounds();
    $.each(markers, function (index,value) {
                var icon;
                    var pos = new google.maps.LatLng(value.lat, value.lng);
                    // icon = '/imdadplus_phase2/img/markers/blue-old';
                    icon = {
                        url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                        scaledSize: new google.maps.Size(28, 28), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(28, 28) // anchor
                    }
                
		
	  	bounds.extend(pos);
	
		marker = new google.maps.Marker({
		  	position: pos,
                        // draggable:true,
			  map: map,
			  icon: icon,
                          //draggable: true
		  });
                  
                //  console.log(marker);
	});   
    
    
    
    
 
    //Create the map object.
  //  map = new google.maps.Map(document.getElementById('map'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            
            var  icon_new = {
                url: '<?php echo $url_starts; ?>/img/markers/low_density.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(54, 54) // anchor
            }
       // marker.setPosition(clickedLocations);
            markers = new google.maps.Marker({
            position: clickedLocation,
            map: map,
            icon: icon_new,
            //z-index: 1200,
            draggable: true //make it draggable
        });            
            
            
            
            
            //Create the marker.
//            marker = new google.maps.Marker({
//                position: clickedLocation,
//                map: map,
//                draggable: true //make it draggable
//            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else {
        //console.log('sdsds');
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}

//function initMap() {
// 
//
//    var markers = <?php echo $json_to_display; ?>;
//    var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['city_centre_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['city_centre_long'] ?>)};
// 
//    //Map options.
//    var options = {
//      center: centerOfMap, //Set center.
//      zoom: 7 //The zoom value.
//    };
//       //Create the map object.
//    map = new google.maps.Map(document.getElementById('map'), options);   
//    var bounds = new google.maps.LatLngBounds();
//    $.each(markers, function (index,value) {
//                var icon;
//                    var pos = new google.maps.LatLng(value.lat, value.lng);
//                    // icon = '/imdadplus_phase2/img/markers/blue-old';
//                    icon = {
//                        url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
//                        scaledSize: new google.maps.Size(28, 28), // scaled size
//                        origin: new google.maps.Point(0, 0), // origin
//                        anchor: new google.maps.Point(28, 28) // anchor
//                    }
//                
//		
//	  	bounds.extend(pos);
//	
//		marker = new google.maps.Marker({
//		  	position: pos,
//                        // draggable:true,
//			  map: map,
//			  icon: icon,
//                          //draggable: true
//		  });
//                  
//                //  console.log(marker);
//	});  
//
//    google.maps.event.addListener(map, 'click', function(event) {
//    var clickedLocations;
//        var isclicked = localStorage.getItem("clicked")
//        if(isclicked != 'yes'){
//                clickedLocations = event.latLng;
//                    var  icon_new = {
//                        url: '<?php echo $url_starts; ?>/img/markers/low_density.png', // url
//                        scaledSize: new google.maps.Size(54, 54), // scaled size
//                        origin: new google.maps.Point(0, 0), // origin
//                        anchor: new google.maps.Point(54, 54) // anchor
//                    }
//               // marker.setPosition(clickedLocations);
//                    markers = new google.maps.Marker({
//                    position: clickedLocations,
//                    map: map,
//                    icon: icon_new,
//                    //z-index: 1200,
//                    draggable: true //make it draggable
//                });
//                            //Listen for drag events!
//                google.maps.event.addListener(markers, 'dragend', function(event){
//                    markerLocation();
//                });
//      //      }
//            //Get the marker's location.
//            markerLocation();
//            localStorage.setItem("clicked", "yes");
//        } 
//            
//        
//        //marker.setPosition(clickedLocation);
//    });   
//    
// //   }
//        
//    
//}
        
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
}
        
        
//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);
</script>
