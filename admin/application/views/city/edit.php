<?php
$this->load->view('header');
?>
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&libraries=places&callback"></script>

<!--       <script src="<?php echo base_url(); ?>design/js/map.js"></script>-->
<script src="<?php echo base_url(); ?>design/js/locationpicker.jquery.js"></script>

<?php
$host = $_SERVER['HTTP_HOST'];

if ($host == "localhost" || $host == 'localhost:8080') {
    $radiusviewpercent = .50;
} else {
    $radiusviewpercent = .26;
}
if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}
if ($host == 'ewaantech.com') {

    $url_starts = $protocol . $host . '/imdadplus';
} else if ($host == 'imdadplus.com') {
    $url_starts = $protocol . $host;
} else {
    $url_starts = $protocol . $host . '/imdadplus_phase2';
}
//$this->load->view('headermapload');

$set_high_density = $details['set_high_density'];
$set_medium_density = $details['set_medium_density'];
$set_low_density = $details['set_low_density'];
;
?>



<style type="text/css">
    /*  #map1{ width:48%; height: 700px; float:right; margin-top: -700px;}
      #map2{ width:48%; height: 700px; float:right; margin-top: -700px;}
      #map3{ width:48%; height: 700px; float:right; margin-top: -700px;}*/
    #map1{ width:100%; height: 600px; float:left; border-style:solid;}
    #map2{ width:100%; height: 600px; float:left; border-style:solid;}
    #map3{ width:100%; height: 600px; float:left; border-style:solid;}
    #pagemap{ width:100%; height: 600px; float:left; border-style:solid;}
    .links { color:#0078bf !important;}
    .previewlink {
        background: #0078bf;
        padding: 10px 20px;
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        float:right;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-warning').hide();
        $('.alert-danger').hide();
        var set_high_density = "<?php echo $set_high_density; ?>";
        var set_medium_density = "<?php echo $set_medium_density; ?>";
        var set_low_density = "<?php echo $set_low_density; ?>";


        if (set_high_density == 'YES') {
            $("#set_high_density_yes").attr('checked', 'checked');
            $("#map1").show();
        } else if (set_high_density == 'NO') {
            $("#set_high_density_no").attr('checked', 'checked');
            $("#map1").hide();

        }

        if (set_medium_density == 'YES') {
            $("#set_medium_density_yes").attr('checked', 'checked');
            $("#map2").show();
        } else if (set_medium_density == 'NO') {
            $("#set_medium_density_no").attr('checked', 'checked');
            $("#map2").hide();

        }

        if (set_low_density == 'YES') {
            $("#set_low_density_yes").attr('checked', 'checked');
            $("#map3").show();
        } else if (set_low_density == 'NO') {
            $("#set_low_density_no").attr('checked', 'checked');
            $("#map3").hide();

        }


//        $("#set_high_density_yes").attr('checked', 'checked');
//        $("#set_medium_density_yes").attr('checked', 'checked');
//        $("#set_low_density_yes").attr('checked', 'checked');

        $(".set_high_density").click(function () {
            var curval = $(this).val();
            if (curval == "NO") {
                $("#map1").hide();
            } else {
                $("#map1").show();
            }

        });
        $(".set_medium_density").click(function () {
            var curval = $(this).val();
            if (curval == "NO") {
                $("#map2").hide();
            } else {
                $("#map2").show();
            }

        });
        $(".set_low_density").click(function () {
            var curval = $(this).val();
            if (curval == "NO") {
                $("#map3").hide();
            } else {
                $("#map3").show();
            }

        });



    });

    function cancelcity() {
        location.reload();
    }
    function back() {
        var url = "<?php echo base_url() ?>city/";
        $(location).attr('href', url);
    }
    function savecity() {

        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('#feild_empty').show().fadeOut(4000);
                valid = false
                return false;
            }
        });
        if (valid == true) {
            $("#updatecity").submit();
        }


    }

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);
    });
</script>   
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
<?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                    <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
<?php } ?>                 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="alert alert-warning" id="feild_empty">
                        <strong>Warning!</strong> Please fill all the fields.
                    </div>
                    <div class="col-md-6 col-sm-6">

                        <h2><b>Edit City</b></h2>

                    </div>       

                    <div class="clearfix"></div>
                    <div class="x_title"></div>
                    <!--                    <div class="x_title">
                                            <h2>Edit City</h2>
                    
                                            <div class="clearfix"></div>
                                        </div>-->
                    <div class="x_content">
                        <form id="cancelcity" action="index" enctype="multipart/form-data" method="post" accept-charset="utf-8" lass="form-horizontal form-label-left">
                        </form>
                        <form id="updatecity" action="<?php echo base_url() ?>city/updatecity" enctype="multipart/form-data" method="post" accept-charset="utf-8" lass="form-horizontal form-label-left">

                            <input id="cityid" type="hidden" name="cityid" value="<?php echo $this->uri->segment(3); ?>">
                            <!--                     <div class="form-group">
                                                     <h2><b>SECTION - CITY DETAILS:</b></h2>
                                                     <div class="clearfix"></div>
                                                 </div>-->

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="first-name"><b>Name</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="first_name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['cityname']; ?>">
                                </div>
                                <label class="control-label col-sm-2" for="first-name"><b>Country</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <select class="form-control" id="countries" name="countries">
                                        <option value="">--Select Option--</option>
                                        <?php
                                        foreach ($countries as $key => $val) {
                                            if ($details['countryid'] == $countries[$key]['countryid']) {

                                                $selected = "selected = selected";
                                            } else {
                                                $selected = "";
                                            }
                                            ?>

                                            <option <?php echo $selected; ?> value="<?php echo $countries[$key]['countryid']; ?>"><?php echo $countries[$key]['countryname']; ?></option>
<?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>                            

                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-9">
<!--                                <h5>Geolocation for displaying the city markers in the country page</h5>-->
<h5 style="line-height: 23px;font-size: 13px;padding-top: 15px;"><span style="font-size: 16px;">Location coordinates and marker type for displaying the city markers in the country page</span><br><br>These values used in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/country_page.png" target="blank">imdad+ country page</a>, <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_country.png" target="blank">device based clinic listing map country page</a> and <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/density_country.png" target="blank">investors / insights map (clinic density map) country page</a><br>Marker type value is used for identifying the city with normal, office OR service hub markers</h5>
                                </div>
                                <div class="col-md-3" style="padding-top: 15px;">
                                    <a target="blank" href="http://imdadplus.com" class="previewlink">Imdadplus Website preview</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
<h5 style="line-height: 23px;font-size: 13px;">Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</h5>
                                </div>
                                <div class="clearfix"></div>
                                </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="latitude_listing"><b>Latitude :</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="latitude_listing" name="latitude_listing" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['lat_listing']; ?>">
                                </div>
                                <label class="control-label col-sm-2" for="longitude_listing"><b>Longitude :</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <input type="text" id="longitude_listing" name="longitude_listing" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['long_listing']; ?>">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-sm-2" for="marker_type"><b>Marker Type</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <select class="form-control" id="marker_type" name="marker_type">
                                        <option value="">--Select Option--</option>
                                        <option <?php echo ($details['marker_type'] == "nothing") ? "selected=selected" : ""; ?> value="nothing">NOTHING</option>
                                        <option <?php echo ($details['marker_type'] == "office") ? "selected=selected" : ""; ?> value="office">OFFICE</option>
                                        <option <?php echo ($details['marker_type'] == "service_hub") ? "selected=selected" : ""; ?> value="service_hub">SERVICE HUB</option>
                                        <option <?php echo ($details['marker_type'] == "both") ? "selected=selected" : ""; ?> value="both">BOTH</option>
                                    </select>
                                </div>                            


                                <div class="clearfix"></div>
                            </div>                            
<!--                            <div class="form-group" style="margin-top:30px;">
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h4><b>Geolocation for displaying the city center marker in the city page.</b></h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h5><b><i>Drag the marker in the map for setting the location and adjust the map zoom level</i></b></h5>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-12">
<!--                                    <h5>Geolocation for displaying the city center marker in the city page.</h5>-->

<h5 style="line-height: 23px;font-size: 13px;padding-top: 15px;"><span style="font-size: 16px;">Location coordinates for centering the city in the city page and load the map with zoom level defined</span><br><br>These values used in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_city.png" target="blank">imdad+ city page</a> and <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/density_city.png" target="blank">investors / insights map (clinic density map) city page</a><br>Nearest city is using when a city loaded have no clinics</h5>
<!--<h5>Geolocation for displaying the city center marker in the city page. This is displaying in imdad+ city page <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/city_page.png" target="blank">Click Here</a> and density map city page <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/density_page.png" target="blank">Click Here</a>. Nearest city is using when a city loaded have no clinics for a device.</h5>-->
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <h5>Drag the marker in the map for setting the location and adjust the map zoom level</h5>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>                           
                            <div class="form-group">
                                <div class="clearfix"></div>
                                <div id="pagemap"></div> 
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="latitude_page"><b>Latitude</b><span class="required">*</span>
                                </label>

                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="latitude_page" name="latitude_page" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['lat_page']; ?>">
                                </div>
                                <label class="control-label col-sm-2" for="longitude_page"><b>Longitude</b><span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="longitude_page" name="longitude_page" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['long_page']; ?>">
                                </div>
                                <label class="control-label col-sm-1" for="zoom"><b>Zoom</b><span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="zoom" name="zoom" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['zoom']; ?>">
                                </div>
                                <div class="clearfix"></div>
                            </div>          

                            <div class="form-group" style="margin-top:25px;">
                                <div class="clearfix"></div>
                                <div class="col-md-9">
                                    <h5 style="font-size: 16px;">Section to manage the device based clinic listing map in IMDAD websites</h5>
                                </div>
                                <div class="col-md-3">
                                    <a target="blank" href="<?php echo $url_starts . '/api/loadmap?auth_key=' . md5('load imdadplus in iframe: ' . date('Y-m-d')) . '&country_name=' . $countryname . '&city_name=' . $cityname; ?>" class="previewlink">Click here for preview</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <h5 style="line-height: 25px;font-size: 13px;">Location coordinates for centering the city in the city page and load the map with zoom level defined<br>These values used in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_city.png" target="blank">device based clinic listing map city page</a></h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <h5 style="font-size: 13px;">Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</h5>
                                </div>

                            </div> 
                            <div class="clearfix"></div>
                            <div class="form-group" style="margin-top:20px;">

                                <label class="control-label col-sm-2" for="sitecore_lat"><b>Latitude:</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_lat" name="sitecore_lat" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['sitecore_lat']; ?>">
                                </div>

                                <label class="control-label col-sm-2" for="sitecore_lng"><b>Longitude:</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_lng" name="sitecore_lng" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['sitecore_lng']; ?>">
                                </div>
                                <div class="clearfix"></div>
                            </div>                     
                            <div class="form-group">    
                                <label class="control-label col-sm-2" for="sitecore_zoom"><b>Zoom Level:</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_zoom" name="sitecore_zoom" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['sitecore_zoom']; ?>">
                                </div>
                                <label class="control-label col-sm-2" for="first-name"><b>Nearest City</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <select class="form-control" id="nearest_city" name="nearest_city">
                                        <option value="">--Select Option--</option>
                                        <?php foreach ($cities as $key => $val) {
                                            ?>
                                            <option value="<?php echo $cities[$key]['cityid']; ?>" <?php echo ($details['nearest_city'] == $cities[$key]['cityid']) ? "selected=selected" : ""; ?>><?php echo $cities[$key]['cityname']; ?></option>
<?php } ?>
                                    </select>
                                </div>                            

                                <div class="clearfix"></div> 
                            </div>
                            <div class="form-group" style="margin-top:30px;">
                                <div class="clearfix"></div>
                                <div class="col-md-9">
                                    <h5 style="font-size: 16px;">Section to manage the investors / insights map (clinic density map) in imdad websites</h5>
                                </div>
                                <div class="col-md-3">
                                    <a target="blank" href="<?php echo $url_starts . '/api/densitymap?auth_key=' . md5('load imdadplus in iframe: ' . date('Y-m-d')) . '&country_name=' . $countryname . '&city_name=' . $cityname; ?>" class="previewlink">Click here for preview</a> 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group" style="padding:20px 0px 20px 0px;">

<!--                                <label class="control-label col-sm-2" for="density_marker_1"><b>Location 1</b><span class="required">*</span>
                                </label>-->
                                <!--                                <div class="col-md-2 col-sm-3">
                                                                    <b>Latitude</b><span class="required">*</span>
                                -->
                                <input type="hidden" id="density_point_1_lat" name="density_point_1_lat" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_1_lat']; ?>">
                                <!--                                </div>-->
                                <!--                                <div class="col-md-2 col-sm-3">
                                                                    <b>Longitude</b><span class="required">*</span>-->
                                <input type="hidden" id="density_point_1_lng" name="density_point_1_lng" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_1_lng']; ?>">
                                <!--                                </div>-->
                                <div class="col-md-8 col-sm-8">
                                    <b>Do you want to display High Density map?</b><span class="required">*</span>&nbsp;&nbsp;Yes&nbsp;&nbsp;<input type="radio" id="set_high_density_yes" class="set_high_density" name="set_high_density" required="required" value="YES">&nbsp;&nbsp;No&nbsp;&nbsp;<input type="radio" id="set_high_density_no" class="set_high_density" name="set_high_density" required="required" class="" value="NO">
                                </div>
                                <div class="clearfix"></div>
                            </div> 

                            <div class="form-group">
                                <!--                         <div class="clearfix"></div>-->
                                <!--                         <div id="somecomponent1" style="width: 50%; height: 700px;"></div>-->
                                <div id="map1"></div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group" style="padding:20px 0px 20px 0px;">

<!--                                <label class="control-label col-sm-2" for="density_marker_2"><b>Location 2</b><span class="required">*</span>
                                </label>-->
                                <!--                                <div class="col-md-2 col-sm-3">
                                                                    <b>Latitude</b><span class="required">*</span>-->

                                <input type="hidden" id="density_point_2_lat" name="density_point_2_lat" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_2_lat']; ?>">
                                <!--                                </div>-->
                                <!--                                <div class="col-md-2 col-sm-3">
                                                                    <b>Longitude</b>
                                                                    <span class="required">*</span>-->
                                <input type="hidden" id="density_point_2_lng" name="density_point_2_lng" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_2_lng']; ?>">
                                <!--                                </div>-->
                                <div class="col-md-8 col-sm-8">
                                    <b>Do you want to display Medium Density map?</b><span class="required">*</span>&nbsp;&nbsp;Yes&nbsp;&nbsp;<input type="radio" id="set_medium_density_yes" class="set_medium_density" name="set_medium_density" required="required" value="YES">&nbsp;&nbsp;No&nbsp;&nbsp;<input type="radio" id="set_medium_density_no" class="set_medium_density" name="set_medium_density" required="required" class="" value="NO">
                                </div>
                            </div>                            
                            <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="clearfix"></div>
                        <!--                         <div id="somecomponent2" style="width: 50%; height: 700px;"></div>-->
                        <div id="map2"></div>
                        <!--                         <div class="map" style="width: 50%; height: 500px;"></div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" style="padding:20px 0px 20px 0px;">

<!--                                    <label class="control-label col-sm-2" for="density_marker_3"><b>Location 3</b><span class="required">*</span>
                                    </label>-->
                        <!--                                    <div class="col-md-2 col-sm-3">
                                                                <b>Latitude</b>-->
                        <!--                                        <span class="required">*</span>-->
                        <input type="hidden" id="density_point_3_lat" name="density_point_3_lat" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_3_lat']; ?>">
                        <!--                                    </div>-->
                        <!--                                    <div class="col-md-2 col-sm-3">
                                                                <b>Longitude</b>-->
                        <!--                                        <span class="required">*</span>-->
                        <input type="hidden" id="density_point_3_lng" name="density_point_3_lng" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['density_point_3_lng']; ?>">
                        <!--                                    </div>-->
                        <div class="col-md-8 col-sm-8">
                            <b>Do you want to display Low Density map?</b><span class="required">*</span>&nbsp;&nbsp;Yes&nbsp;&nbsp;<input type="radio" id="set_low_density_yes" class="set_low_density" name="set_low_density" required="required" value="YES">&nbsp;&nbsp;No&nbsp;&nbsp;<input type="radio" id="set_low_density_no" class="set_low_density" name="set_low_density" required="required" class="" value="NO">
                        </div>
                        <div class="clearfix"></div>
                    </div>   
                    <div class="form-group">
                        <div class="clearfix"></div>
                        <!--                         <div id="somecomponent3" style="width: 50%; height: 700px;"></div>-->
                        <div id="map3"></div>
                        <div class="clearfix"></div>
                    </div>
                    <!--                     <div class="form-group" style="margin-top:30px;">
                                             <div class="clearfix"></div>
                                             <h2><b><i>Section to set density of clinics</i></b></h2>
                                             <div class="clearfix"></div>
                                         </div>-->
                    <!--                        <div class="form-group">
                                                
                                                <label class="control-label col-sm-6" for="high_density"><img height="28px" width="28px" src="<?php echo $url_starts; ?>/img/markers/high_density.png"/><b>High Density - if greater than:</b><span class="required">*</span>
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                  <input type="text" id="high_density" name="high_density" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['high_density']; ?>">
                                             
                                                </div>
                                            </div>
                                                                    <div class="clearfix"></div>
                                            
                                             <div class="form-group">    
                                                <label class="control-label col-sm-6" for="medium_density"><img height="28px" width="28px" src="<?php echo $url_starts; ?>/img/markers/medium_density.png"/><b>Medium Density - if less than High Density and greater than:</b><span class="required">*</span>
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                  <input type="text" id="medium_density" name="medium_density" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['medium_density']; ?>">
                                                </div>
                                             </div>
                                                                    <div class="clearfix"></div>
                                            <div class="form-group">  
                                                <label class="control-label col-sm-6" for="low_density"><img height="28px" width="28px" src="<?php echo $url_starts; ?>/img/markers/low_density.png"/><b>Low Density - if less than Medium Density and greater than:</b><span class="required">*</span>
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                  <input type="text" id="low_density" name="low_density" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $details['low_density']; ?>">
                                                </div> 
                                            </div>-->
                    <div class="clearfix"></div>
                </div>                        
                </form>

                <!--map div-->
                <!--                    <div id="map"></div>-->

                <!--our form-->                  
                <div class="form-group">  
                    <div class="ln_solid"></div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" onclick="savecity()">Submit</button>
                        <button class="btn btn-primary" type="button" onclick="back()">Cancel</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <br><h4>Notes:</h4>
                    1.* - Mandatory Feilds.
                    <br>2.Latitude and longitude is for identifying the geolocation
                    <!--                            <br>3.&nbsp;<b>Latitude (List)</b> and <b>Longitude (List)</b>: This is for plotting the city markers in city listing of the country page.-->
                    <br>3.Zoom level is the zoom in which the map is loading 
                    <!--                            <br>5.&nbsp;<b>Zoom Level</b>: This is for the map zoom level in the city page.
                                                <br>
                                                <br>
                                                <b><i>Section for IFRAME to embed in Imdad Websites</i></b>
                                                <br>
                                                <br>6.&nbsp;<b>Latitude (IFRAME)</b> and <b>Longitude (IFRAME)</b>: This is for centering the city in the map loading in IFRAME.
                                                <br>7.&nbsp;<b>Zoom Level (IFRAME)</b>: This is for the map zoom level in IFRAME.
                                                <br>8.&nbsp;<b>Nearest City</b>: Nearest City.
                                                <br>
                                                <br>
                                                <b><i>Section to manage the locations to identify clinic density</i></b>
                                                <br>
                                                <br>
                                                9.&nbsp;<b>Location 1, Location 2 & Location 3 and Radius in Meters</b>:-->
                </div>


            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php //$zoom = 7;   ?>
<script>
    var map1; //Will contain map object.
    var marker1 = false; ////Has the user plotted their location marker? 
    var map2; //Will contain map object.
    var marker2 = false; ////Has the user plotted their location marker? 
    var map3; //Will contain map object.
    var marker3 = false; ////Has the user plotted their location marker? 
    localStorage.setItem("clicked1", "no");
    localStorage.setItem("clicked2", "no");
    localStorage.setItem("clicked3", "no");


    function initMap1() {


        var markers = <?php echo $json_to_display; ?>;
        var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['density_point_1_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['density_point_1_lng'] ?>)};

        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        //Create the map object.
        map1 = new google.maps.Map(document.getElementById('map1'), options);
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function (index, value) {
            var icon;
            var pos = new google.maps.LatLng(value.lat, value.lng);
            // icon = '/imdadplus_phase2/img/markers/blue-old';
            icon = {
                url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                scaledSize: new google.maps.Size(28, 28), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            }


            bounds.extend(pos);

            marker1 = new google.maps.Marker({
                position: pos,
                // draggable:true,
                map: map1,
                icon: icon,
                //draggable: true
            });

            //  console.log(marker);
        });

        var icon_new = {
            url: '<?php echo $url_starts; ?>/img/markers/high_density.png', // url
            //scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            //anchor: new google.maps.Point(54, 54) // anchor
        }
        markers1 = new google.maps.Marker({
            position: centerOfMap,
            map: map1,
            icon: icon_new,
            zIndex: 99999,
            draggable: true //make it draggable
        });

        google.maps.event.addListener(markers1, 'dragend', function (event) {
            var currentLocation1 = markers1.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('density_point_1_lat').value = currentLocation1.lat(); //latitude
            document.getElementById('density_point_1_lng').value = currentLocation1.lng(); //longitude                    
        });

    }

    function initMap2() {


        var markers = <?php echo $json_to_display; ?>;
        var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['density_point_2_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['density_point_2_lng'] ?>)};

        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        //Create the map object.
        map2 = new google.maps.Map(document.getElementById('map2'), options);
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function (index, value) {
            var icon;
            var pos = new google.maps.LatLng(value.lat, value.lng);
            // icon = '/imdadplus_phase2/img/markers/blue-old';
            icon = {
                url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                scaledSize: new google.maps.Size(28, 28), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            }


            bounds.extend(pos);

            marker2 = new google.maps.Marker({
                position: pos,
                // draggable:true,
                map: map2,
                icon: icon,
                //draggable: true
            });

            //  console.log(marker);
        });

        var icon_new = {
            url: '<?php echo $url_starts; ?>/img/markers/medium_density.png', // url
            //scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            //anchor: new google.maps.Point(54, 54) // anchor
        }
        markers2 = new google.maps.Marker({
            position: centerOfMap,
            map: map2,
            icon: icon_new,
            zIndex: 99999,
            //z-index: 1200,
            draggable: true //make it draggable
        });

        google.maps.event.addListener(markers2, 'dragend', function (event) {
            var currentLocation2 = markers2.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('density_point_2_lat').value = currentLocation2.lat(); //latitude
            document.getElementById('density_point_2_lng').value = currentLocation2.lng(); //longitude                    
        });


    }

    function initMap3() {


        var markers = <?php echo $json_to_display; ?>;
        var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['density_point_3_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['density_point_3_lng'] ?>)};

        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        //Create the map object.
        map3 = new google.maps.Map(document.getElementById('map3'), options);
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function (index, value) {
            var icon;
            var pos = new google.maps.LatLng(value.lat, value.lng);
            // icon = '/imdadplus_phase2/img/markers/blue-old';
            icon = {
                url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                scaledSize: new google.maps.Size(28, 28), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            }


            bounds.extend(pos);

            marker3 = new google.maps.Marker({
                position: pos,
                // draggable:true,
                map: map3,
                icon: icon,
                //draggable: true
            });
            //  console.log(marker);
        });

        var icon_new = {
            url: '<?php echo $url_starts; ?>/img/markers/low_density.png', // url
            //scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            //anchor: new google.maps.Point(54, 54) // anchor
        }
        markers3 = new google.maps.Marker({
            position: centerOfMap,
            map: map3,
            icon: icon_new,
            zIndex: 99999,
            //z-index: 1200,
            draggable: true //make it draggable
        });

        google.maps.event.addListener(markers3, 'dragend', function (event) {
            var currentLocation3 = markers3.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('density_point_3_lat').value = currentLocation3.lat(); //latitude
            document.getElementById('density_point_3_lng').value = currentLocation3.lng(); //longitude                    
        });


    }

    function initMap2() {


        var markers = <?php echo $json_to_display; ?>;
        var centerOfMap = {lat: parseFloat(<?php echo $citys[0]['density_point_2_lat'] ?>), lng: parseFloat(<?php echo $citys[0]['density_point_2_lng'] ?>)};

        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        //Create the map object.
        map2 = new google.maps.Map(document.getElementById('map2'), options);
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function (index, value) {
            var icon;
            var pos = new google.maps.LatLng(value.lat, value.lng);
            // icon = '/imdadplus_phase2/img/markers/blue-old';
            icon = {
                url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                scaledSize: new google.maps.Size(28, 28), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            }


            bounds.extend(pos);

            marker2 = new google.maps.Marker({
                position: pos,
                // draggable:true,
                map: map2,
                icon: icon,
                //draggable: true
            });

            //  console.log(marker);
        });

        var icon_new = {
            url: '<?php echo $url_starts; ?>/img/markers/medium_density.png', // url
            //scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            //anchor: new google.maps.Point(54, 54) // anchor
        }
        markers2 = new google.maps.Marker({
            position: centerOfMap,
            map: map2,
            icon: icon_new,
            zIndex: 99999,
            //z-index: 1200,
            draggable: true //make it draggable
        });

        google.maps.event.addListener(markers2, 'dragend', function (event) {
            var currentLocation2 = markers2.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('density_point_2_lat').value = currentLocation2.lat(); //latitude
            document.getElementById('density_point_2_lng').value = currentLocation2.lng(); //longitude                    
        });


    }

    function initPageMap() {


        var markers = <?php echo $json_to_display; ?>;
        var centerOfMap = {lat: parseFloat($('#latitude_page').val()), lng: parseFloat($('#longitude_page').val())};

        //Map options.
        var options = {
            center: centerOfMap, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        //Create the map object.
        pagemap = new google.maps.Map(document.getElementById('pagemap'), options);
        var bounds = new google.maps.LatLngBounds();

        var icon_new = {
            url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
            scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(54, 54) // anchor
        }
        markers4 = new google.maps.Marker({
            position: centerOfMap,
            map: pagemap,
            icon: icon_new,
            zIndex: 99999,
            //z-index: 1200,
            draggable: true //make it draggable
        });

        google.maps.event.addListener(markers4, 'dragend', function (event) {
            var currentLocation4 = markers4.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('latitude_page').value = currentLocation4.lat(); //latitude
            document.getElementById('longitude_page').value = currentLocation4.lng(); //longitude                    
        });

        pagemap.addListener('zoom_changed', function () {
            document.getElementById('zoom').value = pagemap.getZoom(); //longitude 
        });


    }

//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
    function markerLocation1() {
        //Get location.
        var currentLocation1 = marker1.getPosition();
        //Add lat and lng values to a field that we can save.
        document.getElementById('density_point_1_lat').value = currentLocation1.lat(); //latitude
        document.getElementById('density_point_1_lng').value = currentLocation1.lng(); //longitude
    }
    function markerLocation2() {
        //Get location.
        var currentLocation2 = marker2.getPosition();
        //Add lat and lng values to a field that we can save.
        document.getElementById('density_point_2_lat').value = currentLocation2.lat(); //latitude
        document.getElementById('density_point_2_lng').value = currentLocation2.lng(); //longitude
    }
    function markerLocation3() {
        //Get location.
        var currentLocation3 = marker3.getPosition();
        //Add lat and lng values to a field that we can save.
        document.getElementById('density_point_3_lat').value = currentLocation3.lat(); //latitude
        document.getElementById('density_point_3_lng').value = currentLocation3.lng(); //longitude
    }


//Load the map when the page has finished loading.
    google.maps.event.addDomListener(window, 'load', initMap1);
    google.maps.event.addDomListener(window, 'load', initMap2);
    google.maps.event.addDomListener(window, 'load', initMap3);
    google.maps.event.addDomListener(window, 'load', initPageMap);
</script>          
<?php
$this->load->view('footer');
?>

</html>

