<?php
$this->load->view('header');
?>
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&libraries=places&callback"></script>

<!--       <script src="<?php echo base_url(); ?>design/js/map.js"></script>-->
<script src="<?php echo base_url(); ?>design/js/locationpicker.jquery.js"></script>

<?php
$host = $_SERVER['HTTP_HOST'];

if ($host == "localhost" || $host == 'localhost:8080') {
    $radiusviewpercent = .50;
} else {
    $radiusviewpercent = .26;
}
if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}
if ($host == 'ewaantech.com') {

    $url_starts = $protocol . $host . '/imdadplus';
} else if ($host == 'imdadplus.com') {
    $url_starts = $protocol . $host;
} else {
    $url_starts = $protocol . $host . '/imdadplus_phase2';
}
//$this->load->view('headermapload');

;
?>



<style type="text/css">
    /*  #map1{ width:48%; height: 700px; float:right; margin-top: -700px;}
      #map2{ width:48%; height: 700px; float:right; margin-top: -700px;}
      #map3{ width:48%; height: 700px; float:right; margin-top: -700px;}*/
    #map1{ width:100%; height: 600px; float:left; border-style:solid;}
    #map2{ width:100%; height: 600px; float:left; border-style:solid;}
    #map3{ width:100%; height: 600px; float:left; border-style:solid;}
    #pagemap{ width:100%; height: 600px; float:left; border-style:solid;}
    .links { color:#0078bf !important;}
    .previewlink {
        background: #0078bf;
        padding: 10px 20px;
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        float:right;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
            $('.alert-warning').hide();
            $('.alert-danger').hide();
    });
    function cancelcity(){
        location.reload();
    }
    function back(){
        var url = "<?php  echo base_url() ?>city/";
        $(location).attr('href',url);
    }
    function savecity(){ 
        
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
               window.scrollTo(0,0);
               myarray.push(valid);
               $('#feild_empty').show().fadeOut(4000); 
               valid = false
                return false;
            }
        }); 
        if(valid == true){
            $("#savecity").submit();
        }
        
        
    }
</script>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="alert alert-warning" id="feild_empty">
                      <strong>Warning!</strong>Please fill all the mandatory fields.
                    </div>
                  <div class="x_title">
                    <h2>Add New City</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form id="cancelcity" action="index" enctype="multipart/form-data" method="post" accept-charset="utf-8" lass="form-horizontal form-label-left">
                      </form>
                  <form id="savecity" action="<?php  echo base_url() ?>city/savecity" enctype="multipart/form-data" method="post" accept-charset="utf-8" lass="form-horizontal form-label-left">
                     
                            <!--                     <div class="form-group">
                                                     <h2><b>SECTION - CITY DETAILS:</b></h2>
                                                     <div class="clearfix"></div>
                                                 </div>-->

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="first-name"><b>Name</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="first_name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <label class="control-label col-sm-2" for="first-name"><b>Country</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <select class="form-control" id="countries" name="countries">
                                        <option value="">--Select Option--</option>
                                        <?php
                                        foreach ($countries as $key => $val) {
                                            ?>

                                            <option value="<?php echo $countries[$key]['countryid']; ?>"><?php echo $countries[$key]['countryname']; ?></option>
<?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>                            
<!--                            <div class="form-group">
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h4><b>Geolocation for displaying the city markers in the country page.</b></h4>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a target="blank" href="http://imdadplus.com" class="previewlink">Imdadplus for preview</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
                                    <h5><b><i>Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</i></b></h5>
                                </div>
                                <div class="clearfix"></div>
                            </div>-->
                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
<!--                                <h5>Geolocation for displaying the city markers in the country page.</h5>-->
<h5 style="line-height: 23px;font-size: 13px;padding-top: 21px;"><span style="font-size: 16px;">Geolocation for displaying the city markers in the country page.</span><br><br>This location coordinate is used for displaying the markers in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/country_page.png" target="blank">imdad+ country page</a>, <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_country.png" target="blank">imdad websites device installation count marker map country page</a> and <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/density_country.png" target="blank">imdad websites investors / insights map (clinic density map) country page</a>. <br>Marker type value is used for identifying the city with normal, office OR service hub markers.</h5>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a target="blank" href="http://imdadplus.com" class="previewlink">Imdadplus Website preview</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
                                <h5>Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</h5>
                                </div>
                                <div class="clearfix"></div>
                                </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="latitude_listing"><b>Latitude :</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="latitude_listing" name="latitude_listing" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <label class="control-label col-sm-2" for="longitude_listing"><b>Longitude :</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <input type="text" id="longitude_listing" name="longitude_listing" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-sm-2" for="marker_type"><b>Marker Type</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <select class="form-control" id="marker_type" name="marker_type">
                                        <option value="">--Select Option--</option>
                                        <option value="nothing">NOTHING</option>
                                        <option value="office">OFFICE</option>
                                        <option value="service_hub">SERVICE HUB</option>
                                        <option value="both">BOTH</option>
                                    </select>
                                </div>                            


                                <div class="clearfix"></div>
                            </div>                            
<!--                            <div class="form-group" style="margin-top:30px;">
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h4><b>Geolocation for displaying the city center marker in the city page.</b></h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h5><b><i>Drag the marker in the map for setting the location and adjust the map zoom level</i></b></h5>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
<!--                                    <h5>Geolocation for displaying the city center marker in the city page.</h5>-->
<h5 style="line-height: 23px;font-size: 13px;padding-top: 21px;"><span style="font-size: 16px;">Geolocation for displaying the city markers in the City page.</span><br><br>This location coordinate is used for displaying the markers in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_city.png" target="blank">imdad+ city page</a> and <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/density_city.png" target="blank">imdad websites investors / insights map (clinic density map) country page</a>. <br>Nearest city is using when a city loaded have no clinics for a device.</h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
                                    <h5>Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</h5>
                                <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
<!--                                <div class="col-md-8 col-sm-6">
                                    <h5>Drag the marker in the map for setting the location and adjust the map zoom level</h5>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>                           
                            <div class="form-group">
                                <div class="clearfix"></div>
                                <div id="pagemap"></div> 
                                <div class="clearfix"></div>
                            </div>-->
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="latitude_page"><b>Latitude</b><span class="required">*</span>
                                </label>

                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="latitude_page" name="latitude_page" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <label class="control-label col-sm-2" for="longitude_page"><b>Longitude</b><span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="longitude_page" name="longitude_page" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <label class="control-label col-sm-1" for="zoom"><b>Zoom</b><span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="text" id="zoom" name="zoom" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <div class="clearfix"></div>
                            </div>          

                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-sm-6">
                                    <h5><b>Section to manage the IFRAME loaded for clinic listing in IMDAD websites.</b></h5>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group" style="margin-top:10px;">
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
                                    <h5 style="line-height: 23px;font-size: 13px;padding-top: 21px;"><span style="font-size: 16px;">Geolocation for displaying the city markers in the City page.</span><br><br>This location coordinate is used for displaying the markers in <a class="links" href="<?php echo $url_starts; ?>/img/screenshots/iframe_city.png" target="blank">imdad+ city page</a>.</h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-10 col-sm-6">
                                    <h5>Get the latitude and longitude of the location from website here <a target="blank" href="https://www.latlong.net/" style="color:#0078bf !important;">https://www.latlong.net/</a>, then copy paste it in the fields below</h5>
                                </div>

                            </div> 
                            <div class="clearfix"></div>
                            <div class="form-group" style="margin-top:20px;">

                                <label class="control-label col-sm-2" for="sitecore_lat"><b>Latitude (IFRAME):</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_lat" name="sitecore_lat" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>

                                <label class="control-label col-sm-2" for="sitecore_lng"><b>Longitude (IFRAME):</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_lng" name="sitecore_lng" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <div class="clearfix"></div>
                            </div>                     
                            <div class="form-group">    
                                <label class="control-label col-sm-2" for="sitecore_zoom"><b>Zoom Level (IFRAME):</b><span class="required">*</span>
                                </label>

                                <div class="col-md-4 col-sm-3">
                                    <input type="text" id="sitecore_zoom" name="sitecore_zoom" required="required" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <label class="control-label col-sm-2" for="first-name"><b>Nearest City</b><span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-3">

                                    <select class="form-control" id="nearest_city" name="nearest_city">
                                        <option value="">--Select Option--</option>
                                        <?php foreach ($cities as $key => $val) {
                                            ?>
                                            <option value="<?php echo $cities[$key]['cityid']; ?>"><?php echo $cities[$key]['cityname']; ?></option>
<?php } ?>
                                    </select>
                                </div>                            

                                <div class="clearfix"></div> 
                            </div>
                      
<!--                    <div class="form-group">
                         <div class="clearfix"></div>
                         <div id="somecomponent3" style="width: 100%; height: 500px;"></div>
                         <div class="clearfix"></div>
                     </div>                      -->
                    </form>
<!--                    <h1>Select a location!</h1>
                    <p>Click on a location on the map to select it. Drag the marker to change location.</p>

                    map div
                    <div id="map"></div>


                    our form
                    <h2>Chosen Location</h2>
                    <form method="post">
                        <input type="text" id="lat" readonly="yes"><br>
                        <input type="text" id="lng" readonly="yes">
                    </form>-->
                    <div class="form-group">
                       <div class="ln_solid"></div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success" onclick="savecity()">Submit</button>
                          <button class="btn btn-primary" type="button" onclick="back()">Cancel</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                <div class="form-group">
                    <br><h4>Notes:</h4>
                    1.* - Mandatory Feilds.
                    <br>2.Latitude and longitude is for identifying the geolocation
                    <!--                            <br>3.&nbsp;<b>Latitude (List)</b> and <b>Longitude (List)</b>: This is for plotting the city markers in city listing of the country page.-->
                    <br>3.Zoom level is the zoom in which the map is loading 
                    <!--                            <br>5.&nbsp;<b>Zoom Level</b>: This is for the map zoom level in the city page.
                                                <br>
                                                <br>
                                                <b><i>Section for IFRAME to embed in Imdad Websites</i></b>
                                                <br>
                                                <br>6.&nbsp;<b>Latitude (IFRAME)</b> and <b>Longitude (IFRAME)</b>: This is for centering the city in the map loading in IFRAME.
                                                <br>7.&nbsp;<b>Zoom Level (IFRAME)</b>: This is for the map zoom level in IFRAME.
                                                <br>8.&nbsp;<b>Nearest City</b>: Nearest City.
                                                <br>
                                                <br>
                                                <b><i>Section to manage the locations to identify clinic density</i></b>
                                                <br>
                                                <br>
                                                9.&nbsp;<b>Location 1, Location 2 & Location 3 and Radius in Meters</b>:-->
                </div>
                  </div>
                </div>
              </div>
            </div>
              </div>
            </div>
  <script>
//    function initPageMap() {
//
//
//        var markers = <?php echo $json_to_display; ?>;
//        var centerOfMap = {lat: parseFloat($('#latitude_page').val()), lng: parseFloat($('#longitude_page').val())};
//
//        //Map options.
//        var options = {
//            center: centerOfMap, //Set center.
//            zoom: <?php echo $zoom; ?> //The zoom value.
//        };
//        //Create the map object.
//        pagemap = new google.maps.Map(document.getElementById('pagemap'), options);
//        var bounds = new google.maps.LatLngBounds();
//
//        var icon_new = {
//            url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
//            scaledSize: new google.maps.Size(54, 54), // scaled size
//            origin: new google.maps.Point(0, 0), // origin
//            anchor: new google.maps.Point(54, 54) // anchor
//        }
//        markers4 = new google.maps.Marker({
//            position: centerOfMap,
//            map: pagemap,
//            icon: icon_new,
//            zIndex: 99999,
//            //z-index: 1200,
//            draggable: true //make it draggable
//        });
//
//        google.maps.event.addListener(markers4, 'dragend', function (event) {
//            var currentLocation4 = markers4.getPosition();
//            //Add lat and lng values to a field that we can save.
//            document.getElementById('latitude_page').value = currentLocation4.lat(); //latitude
//            document.getElementById('longitude_page').value = currentLocation4.lng(); //longitude                    
//        });
//
//        pagemap.addListener('zoom_changed', function () {
//            document.getElementById('zoom').value = pagemap.getZoom(); //longitude 
//        });
//
//
//    }


//Load the map when the page has finished loading.
    //google.maps.event.addDomListener(window, 'load', initPageMap);
</script>                     
<?php 
$this->load->view('footer');
?>
</html>

