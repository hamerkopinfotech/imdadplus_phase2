 
<?php 
$this->load->view('header');
?>
<script type="text/javascript">

function execute(){ 
        var url = "<?php  echo base_url() ?>report/syncjsondataclustermap";
        $(location).attr('href',url);
}

function executedevice(id,name){ 
        var url = "<?php  echo base_url() ?>report/syncdevicedataclustermap?device="+id+"&&devicename="+name;
        $(location).attr('href',url);
}
</script>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
<!--              <div class="title_left">
                <h3>Import</h3>
              </div>-->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
<!--                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>-->
                    <div class="clearfix"></div>
                    
                  </div>
                    
                  <div class="x_content">
                        <?php 
                               if(isset($success) && ($success !="")){ ?>
                        <div id="data-success">

                            
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                                <?php 
                                echo $success;
                                ?>
                  </div>
                            </div>
                      <br />
                            <?php } ?>
                      
                    

                    
                    <?php 
                    	$split_url = explode('/',trim($_SERVER['REQUEST_URI'],'/'));
                        $project = (isset($split_url[0]))?$split_url[0]:"imdadplus_phase2";
                    
                    ?>
                      <?php //echo '<pre>'; print_r($devices);?>
                    <table>
                        <tbody>
<!--                            <tr style="font-style: italic">
                                <td style="width:400px">Generate Json Data for Home Page Quick Load</td><td style="width:200px"></td><td style="width:200px"><button type="submit" class="btn btn-success" id="import-submit" onclick="execute()">Generate Json Data</button></td>
                                <td><a target="blank" class="btn btn-primary" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/<?php echo $project; ?>/" style="background-color:#f6b40e;border-color: white;;float:right">Visit Website</a></td>
              
                            </tr>-->
                            <?php foreach($devices as $key => $val){ ?>
                            <tr style="font-style: italic">
                                <?php $devicenames = explode('-', $devices[$key]['devicename']); ?>
                                <td style="width:400px">Generate Json Data for device <b><?php echo $devicenames[1]; ?></b> </td><td style="width:200px;"><?php echo $devices[$key]['clusterdate']; ?></td><td style="width:200px"><button type="submit" class="btn btn-success" id="import-submit" onclick="executedevice(<?php echo $devices[$key]['deviceid']; ?>,'<?php echo $devicenames[1]; ?>')">Generate Json Data</button></td>
              
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            </div>
        
        <!-- /page content -->

<?php 
$this->load->view('footer');
?>