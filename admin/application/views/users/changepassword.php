
<!DOCTYPE html>
<?php 
$this->load->view('header');
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Change Password</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?php echo base_url(); ?>users/changepassword" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pass-word">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" name="pass-word" id="pass-word" required="required" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('pass-word','<span class="help-block" style="color: #CC6633">','</span>'); ?>
                            <?php if($this->session->flashdata('message')) {?>
                                <span class="help-block" style="color: green"><?php echo $this->session->flashdata('message');?></span>
                            <?php }?>
                        </div>
                      </div>
<!--                      <div class="ln_solid"></div>-->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <input type="submit" name="changePassword" class="btn btn-success" value="Submit"/>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    
        <!-- /page content -->
<?php 
$this->load->view('footer');
?>
      
