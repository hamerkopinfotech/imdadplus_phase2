<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>IMDAD PLUS! | </title>

    <!-- Bootstrap -->
    <link href="design/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="design/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="design/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="design/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="design/build/css/custom.min.css" rel="stylesheet">
    <!--   custom design changes-->
    <link href="design/css/css_admin.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content login_override">
            <form action="" method="post">
              <h1>Admin Login</h1>
              <div>
                <input type="email" class="form-control" name="email" placeholder="Email" required="" />
                <?php echo form_error('email','<span class="help-block" style="color: #CC6633">','</span>'); ?>
<!--                <input type="text" class="form-control" placeholder="Username" required="" />-->
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="">
                <?php echo form_error('password','<span class="help-block" style="color: #CC6633">','</span>'); ?>
<!--                <input type="password" class="form-control" placeholder="Password" required="" />-->
              </div>
                <?php if($this->session->flashdata('message')) {?>
                <div>
                    <label><span style="color: #CC6633"><?php echo $this->session->flashdata('message');?><span></label>
                </div>
                <?php }?>
              <div>
<!--                <a class="btn btn-default submit" href="">Log in</a>-->
                    <input type="submit" name="loginSubmit" class="btn btn-default submit" value="Log in"/>
                <a class="reset_pass login_override" href="<?php echo base_url(); ?>welcome/display_doforget">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

<!--              <div class="separator">
                <p class="change_link">New to site?
                  <a href="<?php echo base_url(); ?>/users/registration" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> IMDAD!</h1>
                <p>©2018 All Rights Reserved. Imdad. Privacy and Terms</p>
                </div>
              </div>-->
              <div class="separator separator_override">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>
<!--                      <i class="fa fa-paw"></i> -->
                      IMDAD PLUS</h1>
                  <p>Copyright © 2012 Imdad LLC</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

<!--                <div>
                <p>©2018 All Rights Reserved. Imdad. Privacy and Terms</p><!--
                </div>-->
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>

