
<!DOCTYPE html>
<?php 
$this->load->view('header');
?>
  
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <?php if(!$this->session->flashdata('message')) {?>   
                        <h2>Sync LIVE Data to Local Server - Step 1</h2>
                    <?php }?>
                    <?php if($this->session->flashdata('message')) {?>   
                        <h2>Sync LIVE Data to Local Server - Step 2</h2>
                    <?php }?>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php if($this->session->flashdata('messageafterimport')) {?>
                        <div class="alert alert-success" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                                      <?php 
                                      echo $this->session->flashdata('messageafterimport');
                                      ?>
                        </div>
                    <?php } ?> 
                    <div class="col-md-12">
                        <div class="col-md-3">&nbsp;</div>
                        <?php if(!$this->session->flashdata('message')) {?>                       
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div id="data-count" style="color:grey;float:right">

                                       Export LIVE Data : <a class="btn btn-primary" href="<?php  echo base_url() ?>welcome/database_export_init" style="background-color:#f6b40e;border-color: white;">Export</a>
                                    </div>
                                </div>

                                
                            </div>
                        <?php }?>
                        <?php if($this->session->flashdata('message')) {?>  
                            <div class="col-md-6">
                                <div id="data-count" style="color:grey;">

                                   Import LIVE Data to Local Server : <a class="btn btn-primary" href="<?php  echo base_url() ?>import/database_import_process" style="background-color:#f6b40e;border-color: white">Import</a>
                                </div>
                            </div>
                        <?php }?>
<!--                        <div class="col-md-12">
                         <?php if($this->session->flashdata('messageafterimport')) {?>
                            <span class="help-block" style="color: green; padding-left: 460px;"><?php echo $this->session->flashdata('messageafterimport');?></span>
                        <?php }?>                           
                        </div>-->
                    </div>
<!--                    <form action="<?php echo base_url(); ?>users/profile" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user-name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="user-name" id="user-name" value="<?php echo $user['name']; ?>" required="required" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('user-name','<span class="help-block" style="color: #CC6633">','</span>'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" name="email" value="<?php echo $user['email']; ?>" required="required" class="form-control col-md-7 col-xs-12">
                          <?php echo form_error('email','<span class="help-block" style="color: #CC6633">','</span>'); ?>
                            <?php if($this->session->flashdata('message')) {?>
                                <span class="help-block" style="color: green"><?php echo $this->session->flashdata('message');?></span>
                            <?php }?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <input type="submit" name="profileUpdate" class="btn btn-success" value="Submit"/>
                        </div>
                      </div>

                    </form>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    
        <!-- /page content -->
<?php 
$this->load->view('footer');
?>
      
