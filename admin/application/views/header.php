<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>IMDAD PLUS | </title>
    
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>-->

    <!-- Bootstrap -->
    <link href="<?php  echo base_url() ?>design/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php  echo base_url() ?>design/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php  echo base_url() ?>design/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php  echo base_url() ?>design/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!--   custom design changes-->
    <link href="<?php  echo base_url() ?>design/css/css_admin.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php  echo base_url() ?>design/build/css/custom.min.css" rel="stylesheet">
  </head>
  <script src="<?php  echo base_url(); ?>design/build/js/jquery-1.10.2.js"></script>
  <script src="<?php  echo base_url(); ?>design/build/js/jquery-ui.js"></script>

  <script>
      var $ = jQuery;
jQuery(document).ready(function() {
        jQuery('.nav').show();
});
   
  </script>

  <body class="nav-md body_override">
      
    <div class="container body body_override">
      <div class="main_container body_override">
        <div class="col-md-3 left_col  body_override">
          <div class="left_col scroll-view body_override">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php  echo base_url() ?>users/account" class="site_title">
<!--                  <span>IMDAD PLUS</span>-->
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
<!--              <div class="profile_pic">
                <img src="<?php  echo base_url() ?>design/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>-->
<!--              <div class="profile_info">
                <span>Welcome, <h2><?php echo $user['name']; ?></h2></span>
              </div>-->
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
<!--                  <h3 style="border-bottom: 1px double white;">General</h3>-->
                <ul class="nav side-menu">
                        <?php
                    if ($tablecount > 1) {
                      ?> 
                  <li><a><i class="fa fa-home"></i> Synced Data <span class="fa fa-chevron-down"></span></a>
                    <?php } ?>
                    <ul class="nav child_menu">
                    <?php
                        if ($tablecount > 1) {
                      ?>       
                        
                      <li><a href="<?php  echo base_url() ?>import/clinics_device">Clinics and Device Installation</a></li>
                      <li><a href="<?php  echo base_url() ?>import/device_count">Clinic Report</a></li>
                      <li><a href="<?php  echo base_url() ?>import/map_report">Map Report</a></li>
                      <li><a href="<?php  echo base_url() ?>import/report_marketing">Map Report Marketing</a></li>
                      <li><a href="<?php  echo base_url() ?>import/report_socialmedia">Map Report Social Media</a></li>
                    <?php
                        }
                      ?>  
                      
                  </li>
                </ul>
                </ul>
 <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> General Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <?php 
                        if ($is_local) {                    
                      ?>
                        <li><a href="<?php  echo base_url() ?>import/database_sync">Data Sync</a></li>
                      <?php 
                        }  
                      ?>
                    <?php
                    if ($tablecount > 1) {
                      ?>                      
                      <li><a href="<?php  echo base_url() ?>report/sync_json">Generate Json</a></li>
<!--                    <li><a href="<?php  echo base_url() ?>report/sync_clustor_map_json">Generate Cluster Map Json</a></li>-->
                        
                        <li><a href="<?php  echo base_url() ?>city/">City Management</a></li>
                    <?php
                    }
                      ?> 
                </ul>
                  </li>
 </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
<!--            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a href="<?php echo base_url(); ?>/users/logout" data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>-->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
<!--                    <img src="<?php  echo base_url() ?>design/images/img.jpg" alt="">-->
                    <?php echo $user['name']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url(); ?>users/profile"> Profile</a></li>
                    <li><a href="<?php echo base_url(); ?>users/changepassword"> Change Password</a></li>
<!--                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>-->
                    <li><a href="<?php echo base_url(); ?>/users/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
<!--                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>-->
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="<?php  echo base_url() ?>design/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php  echo base_url() ?>design/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php  echo base_url() ?>design/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="<?php  echo base_url() ?>design/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->