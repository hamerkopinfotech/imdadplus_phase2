<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller {
        public function __construct()
        {
            $this->data = parent::__construct();
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            set_time_limit(0);
        }
	public function index()
	{
		$this->load->view('import');
	}
        
         public function clinics_device()
	{
             $this->data['title'] = "Import clinics and device installation data";
             $this->data['type'] = "clinics-device";
             $clinicDeviceInstallCount = $this->Me_model->getClinicDeviceInstallCount();
             $clinicCount = $this->Me_model->getClinicCount();
             $this->data['datacount'] = $clinicDeviceInstallCount;
             $this->data['datacount_title'] = "";
             $this->data['sample_file'] = "clinics-and-devices-installed-sample.xlsx";
            if($clinicDeviceInstallCount > 0){
             $this->data['datacount_title'] = "Total <b>$clinicCount Clinics</b> and <b>$clinicDeviceInstallCount Devices</b> Found.";
            }
             return $this->load->view('import',$this->data);
	}
        
         public function map_report()
	{
             $this->data['title'] = "Import Map Report";
             $this->data['type'] = "map-report";
             $mapReportCount = $this->Me_model->getMapReportCount();
             $this->data['datacount'] = $mapReportCount;
             $this->data['datacount_title'] = "";
             $this->data['sample_file'] = "map-report-data-sample.xlsx";
             if($mapReportCount > 0){
                $this->data['datacount_title'] = "Total <b>$mapReportCount Map Report</b> Found.";
             }
             return $this->load->view('import',$this->data);
	}
        
        public function device_count()
	{
             $this->data['title'] = "Clinic Report";
             $this->data['type'] = "device-count";
             $deviceCount= $this->Me_model->getDeviceCount();
             $this->data['datacount'] = $deviceCount;
             $this->data['datacount_title'] = "";  
             $this->data['sample_file'] = "map-report-clinic.xlsx";
            if($deviceCount > 0){
                $this->data['datacount_title'] = "Total <b>$deviceCount Clinic Report</b> Found.";
             }
             return $this->load->view('import',$this->data);
	}
        public function report_marketing()
	{
             $this->data['title'] = "Import Map Report Marketing";
             $this->data['type'] = "report-marketing";
             $reportMarketingCount= $this->Me_model->getReportMarketingCount();
             $this->data['datacount'] = $reportMarketingCount;
             $this->data['datacount_title'] = "";
             $this->data['sample_file'] = "countrywise-marketing-report-data.xlsx";
            if($reportMarketingCount > 0){
                $this->data['datacount_title'] = "Total <b>$reportMarketingCount Map Report Marketing</b> Found.";
            }
             return $this->load->view('import',$this->data);
	}
         public function report_socialmedia()
	{
             $this->data['title'] = "Import Map Report Social Media";
             $this->data['type'] = "report-socialmedia";
             $mapReportSocialMediaCount = $this->Me_model->getMapReportSocialMediaCount();
             $this->data['datacount'] = $mapReportSocialMediaCount;
             $this->data['datacount_title'] = "";
             $this->data['sample_file'] = "map-report-socialmedia-data-sample.xlsx";
             if($mapReportSocialMediaCount > 0){
                $this->data['datacount_title'] = "Total <b>$mapReportSocialMediaCount Map Report Social Media</b> Found.";
             }
             return $this->load->view('import',$this->data);
	}        
        
        public function import_clinics_device(){
			
            $this->load->library('excel');
            $import_type = $this->input->post('import-type');
			
            $content =  array();
			$content['errors'] = array();
			
            $content['datacount'] = 0;
            $content['datacount_title'] = "";
            if($import_type = "clinics-device"){
                $content['title'] = "Import clinics and device installation data";
                $content['type'] = "clinics-device";
                $content['sample_file'] = "clinics-and-devices-installed-sample.xlsx";
            }
            $file_tmp =$_FILES['excel']['tmp_name'];

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $arrayCount = count($allDataInSheet);
			
			$all_countries = $this->Country_model->getAllCountries();
			
			$all_cities = $this->City_model->getAllCities();
			
			$all_devices = $this->Device_model->getAllDevices();
			
			//echo '<pre>';print_r($all_devices);echo '</pre>';exit;
            
			$excel_data =  $clinic_name_data =  array();
            for($i=2;$i<=$arrayCount;$i++){
				$clinic_code = trim($allDataInSheet[$i]["C"]);
				$clinic_name = trim($allDataInSheet[$i]["D"]);
				$clinic_name = str_replace('"', '', $clinic_name);
				$clinic_name = str_replace("'", "", $clinic_name);
				
				$nameandcode = $clinic_code. "-" . $clinic_name;
								
				$country_name = trim($allDataInSheet[$i]["A"]);
				$country_id = $this->Country_model->isCountryExist($country_name, $all_countries);
				if($country_id == 0){
					$content['errors'][] = "Error - Country Name (<b>$country_name</b>) is not matching at <b>Row $i</b>";
				}
				
				$city_name = trim($allDataInSheet[$i]["B"]);
				$city_id = $this->City_model->isCityExist($city_name, $all_cities);
				if($city_id == 0){
					$content['errors'][] = "Error - City Name (<b>$city_name</b>) is not matching at <b>Row $i</b>";
				}
					
				$device_name = trim($allDataInSheet[$i]["G"]);
				$device_id = $this->Device_model->isDeviceExist($device_name, $all_devices);
				if($device_id == 0){
					$content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $i</b>";
				}
				
				if(!in_array($nameandcode, $clinic_name_data)) {
					$excel_data[$nameandcode]['country'] = $country_id;
					$excel_data[$nameandcode]['city'] = $city_id;
                                        $excel_data[$nameandcode]['clinic_code'] = $clinic_code;
					$excel_data[$nameandcode]['clinic'] = $clinic_name;
					$excel_data[$nameandcode]['latitude'] = trim($allDataInSheet[$i]["E"]);
					$excel_data[$nameandcode]['longitude'] = trim($allDataInSheet[$i]["F"]);
					
					$clinic_name_data[] = $nameandcode;
				}
				
				$excel_data[$nameandcode]['device'][] = $device_id;
            }
			
			//echo '<pre>';print_r($excel_data);echo '</pre>';
			//echo '<pre>';print_r($clinic_name_data);echo '</pre>';
			//echo '<pre>';print_r($content['errors']);echo '</pre>';
			//exit;
			
			if(count($content['errors']) == 0) {
				$this->Me_model->insertClinicsAndDevice($excel_data);
				
				$clinic_count = $this->Me_model->getClinicCount();
				$content['datacount'] = $this->Me_model->getClinicDeviceInstallCount();
				if($clinic_count > 0){
					$content['datacount_title'] = "Total <b>" . $this->Me_model->getClinicCount() . " Clinics</b> and <b>" . $content['datacount'] . " Devices</b> Found.";
				}
				$content['success'] = "<b>Data Imported Successfully</b>";
				$this->Me_model->updateVersionDate();
			}
			
			$this->data = array_merge($this->data, $content);
			
			//echo '<pre>';print_r($this->data);echo '</pre>';
				
			return $this->load->view('import',$this->data);
            
        }
        
        
        public function import_map_report(){
            
            $content =  array();
            $this->load->library('excel');
            $import_type = $this->input->post('import-type');
            $content['datacount'] = 0;
            $content['datacount_title'] = "";
            
            $all_countries = $this->Country_model->getAllCountries();

            $all_cities = $this->City_model->getAllCities();

            $all_devices = $this->Device_model->getAllDevices();
            
            if($import_type = "map-report"){
                $content['title'] = "Import Map Report";
                $content['type'] = "map-report";
                $content['sample_file'] = "map-report-data-sample.xlsx";
            }
            $file_tmp =$_FILES['excel']['tmp_name'];
            
            $datas = array();
            $errors = array();
            $device = array();

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }


            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $arrayCount = count($allDataInSheet);
            //echo '<pre>';print_r($allDataInSheet);exit;
            $j = 0;
            for($i=2;$i<=$arrayCount;$i++){
                //$datas['country'][] = trim($allDataInSheet[$i]["A"]);
                $country_name = trim($allDataInSheet[$i]["A"]);
                $country_id = $this->Country_model->isCountryExist($country_name, $all_countries);
                if($country_id == 0){
                        $content['errors'][] = "Error - Country Name (<b>$country_name</b>) is not matching at <b>Row $i</b>";
                } else {
                    $content['country'][] = $country_id;
                }
                
                //$datas['city'][] = trim($allDataInSheet[$i]["B"]);
                
                $city_name = trim($allDataInSheet[$i]["B"]);
                $city_id = $this->City_model->isCityExist($city_name, $all_cities);
                if($city_id == 0){
                        $content['errors'][] = "Error - City Name (<b>$city_name</b>) is not matching at <b>Row $i</b>";
                } else {
                    $content['city'][] = $city_id;
                }
                
                //$datas['device'][] = trim($allDataInSheet[$i]["C"]);
                $device_name = trim($allDataInSheet[$i]["C"]);
                $device_id = $this->Device_model->isDeviceExist($device_name, $all_devices);
                if($device_id == 0){
                        $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $i</b>";
                } else {
                        $content['devices'][] = $device_id;
                }
                
                $datas['installation_base'][] = trim($allDataInSheet[$i]["D"]);
                $datas['total_installation_base'][] = trim($allDataInSheet[$i]["E"]);
                $datas['U_W_AVG_Repair_Time_12_h'][] = trim($allDataInSheet[$i]["F"]);
                $datas['U_W_AVG_Repair_Time'][] = trim($allDataInSheet[$i]["G"]);
                $datas['AVG_Repair_Time_12_h'][] = trim($allDataInSheet[$i]["H"]);
                $datas['AVG_Repair_Time_All'][] = trim($allDataInSheet[$i]["I"]);
                $datas['First_Time_Call'][] = trim($allDataInSheet[$i]["J"]);
                $datas['Total_Repair_Calls_All'][] = trim($allDataInSheet[$i]["K"]);
                $datas['Up_Time'][] = trim($allDataInSheet[$i]["L"]);
                $datas['Doctors_Trained'][] = trim($allDataInSheet[$i]["M"]);
                $datas['Nurses_Trained'][] = trim($allDataInSheet[$i]["N"]);
                $datas['E_Learning_Course_Delivered'][] = trim($allDataInSheet[$i]["O"]);
 
            }

//            $j = 0;            
//            foreach($datas['country'] as $key => $val){
//                $country_name = trim($val);
//                $country_id = $this->Country_model->isAvailable($country_name);
//                $k = $j+2;
//                if($country_id == 0){
//                    $content['errors'][] = "Error - Country Name (<b>$country_name</b>) is not matching at <b>Row $k</b>";
//                } else {
//                    $content['country'][] = $country_id;
//                }
//                $j++;
//            }
            
//            $j = 0;            
//            foreach($datas['city'] as $key => $val){
//                $city_name = trim($val);
//                $city_id = $this->City_model->isAvailable($city_name);
//                $k = $j+2;
//                if($city_id == 0){
//                    $content['errors'][] = "Error - City name (<b>$city_name</b>) is not matching at <b>Row $k</b>";
//                } else {
//                    $content['city'][] = $city_id;
//                }
//                $j++;
//            }
            
//            $j = 0;            
//            foreach($datas['device'] as $key => $val){
//                $device_name = trim($val);
//
//                $device_id = $this->Me_model->isAvailable($device_name);
//                $k = $j+2;
//                if($device_id == 0){
//                    $content['errors'][] = "Error - Device name (<b>$device_name</b>) is not matching at <b>Row $k</b>";
//                } else {
//                    $content['devices'][] = $device_id;
//                }
//                $j++;
//            }
            
       
            foreach($datas['installation_base'] as $key => $val){
                $installation_base = trim($val);
                $content['installation_base'][] = $installation_base;
            }
            
            foreach($datas['total_installation_base'] as $key => $val){
                $total_installation_base = trim($val);
                $content['total_installation_base'][] = $total_installation_base;
            }
            
            foreach($datas['U_W_AVG_Repair_Time_12_h'] as $key => $val){
                $U_W_AVG_Repair_Time_12_h = trim($val);
                $content['U_W_AVG_Repair_Time_12_h'][] = $U_W_AVG_Repair_Time_12_h;
            }
 
            foreach($datas['U_W_AVG_Repair_Time'] as $key => $val){
                $U_W_AVG_Repair_Time = trim($val);
                $content['U_W_AVG_Repair_Time'][] = $U_W_AVG_Repair_Time;
            }
            
            foreach($datas['AVG_Repair_Time_12_h'] as $key => $val){
                $AVG_Repair_Time_12_h = trim($val);
                $content['AVG_Repair_Time_12_h'][] = $AVG_Repair_Time_12_h;
            }
            
            foreach($datas['AVG_Repair_Time_All'] as $key => $val){
                $AVG_Repair_Time_All = trim($val);
                $content['AVG_Repair_Time_All'][] = $AVG_Repair_Time_All;
            }
            
            foreach($datas['First_Time_Call'] as $key => $val){
                $First_Time_Call = trim($val);
                $content['First_Time_Call'][] = $First_Time_Call;
            }
            
            foreach($datas['Total_Repair_Calls_All'] as $key => $val){
                $Total_Repair_Calls_All = trim($val);
                $content['Total_Repair_Calls_All'][] = $Total_Repair_Calls_All;
            }
            
            foreach($datas['Up_Time'] as $key => $val){
                $Up_Time = trim($val);
                $content['Up_Time'][] = $Up_Time;
            }
            
            foreach($datas['Doctors_Trained'] as $key => $val){
                $Doctors_Trained = trim($val);
                $content['Doctors_Trained'][] = $Doctors_Trained;
            }
            
            
            foreach($datas['Nurses_Trained'] as $key => $val){
                $Nurses_Trained = trim($val);
                $content['Nurses_Trained'][] = $Nurses_Trained;
            }
            
            foreach($datas['E_Learning_Course_Delivered'] as $key => $val){
                $E_Learning_Course_Delivered = trim($val);
                $content['E_Learning_Course_Delivered'][] = $E_Learning_Course_Delivered;
            }
            
//            foreach($datas as $data){
//                foreach($datas as $key=>$val){
//                    if(isset($datas['country'][$j])){
//                        $country_name = trim($datas['country'][$j]);
//                        $country_id = $this->Country_model->isAvailable($country_name);
//                        $k = $j+2;
//                        if($country_id == 0){
//                            $content['errors'][] = "Error - Country Name (<b>$country_name</b>) is not matching at <b>Row $k</b>";
//                        } else {
//                            $content['country'][] = $country_id;
//                        }
//                        
//                        $city_name = trim($datas['city'][$j]);
//                        $city_id = $this->City_model->isAvailable($city_name);
//                        
//                        if($city_id == 0){
//                            $content['errors'][] = "Error - City name (<b>$city_name</b>) is not matching at <b>Row $k</b>";
//                        } else {
//                            $content['city'][] = $city_id;
//                        }
//                        
//                        $device_name = trim($datas['device'][$j]);
////                        $device = explode("-",$device_name);
////                        $device_name = trim($device[0]);
//                        $device_id = $this->Me_model->isAvailable($device_name);
//                        
//                        if($device_id == 0){
//                            $content['errors'][] = "Error - Device name (<b>$device_name</b>) is not matching at <b>Row $k</b>";
//                        } else {
//                            $content['devices'][] = $device_id;
//                        }
//                       
//                       
//                        $installation_base = trim($datas['installation_base'][$j]);
//                        $content['installation_base'][] = $installation_base;
//                        
//                        $total_installation_base = trim($datas['total_installation_base'][$j]);
//                        $content['total_installation_base'][] = $total_installation_base;
//                        
//                        $U_W_AVG_Repair_Time_12_h = trim($datas['U_W_AVG_Repair_Time_12_h'][$j]);
//                        $content['U_W_AVG_Repair_Time_12_h'][] = $U_W_AVG_Repair_Time_12_h;
//                        
//                        $U_W_AVG_Repair_Time = trim($datas['U_W_AVG_Repair_Time'][$j]);
//                        $content['U_W_AVG_Repair_Time'][] = $U_W_AVG_Repair_Time;
//                        
//                        $AVG_Repair_Time_12_h = trim($datas['AVG_Repair_Time_12_h'][$j]);
//                        $content['AVG_Repair_Time_12_h'][] = $AVG_Repair_Time_12_h;
//                        
//                        $AVG_Repair_Time_All = trim($datas['AVG_Repair_Time_All'][$j]);
//                        $content['AVG_Repair_Time_All'][] = $AVG_Repair_Time_All;
//                        
//                        $First_Time_Call = trim($datas['First_Time_Call'][$j]);
//                        $content['First_Time_Call'][] = $First_Time_Call;
//                        
//                        $Total_Repair_Calls_All = trim($datas['Total_Repair_Calls_All'][$j]);
//                        $content['Total_Repair_Calls_All'][] = $Total_Repair_Calls_All;
//                        
//                        $ppm_service_on_time = trim($datas['ppm_service_on_time'][$j]);
//                        $content['ppm_service_on_time'][] = $ppm_service_on_time;
//                        
//                        $ppm_service_on_time_all = trim($datas['ppm_service_on_time_all'][$j]);
//                        $content['ppm_service_on_time_all'][] = $ppm_service_on_time_all;                        
//                        
//                        $Up_Time = trim($datas['Up_Time'][$j]);
//                        $content['Up_Time'][] = $Up_Time;
//                        
//                        $solutions_under_warranty = trim($datas['solutions_under_warranty'][$j]);
//                        $content['solutions_under_warranty'][] = $solutions_under_warranty;
//                        
//                        $under_service_contracts = trim($datas['under_service_contracts'][$j]);
//                        $content['under_service_contracts'][] = $under_service_contracts;
//                        
//                        $investments = trim($datas['investments'][$j]);
//                        $content['investments'][] = $investments;
//                        
//                        $Doctors_Trained = trim($datas['Doctors_Trained'][$j]);
//                        $content['Doctors_Trained'][] = $Doctors_Trained;
//                        
//                        $Nurses_Trained = trim($datas['Nurses_Trained'][$j]);
//                        $content['Nurses_Trained'][] = $Nurses_Trained;
//                        
//                        $E_Learning_Course_Delivered = trim($datas['E_Learning_Course_Delivered'][$j]);
//                        $content['E_Learning_Course_Delivered'][] = $E_Learning_Course_Delivered;
//                        
//                        $lead_time = trim($datas['lead_time'][$j]);
//                        $content['lead_time'][] = $lead_time;
//                        
//                        $not_estore = trim($datas['not_estore'][$j]);
//                        $content['not_estore'][] = $not_estore;
//                        
//                        $estore = trim($datas['estore'][$j]);
//                        $content['estore'][] = $estore;
//                        
//                        $j++;
//                        
//                    }
//                }
//                
//            }
            
            
            if(empty($content['errors'])){
                unset($content['errors']);
            } else {
                $this->data = array_merge($this->data, $content);
                return $this->load->view('import',$this->data);
            }
            $this->Me_model->insertMapReport($content);
            
             $mapReportCount = $this->Me_model->getMapReportCount();
             $content['datacount'] = $mapReportCount;
             if($mapReportCount > 0){
                $content['datacount_title'] = "Total <b>$mapReportCount Map Report</b> Found.";
             }
             
            $content['success'] = "<b>Map Report Data Imported Successfully</b>";
            $this->Me_model->updateVersionDate();
            $this->data = array_merge($this->data, $content);
            return $this->load->view('import',$this->data);
            
            
        }
        
        
        public function import_device_count(){
            $content =  array();
            $this->load->library('excel');
            $import_type = $this->input->post('import-type');
            $content['datacount'] = 0;
            $content['datacount_title'] = "";
            
            if($import_type = "device-count"){
                $content['title'] = "Import Clinic Report";
                $content['type'] = "device-count";
                $content['sample_file'] = "clinics-device-installation-count-sample.xlsx";
            }
            $file_tmp =$_FILES['excel']['tmp_name'];
            
            $datas = array();
            $errors = array();
            $device = array();

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }


            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $arrayCount = count($allDataInSheet);
            
            $j = 2;
            for($i=2;$i<=$arrayCount;$i++){
                $clinic_code = trim($allDataInSheet[$i]["A"]);
				$clinic_name = trim($allDataInSheet[$i]["B"]);
				$clinic_name = str_replace('"', '', $clinic_name);
				$clinic_name = str_replace("'", "", $clinic_name);
				$clinic_id = $this->Clinics_model->isAvailable($clinic_name, $clinic_code);
                
                if($clinic_id == 0){
                    $content['errors'][] = "Error - Clinic Code (<b>$clinic_code</b>) is not matching at <b>Row $j</b>";
                } else {
                    $content['clinic'][] = $clinic_id;
                }
				
				$device_name = trim($allDataInSheet[$i]["C"]);
                $device_id = $this->Me_model->isAvailable($device_name);

                if($device_id == 0){
                    $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $j</b>";
                } else {
                    $content['devices'][] = $device_id;
                }
				
				$content['quantity'][] = trim($allDataInSheet[$i]["D"]);
				$content['U_W_AVG_Repair_Time_12_h'][] = trim($allDataInSheet[$i]["E"]);
                $content['U_W_AVG_Repair_Time'][] = trim($allDataInSheet[$i]["F"]);
                $content['AVG_Repair_Time_12_h'][] = trim($allDataInSheet[$i]["G"]);
                $content['AVG_Repair_Time_All'][] = trim($allDataInSheet[$i]["H"]);
                $content['First_Time_Call'][] = trim($allDataInSheet[$i]["I"]);
                $content['Total_Repair_Calls_All'][] = trim($allDataInSheet[$i]["J"]);
                $content['Up_Time'][] = trim($allDataInSheet[$i]["K"]);
                $content['Doctors_Trained'][] = trim($allDataInSheet[$i]["L"]);
                $content['Nurses_Trained'][] = trim($allDataInSheet[$i]["M"]);
                $content['E_Learning_Course_Delivered'][] = trim($allDataInSheet[$i]["N"]);
				
				$j++;
            }
            
            if(empty($content['errors'])){
                unset($content['errors']);
            } else {
                $this->data = array_merge($this->data, $content);
                return $this->load->view('import',$this->data);
            }

             $this->Me_model->insertDeviceCount($content);
            
             $deviceCount = $this->Me_model->getDeviceCount();
             $content['datacount'] = $deviceCount;
             if($deviceCount > 0){
                $content['datacount_title'] = "Total <b>$deviceCount Clinic Report</b> Found.";
             }
             
            $content['success'] = "<b>Installed Device Count Data Imported Successfully</b>";
            $this->Me_model->updateVersionDate();
            $this->data = array_merge($this->data, $content);
            return $this->load->view('import',$this->data);
        }
        
        public function import_report_marketing(){
           $content =  array();
            $this->load->library('excel');
            $import_type = $this->input->post('import-type');
            $content['datacount'] = 0;
            $content['datacount_title'] = "";
            
            $all_countries = $this->Country_model->getAllCountries();

            $all_cities = $this->City_model->getAllCities();

            $all_devices = $this->Device_model->getAllDevices();
            
            if($import_type = "map-report"){
             $content['title'] = "Import Map Report Marketing";
             $content['type'] = "report-marketing";
             $content['sample_file'] = "countrywise-marketing-report-data.xlsx";
            }
            $file_tmp =$_FILES['excel']['tmp_name'];
            
            $datas = array();
            $errors = array();
            $device = array();

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }


            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $arrayCount = count($allDataInSheet);
            
            $j = 0;
            for($i=2;$i<=$arrayCount;$i++){
                //$datas['country'][] = trim($allDataInSheet[$i]["A"]);
                $country_name = trim($allDataInSheet[$i]["A"]);
                $country_id = $this->Country_model->isCountryExist($country_name, $all_countries);
                if($country_id == 0){
                        $content['errors'][] = "Error - Country Name (<b>$country_name</b>) is not matching at <b>Row $i</b>";
                } else {
                    $content['country'][] = $country_id;
                }
                
                
                //$datas['device'][] = trim($allDataInSheet[$i]["B"]);
                
                $device_name = trim($allDataInSheet[$i]["B"]);
                $device_id = $this->Device_model->isDeviceExist($device_name, $all_devices);
                if($device_id == 0){
                        $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $i</b>";
                } else {
                    $content['devices'][] = $device_id;
                }
                
                $datas['Microsite_Visitors'][] = trim($allDataInSheet[$i]["C"]);
//                $datas['Locator_Actions'][] = trim($allDataInSheet[$i]["D"]);
//                $datas['Qualified_Leads'][] = trim($allDataInSheet[$i]["E"]);
 
            }
            
//            $j = 0;          
//            foreach($datas['country'] as $key => $val){
//                $country_name = trim($val);
//                $country_id = $this->Country_model->isAvailable($country_name);
//                $k = $j+2;
//                if($country_id == 0){
//                    $content['errors'][] = "Error - Country name (<b>$country_name</b>) is not matching at <b>Row $k</b>";
//                } else {
//                    $content['country'][] = $country_id;
//                }
//                $j++;
//            }
//            
//            $j = 0;          
//            foreach($datas['device'] as $key => $val){
//                $device_name = trim($val);
//                $device_id = $this->Me_model->isAvailable($device_name);
//                $k = $j+2;
//                if($device_id == 0){
//                    $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $k</b>";
//                } else {
//                    $content['devices'][] = $device_id;
//                }
//                $j++;
//            }
            
            foreach($datas['Microsite_Visitors'] as $key => $val){
                $Microsite_Visitors = trim($val);
                $content['Microsite_Visitors'][] = str_replace( ',', '', $Microsite_Visitors);
            }
            
//            foreach($datas['Locator_Actions'] as $key => $val){
//                $Locator_Actions = trim($val);
//                $content['Locator_Actions'][] = str_replace( ',', '', $Locator_Actions);
//            }
//            
//            foreach($datas['Qualified_Leads'] as $key => $val){
//                $Qualified_Leads = trim($val);
//                $content['Qualified_Leads'][] = str_replace( ',', '', $Qualified_Leads);
//            }
            
//            foreach($datas as $data){
//                foreach($datas as $key=>$val){
//                    if(isset($datas['country'][$j])){
//                        $country_name = trim($datas['country'][$j]);
//                        $country_id = $this->Country_model->isAvailable($country_name);
//                        $k = $j+2;
//                        if($country_id == 0){
//                            $content['errors'][] = "Error - Country name (<b>$country_name</b>) is not matching at <b>Row $k</b>";
//                        } else {
//                            $content['country'][] = $country_id;
//                        }
//                        
//                        $device_name = trim($datas['device'][$j]);
////                        $device = explode("-",$device_name);
////                        $device_name = trim($device[0]);
//
//                        $device_id = $this->Me_model->isAvailable($device_name);
//                        
//                        if($device_id == 0){
//                            $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $k</b>";
//                        } else {
//                            $content['devices'][] = $device_id;
//                        }
//                       
//                       
//                        $Microsite_Visitors = trim($datas['Microsite_Visitors'][$j]);
//                        $content['Microsite_Visitors'][] = str_replace( ',', '', $Microsite_Visitors);
//                        
//                        $Locator_Actions = trim($datas['Locator_Actions'][$j]);
//                        $content['Locator_Actions'][] = str_replace( ',', '', $Locator_Actions);
//                        
//                        $Qualified_Leads = trim($datas['Qualified_Leads'][$j]);
//                        $content['Qualified_Leads'][] = str_replace( ',', '', $Qualified_Leads);
//                        
//                        $j++;
//                        
//                    }
//                }
//                
//            }
            
            
            if(empty($content['errors'])){
                unset($content['errors']);
            } else {
                $this->data = array_merge($this->data, $content);
                return $this->load->view('import',$this->data);
            }
            
            $this->Me_model->insertReportMarketing($content);
            
            $reportMarketingCount = $this->Me_model->getReportMarketingCount();
            $content['datacount'] = $reportMarketingCount;
            if($reportMarketingCount > 0){
                $content['datacount_title'] = "Total <b>$reportMarketingCount Map Report Marketing</b> Found.";
            }
             
            $content['success'] = "<b>Map Report Data Imported Successfully</b>";
            $this->Me_model->updateVersionDate();
            $this->data = array_merge($this->data, $content);
            return $this->load->view('import',$this->data);
            
        }
        
            public function cleardata()
        {
                
            $import_type = $this->input->post('import-type');
            if($import_type == "clinics-device"){
                
                $content['title'] = "Import clinics and device installation data";
                $content['type'] = "clinics-device";
                $content['sample_file'] = "clinics-and-devices-installed-sample.xlsx";

                $this->Me_model->clearClinicDeviceInstall();
                
                $clinicDeviceInstallCount = $this->Me_model->getClinicDeviceInstallCount();
                $content['datacount'] = $clinicDeviceInstallCount;
                
                $content['success'] = "<b>Data Cleared Successfully</b>";
                $this->data = array_merge($this->data, $content);               
                return $this->load->view('import',$this->data);

            }
            
            if($import_type == "map-report"){
                
                $content['title'] = "Import Map Report data";
                $content['type'] = "map-report";
                $content['sample_file'] = "map-report-data-sample.xlsx";

                $this->Me_model->clearMapReport();
                
                $mapReportCount = $this->Me_model->getMapReportCount();
                $content['datacount'] = $mapReportCount;
                
                $content['success'] = "<b>Data Cleared Successfully</b>";
                $this->data = array_merge($this->data, $content);               
                return $this->load->view('import',$this->data);

            }
            
            if($import_type == "device-count"){
                
                $content['title'] = "Import Clinic Report";
                $content['type'] = "device-count";
                $content['sample_file'] = "map-report-data-sample.xlsx";

                $this->Me_model->clearDeviceCount();
                
                $deviceCount = $this->Me_model->getDeviceCount();
                $content['datacount'] = $deviceCount;
                
                $content['success'] = "<b>Data Cleared Successfully</b>";
                $this->data = array_merge($this->data, $content);               
                return $this->load->view('import',$this->data);

            }
            
            if($import_type == "report-marketing"){
                
                $content['title'] = "Import Map Report Marketing";
                $content['type'] = "report-marketing";
                $content['sample_file'] = "countrywise-marketing-report-data.xlsx";

                $this->Me_model->clearReportMarketing();
                
                $deviceCount = $this->Me_model->getReportMarketingCount();
                $content['datacount'] = $deviceCount;
                
                $content['success'] = "<b>Data Cleared Successfully</b>";
                $this->data = array_merge($this->data, $content);               
                return $this->load->view('import',$this->data);

            }
            if($import_type == "report-socialmedia"){
                
                $content['title'] = "Import Map Report Social Media";
                $content['type'] = "report-socialmedia";
                $content['sample_file'] = "map-report-socialmedia-data-sample.xlsx";

                $this->Me_model->clearReportSocialMedia();
                
                $deviceCount = $this->Me_model->getReportSocialMediaCount();
                $content['datacount'] = $deviceCount;
                
                $content['success'] = "<b>Data Cleared Successfully</b>";
                $this->data = array_merge($this->data, $content);               
                return $this->load->view('import',$this->data);

            }            
            
        }
        
        function database_sync() {
            $this->load->view('import/database', $this->data);
        }
        
        function database_import_process() {
            $sql = file_get_contents(IMPORT_URL);
            //echo $sql; exit;
            //echo IMPORT_URL; exit;
            $sqls = explode(';', $sql);
            array_pop($sqls);
            if (empty($sqls)) {
                 $this->session->set_flashdata('messageafterimport','Nothing to import');                 
            } else {
                // $this->Me_model->dropTablesExceptUser($this->db->database);
                foreach($sqls as $statement){
                    // $statment = $statement . ";";
                    $statment = $statement;
                    $this->db->query($statement);   
                }
                $this->session->set_flashdata('messageafterimport','Data Imported Sucessfully from Live Server');              
            }

            redirect(base_url('import/database_sync'));
        }
     
        public function import_report_socialmedia(){
            $content =  array();
            $this->load->library('excel');
            $import_type = $this->input->post('import-type');
            $content['datacount'] = 0;
            $content['datacount_title'] = "";

            $all_devices = $this->Device_model->getAllDevices();
            
            if($import_type = "map-report-socialmedia"){
             $content['title'] = "Import Map Report Social Media";
             $content['type'] = "report-socialmedia";
             $content['sample_file'] = "map-report-socialmedia-data.xlsx";
            }
            $file_tmp =$_FILES['excel']['tmp_name'];
            
            $datas = array();
            $errors = array();
            $device = array();

            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }


            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $arrayCount = count($allDataInSheet);
            
            $j = 0;
            for($i=2;$i<=$arrayCount;$i++){
                
                $device_name = trim($allDataInSheet[$i]["A"]);
                $device_id = $this->Device_model->isDeviceExist($device_name, $all_devices);
                if($device_id == 0){
                        $content['errors'][] = "Error - Device Name (<b>$device_name</b>) is not matching at <b>Row $i</b>";
                } else {
                    $content['devices'][] = $device_id;
                }
                
                $datas['facebook_likes'][] = trim($allDataInSheet[$i]["B"]);
                
                $datas['instagram_likes'][] = trim($allDataInSheet[$i]["C"]);
 
            }
            
            foreach($datas['facebook_likes'] as $key => $val){
                $facebook_likes = trim($val);
                $content['facebook_likes'][] = str_replace( ',', '', $facebook_likes);
            }
            
            foreach($datas['instagram_likes'] as $key => $val){
                $instagram_likes = trim($val);
                $content['instagram_likes'][] = str_replace( ',', '', $instagram_likes);
            }   
            
            if(empty($content['errors'])){
                unset($content['errors']);
            } else {
                $this->data = array_merge($this->data, $content);
                return $this->load->view('import',$this->data);
            }
            
            $this->Me_model->insertMapReportSocialMedia($content);
            
            $reportSocialMediaCount = $this->Me_model->getMapReportSocialMediaCount();
            $content['datacount'] = $reportSocialMediaCount;
            if($reportSocialMediaCount > 0){
                $content['datacount_title'] = "Total <b>$reportSocialMediaCount Map Report Social Media</b> Found.";
            }
             
            $content['success'] = "<b>Map Report Social Data Imported Successfully</b>";
            $this->Me_model->updateVersionDate();
            $this->data = array_merge($this->data, $content);
            return $this->load->view('import',$this->data);
            
        }
        	
}
