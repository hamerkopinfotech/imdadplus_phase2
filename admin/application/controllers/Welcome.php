<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user');
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
        
    /*
     * User login
     */
    public function index(){
        $data = array();
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        if($this->input->post('loginSubmit')){
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            // $this->form_validation->set_rules('name', 'Username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            if ($this->form_validation->run() == true) {
                $con['returnType'] = 'single';
                $con['conditions'] = array(
                    'email'=>$this->input->post('email'),
                    // 'name'=>$this->input->post('name'),
                    'password' => md5($this->input->post('password')),
                    'status' => '1'
                );
                $checkLogin = $this->user->getRows($con);
                if($checkLogin){
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('userId',$checkLogin['id']);
                    redirect(base_url('users/account/'));
                }else{
                    $this->session->set_flashdata('message','Wrong email or password, please try again');
                }
            } 
        }
        //load the view
            $this->load->view('users/login', $data);
    }
    
    
    /*
     * User registration
     */
    public function registration(){
        $data = array();
        $userData = array();
        if($this->input->post('regisSubmit')){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[password]');

            $userData = array(
                'name' => strip_tags($this->input->post('name')),
                'email' => strip_tags($this->input->post('email')),
                'password' => md5($this->input->post('password')),
                'gender' => $this->input->post('gender'),
                'phone' => strip_tags($this->input->post('phone'))
            );

            if($this->form_validation->run() == true){
                $insert = $this->user->insert($userData);
                if($insert){
                    $this->session->set_userdata('success_msg', 'Your registration was successfully. Please login to your account.');
                    // redirect('users/login');
                    redirect(base_url(''));
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['user'] = $userData;
        //load the view
        $this->load->view('users/registration', $data);
    }
    
    public function display_doforget()
    {
        $data="";
        $this->load->view('users/forgetpassword',$data);
    }
    
    public function doforget()
    {
        $this->load->helper('url');
        $email= $this->input->post('emailid');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('emailid','emailid','required|trim');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('users/forgetpassword');
        }
        else
        {
            $user= $this->user->getUserData($email);

            if (!empty($user)) {
                $this->load->helper('string');
                $password= random_string('alnum',6);
//                $this->db->where('id', $user['id']);
//                $this->db->update('user',array('password'=>$password,'pass_encryption'=>MD5($password)));
                $this->user->changePassword($user['id'],$password);
                $this->load->library('email');
                /*$config['protocol']    = 'smtp';

                $config['smtp_host']    = 'ssl://smtp.gmail.com';
                // $config['smtp_host']    = 'smtp.gmail.com';
                $config['smtp_port']    = '465';
                // $config['smtp_port']    = '587';

                $config['smtp_timeout'] = '7';

                $config['smtp_user']    = 'hamerkoptest@gmail.com';

                $config['smtp_pass']    = 'htest123#';

                $config['charset']    = 'utf-8';

                $config['newline']    = "\r\n";

                $config['mailtype'] = 'text'; // or html

                $config['validation'] = TRUE; // bool whether to validate email or not      

                $this->email->initialize($config);*/
                
                /*$this->email->initialize(array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'smtp.sendgrid.net',
                  'smtp_user' => 'himdadtest',
                  'smtp_pass' => 'himdadtest@123#',
                  'smtp_port' => 587,
                  'crlf' => "\r\n",
                  'newline' => "\r\n"
                ));
                $this->email->from('hamerkoptest@gmail.com', 'hamerkoptest');
                $this->email->to($user['email']); 	
                $this->email->subject('Password reset');
                $this->email->message('You have requested the new password, Here is you new password:'. $password);	
                $this->email->send();*/
                $this->session->set_flashdata('message','Password has been reset and has been sent to email');		
                redirect(base_url(''));
            } else {
                $this->session->set_flashdata('message','Incorrect email');		
                redirect(base_url('welcome/display_doforget'));              
            }
        }
    }
    
//    public function home()
//    {
//            //$this->load->view('welcome_message');
//       $this->load->view('home');
//    }
//
//    public function import()
//    {
//
//        echo 'sadasda'; exit;
//       $this->load->view('import');
//    }
        
   
    public function database_export_init() {
//    $tableArray = ['tbl_city',
//                   'tbl_clinics',
//                   'tbl_clinic_device',
//                   'tbl_country',
//                   'tbl_devices',
//                   'tbl_device_install',
//                   'tbl_map_report',
//                   'tbl_map_report_marketing',
//                   'tbl_service_hubs',
//                   'tbl_supplier',
//                   'tbl_version'
//                 ];
//      $this->backup_tables(EXPORT_HOST, EXPORT_USERNAME, EXPORT_PASS, EXPORT_DB, $tableArray); 
//      $this->session->set_flashdata('message','Successfully exported database');
//      redirect(base_url('welcome/success_sync'));
        $url = EXPORT_EXTERNAL_URL.'welcome/database_export_process';
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HEADER, TRUE); 
        curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        $head = curl_exec($ch); 
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
        curl_close($ch); 
        $this->session->set_flashdata('message','Successfully exported database');
        redirect(base_url('import/database_sync'));
    }
    
    public function database_export_process() {
      // shell_exec("mysqldump -u root -p imdad_nishad_test > db.sql");
    $tableArray = ['tbl_city',
                   'tbl_clinics',
                   'tbl_clinic_device',
                   'tbl_country',
                   'tbl_devices',
                   'tbl_device_install',
                   'tbl_map_report',
                   'tbl_map_report_marketing',
                   'tbl_service_hubs',
                   'tbl_supplier',
                   'tbl_version'
                 ];
      $this->backup_tables(EXPORT_HOST, EXPORT_USERNAME, EXPORT_PASS, EXPORT_DB, $tableArray); 
      $this->session->set_flashdata('message','Successfully exported database');
      redirect(base_url('welcome/success_sync'));
    }
    /* backup the db OR just a table */
    private function backup_tables($host,$user,$pass,$name,$tables = '*')
    {

            $link = mysqli_connect($host,$user,$pass);
            mysqli_select_db($link,$name);

            //get all of the tables
            if($tables == '*')
            {
                    $tables = array();
                    $result = mysqli_query($link, 'SHOW TABLES');
                    while($row = mysqli_fetch_row($result))
                    {
                            $tables[] = $row[0];
                    }
            }
            else
            {
                    $tables = is_array($tables) ? $tables : explode(',',$tables);
            }
            
            $return = '';
            //cycle through
            foreach($tables as $table)
            {
                    $result = mysqli_query($link, 'SELECT * FROM '.$table);
                    $num_fields = mysqli_num_fields($result);

                    $return.= 'DROP TABLE IF EXISTS '.$table.';';
                    $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE '.$table));
                    $return.= "\n\n".$row2[1].";\n\n";

                    for ($i = 0; $i < $num_fields; $i++) 
                    {
                            while($row = mysqli_fetch_row($result))
                            {
                                    $return.= 'INSERT INTO '.$table.' VALUES(';
                                    for($j=0; $j < $num_fields; $j++) 
                                    {
                                            $row[$j] = addslashes($row[$j]);
                                            $row[$j] = preg_replace("/\n/","\\n",$row[$j]);
                                            if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                            if ($j < ($num_fields-1)) { $return.= ','; }
                                    }
                                    $return.= ");\n";
                            }
                    }
                    $return.="\n\n\n";
            }

            //save file
            $handle = fopen('db.sql','w+');
            fwrite($handle,$return);
            fclose($handle);
    }        
        	
}
