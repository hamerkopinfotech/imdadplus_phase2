<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Management class created by CodexWorld
 */
class MY_Controller extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('isUserLoggedIn')){
            redirect(base_url(''));            
        } else {
            $this->load->model('user');
            $this->load->model('Me_model');
            $data = array();
            $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
            $data['tablecount'] = $this->Me_model->getTableCount($this->db->database);
            $data['is_local'] = IS_LOCAL;
            return $data;
        }
    }
    
}