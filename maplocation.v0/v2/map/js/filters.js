/**
 * Function to filter markers by category
 */


filterMarkersPG = function (categorypg) {

    //set all to view
    for (i = 0; i < gmarkers1.length; i++) {
        items[i][0] = 1;
        marker = gmarkers1[i];
        marker.setVisible(true);
        //window.alert("setting all to yes");
    }

    //import checkbox values
    var PlaygroundCheck = document.getElementById("playground").checked;
    var BabySwingCheck = document.getElementById("babyswing").checked;
    var BeltSwingCheck = document.getElementById("beltswing").checked;
    var HandicapSwingCheck = document.getElementById("handicapswing").checked;
    var ToddlerFriendlyCheck = document.getElementById("toddlerfriendly").checked;
    var RFGroundCheck = document.getElementById("rfground").checked;
    var WCGroundCheck = document.getElementById("wcground").checked;
    var PavillionCheck = document.getElementById("pavillion").checked;
    var GrillCheck = document.getElementById("grill").checked;
    var RampsCheck = document.getElementById("ramps").checked;
    var RestroomsCheck = document.getElementById("restrooms").checked;
    var CloseRestroomsCheck = document.getElementById("closerestrooms").checked;
    var NursingStationCheck = document.getElementById("nursingstation").checked;
    var ChangingTableCheck = document.getElementById("changingtable").checked;
    var StrollerFriendlyCheck = document.getElementById("strollerfriendly").checked;
    var WalkingTrailsCheck = document.getElementById("walkingtrails").checked;

    //test that it still works
    //window.alert(PlaygroundCheck);
    //window.alert(BabySwingCheck);

    //test if playground checkbox is checked
    if (PlaygroundCheck == true) {    

            //for all sites, loop through playground marker
            for (i = 0; i < markers1.length; i++) {  
        
            //if it is still enabled (we dont want to reenable a previously disabled)
            if (items[i][0] == 1){
            
            //get playground binary
            HasPlayground = items[i][1];
            marker = gmarkers1[i];

                        //test if it has a playground
            if (HasPlayground == 1){
                    //set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
                    //set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
            }
        
        }

    } //end of playground check


    //test if babyswing checkbox is checked
    if (BabySwingCheck == true) {    

            //for all sites, loop through babyswing marker
            for (i = 0; i < markers1.length; i++) {  
        
            //if it is still enabled (we dont want to reenable a previously disabled)
            if (items[i][0] == 1){
            
            //get babyswing binary
            HasBabySwing = items[i][2];
            marker = gmarkers1[i];

                        //test if it has a babyswing
            if (HasBabySwing == 1){
                    //set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
                    //set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
            }
        
        }

    } //end of baby swing test


    //test if beltswing checkbox is checked
    if (BeltSwingCheck == true) {    

            //for all sites, loop through babyswing marker
            for (i = 0; i < markers1.length; i++) {  
        
            //if it is still enabled (we dont want to reenable a previously disabled)
            if (items[i][0] == 1){
            
            //get beltswing binary
            HasBeltSwing = items[i][3];
            marker = gmarkers1[i];

                        //test if it has a beltswing
            if (HasBeltSwing == 1){
                    //set the marker to true (probably redundant)
                marker.setVisible(true);
            }
            else {
                    //set the marker to false
                marker.setVisible(false);
                
                //prevent it from being viewed again in the loop
                items[i][0] = 0;
                
            }
            
            }
        
        }

    } //end of belt swing test

}