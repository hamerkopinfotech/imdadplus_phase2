var gmarkers1 = [];
var markers1 = [];
var pointers = [];
var breadcrumb = [];
var infowindow = new google.maps.InfoWindow({content: ''});
var bounds = new google.maps.LatLngBounds();



var iconBase = '/maplocation/img/markers/';
var icons = {
    "country": {
        icon: iconBase + 'marker-blue.png'
    },
    city: {
        icon: iconBase + 'marker-blue.png'
    }
};


// var json = JSON.parse($.getJSON({'url': "data.php", 'async': false}).responseText);
var json = JSON.parse($.getJSON({'url': "/report/", 'async': false}).responseText);



/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};


/**
 * Function to init map
 */

function initialize() {
    var center = new google.maps.LatLng(23.8859, 45.0792);
    var mapOptions = {
        zoom: 6,
        center: center,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDefaultUI: true

    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    load_markers('gcc', 'gcc');
    breadcrumb.push("gcc");
    //fit_bounds_markers(5);
    // map.setZoom(6);
    fit_bounds_markers(6);
    //goback();


}

goback();


function goback() {
    jQuery("#goback").on("click", function () {

        switch (breadcrumb.length) {
            case 3:
                delete_markers();
                console.log("Country, ", breadcrumb[1]);
                load_previous_markers("country", breadcrumb[1]);
                var zoom = 7;
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }
                //map.setZoom(8)
                smoothZoomV2(map, 9, map.getZoom(), false);


                // fit_bounds_markers(zoom);
                breadcrumb.pop();
                break;
            case 2:
                delete_markers();
                load_previous_markers("gcc", "gcc");
                smoothZoomV2(map, 5, map.getZoom(), false);
                //initialize();
                //console.log(json.report);
                jQuery(".mapview-label").html("Middle East");
                FloatingPopup.populate_popup(json.report.alldevice);
                // jQuery("#goback").hide();
                breadcrumb = ["gcc"];
                // console.log("Country, " , breadcrumb[0]);
                // load_previous_markers("gcc","gcc");
                // var zoom = 9;
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }

                // map.setZoom(zoom)


                // breadcrumb.pop();
                break;
            default:
                FloatingPopup.populate_popup(json.report);
                console.log("Nothign to view from this point backward.");
        }
        console.log("Breadcrumbs", breadcrumb);
        check_val();
    });
}

function check_val() {
    var val = $('#mapview').find('span.mapview-label').text();
    console.log(val);
    if (val === 'Middle East') {
        $('#goback').hide();
    } else {
        $('#goback').show();
    }
}

check_val();
initialize();

// FloatingPopup.populate_popup(json.report.alldevice);
setTimeout(function () {
    FloatingPopup.populate_popup(json.report.alldevice);
}, 1000);


jQuery(function ($) {

    var link = $("ul.imdad-tab li a");

// On clicking of the links do something.
    link.on('click', function (e) {
        e.preventDefault();
        var a = $(this).attr("href");
        $(a).slideToggle('fast');
        //$(a).slideToggle('fast');
        $(".imdad-tab-content .tab-pane").not(a).slideUp('fast');

    });

// if ($('input[type=checkbox]').is(':checked')){
// $('#reset').show();
// }else{
// $('#reset').hide();
// }

    $('.form-check-input').on('click', function (e) {
        if ($("input[type='checkbox']:checked").length > 0)
        {
            //$('#reset').show();
            // alert('true');
        } else {
//    $('#reset').hide();
            // alert('false');
        }
    });


});
