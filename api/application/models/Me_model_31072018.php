<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:22 AM
 */
class ME_model extends CI_Model {

    /**
     * Columns that needs to be ignored in reports i.e. while fetching from tbl_map_report
     * @var array
     */
    protected $ignore_report_cols = array('Report_Id', 'Country_Id', 'City_Id', 'Device_Id');

    /**
     * Default report payload values
     * @var array
     */
    protected $default_report_payload = array();

    public function __construct() {

        // set default payload values
        $this->setDefaultReportPayload();
    }

    /**
     * Find the summary report from database
     *
     * @param array $data Summary filter type e.g. array( 'country_id' => 10, 'city_id' => 5, 'device_id' => array(1,2,3) );
     * @return array
     */
    public function getDeviceSummaryReport($data = array()) {

        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;
        $clinic_id = isset($data['clinic_id']) ? (int) $data['clinic_id'] : 0;
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        $where = array();

        if ($country_id > 0) {
            $where[] = 'Country_Id = ' . $country_id;
        }

        if ($city_id) {
            $where[] = 'City_Id = ' . $city_id;
        }

        if (is_array($device_id) && count($device_id) > 0) {
            $where[] = 'Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $clinics = $this->getUniqueClinics($where);

        // set default payload value to 0
        $payload = $this->default_report_payload;

        $payload['clients'] = count($clinics);

        $sql = "SELECT sum(Installation_Base) as Solutions,sum(Total_Installation_Base) as Market_Share,"
                . " sum(U_W_AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_Warranty, sum(U_W_AVG_Repair_Time) as Total_Repair_Calls_Warranty,"
                . " sum(AVG_Repair_Time_12_h) as Avg_Repair_Time_12h_All, sum(AVG_Repair_Time_All) as Total_Repair_Calls_All,"
                . " sum(First_Time_Call) as First_Time_Fix_Rate, sum(Total_Repair_Calls_All) as FTFR_Total_Repair_Calls_All,"
                . " sum(Up_Time) as Up_Time, sum(8760 - Up_Time) as Down_Time, sum(Doctors_Trained) as Doctors_Trained,"
                . " sum(Nurses_Trained) as Nurses_Trained, sum(E_Learning_Course_Delivered) as E_Learning_Course_Delivered"
                . " FROM tbl_map_report $where";
//        echo $sql;
//        die;

        $query = $this->db->query($sql);

        $result = $query->result();

        if (count($result)) {

            foreach ($result as $k => $data) {
//                echo "<pre>";print_r($data);
//                die;

                $payload['solutions'] = $data->Solutions ? $data->Solutions : 0;
                $payload['market_share'] = 0;
                if ($data->Market_Share > 0) {
                    $payload['market_share'] = round(($data->Solutions / $data->Market_Share) * 100);
                }

                $payload['total_repair_calls_warranty'] = $data->Avg_Repair_Time_12h_Warranty ? $data->Avg_Repair_Time_12h_Warranty : 0;

                $payload['avg_repair_time_12h_warranty'] = 0;
                if ($data->Total_Repair_Calls_Warranty > 0) {
                    $payload['avg_repair_time_12h_warranty'] = round(($data->Avg_Repair_Time_12h_Warranty / $data->Total_Repair_Calls_Warranty) * 100);
                }

                $payload['total_repair_calls_all'] = $data->Avg_Repair_Time_12h_All ? $data->Avg_Repair_Time_12h_All : 0;

                $payload['avg_repair_time_12h_all'] = 0;
                if ($data->Total_Repair_Calls_All > 0) {
                    $payload['avg_repair_time_12h_all'] = round(($data->Avg_Repair_Time_12h_All / $data->Total_Repair_Calls_All) * 100);
                }

                $payload['ftfr_total_repair_calls_all'] = $data->First_Time_Fix_Rate ? $data->First_Time_Fix_Rate : 0;

                $payload['first_time_fix_rate'] = 0;
                if ($data->FTFR_Total_Repair_Calls_All > 0) {
                    $payload['first_time_fix_rate'] = round(($data->First_Time_Fix_Rate / $data->FTFR_Total_Repair_Calls_All) * 100);
                }

                $up_pl_down = $data->Up_Time + $data->Down_Time;
                $payload['up_time'] = 0;
                if ($up_pl_down > 0) {
                    $payload['up_time'] = round(( $data->Up_Time / $up_pl_down * 100), 2);
                }

                $payload['doctors_trained'] = $data->Doctors_Trained ? $data->Doctors_Trained : 0;
                $payload['nurses_trained'] = $data->Nurses_Trained ? $data->Nurses_Trained : 0;
                $payload['e_learning_course_delivered'] = $data->E_Learning_Course_Delivered ? $data->E_Learning_Course_Delivered : 0;
            }
        }

        if ($clinic_id > 0) {
            $clinic_sql = "SELECT sum(Quantity) as Solutions FROM tbl_clinic_device WHERE Clinic_Id=$clinic_id";

            $clinic_query = $this->db->query($clinic_sql);

            $clinic_result = $clinic_query->result();

//            echo $clinic_sql . "<br>";
//            echo "<pre>";print_r($clinic_result);die;
            
            $payload['solutions'] = ($clinic_result[0]->Solutions) ? $clinic_result[0]->Solutions : 0;
        }
//        echo "<pre>";print_r( $payload );
//        die;

        return $payload;
    }

    public function getUniqueClinics($where = '') {
        $where = str_replace('City_Id', 'tbl_clinics.City_Id', $where);
        $where = str_replace('Country_Id', 'tbl_clinics.Country_Id', $where);
        $where = str_replace('Device_Id', 'tbl_device_install.Device_Id', $where);
//        $where = str_replace('Clinic_Id', 'tbl_clinics.Clinic_ID', $where);

        $sql = "SELECT count(tbl_clinics.Clinic_ID) as total_clinics FROM tbl_clinics LEFT JOIN tbl_device_install ON tbl_device_install.Clinic_ID = tbl_clinics.Clinic_ID $where GROUP BY tbl_clinics.Clinic_ID";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function getMarketingReport($data = array()) {
        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;

        // can be array fo devices
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        $where = array();
        
        

        if ($country_id > 0) {
            $where[] = 'Country_Id = ' . $country_id;
        }
        if($city_id > 0){
            $where[] = 'City_Id = ' . $city_id;
        }
        

        if (is_array($device_id) && count($device_id) > 0) {
            // $device_where = array();
            // foreach( $device_id as $device ){
            //     $device_where[] = 'Device_Id = '.$device;
            // }
            // $where[] = implode(' AND ', $device_where );

            $where[] = 'Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $sql = "SELECT sum(Microsite_Visitors) as Microsite_Visitors,sum(Locator_Actions) as Locator_Actions, sum(Qualified_Leads) as Qualified_Leads FROM tbl_map_report_marketing $where";

        $query = $this->db->query($sql);

        $result = $query->result();

        $payload = array('microsite_visitors' => 0, 'locator_actions' => 0, 'qualified_leads' => 0);

        if (count($result)) {

            foreach ($result as $k => $data) {

                foreach ($data as $key => $value) {
                    $payload[strtolower($key)] = ($value == null ) ? 0 : $value;
                }
            }
        }

        return $payload;
    }

    public function getDeviceSummaryGroupedReport($data = array(), $group_by = '') {

        if (!$group_by)
            return false;

        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;

        // can be array fo devices
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        if ($country_id > 0) {
            $this->db->where('Country_Id', $country_id);
        }

        if ($city_id) {
            $this->db->where('City_Id', $city_id);
        }

        if (is_array($device_id) && count($device_id) > 0) {
            $this->db->where_in('tbl_map_report.Device_Id', $device_id);
        } elseif ($device_id > 0) {
            $this->db->where('tbl_map_report.Device_Id', $device_id);
        }

        // fetch the device name if being grouped by device id
        $ignore_report_cols = $this->ignore_report_cols;
        if ($group_by == 'Device_Id') {
            $this->db->join('tbl_devices', 'tbl_map_report.Device_Id = tbl_devices.Device_Id');

            // ignore cols
            $ignore_report_cols[] = 'Supplier_Id';
            unset($ignore_report_cols['Device_Id']);
        }

        $query = $this->db->get('tbl_map_report');

        $result = $query->result();

//        print_r( $result );die;

        $payload = array();

        if (count($result)) {

            foreach ($result as $k => $data) {

                foreach ($data as $key => $value) {

                    if (in_array($key, $ignore_report_cols))
                        continue;

                    if ($group_by == 'Device_Id') {
                        if ($key == 'Device_Name') {
                            $payload[$data->{$group_by}][strtolower($key)] = $value;
                        } else {
                            $payload[$data->{$group_by}][strtolower($key)] = isset($payload[$data->{$group_by}][strtolower($key)]) ? $payload[$data->{$group_by}][strtolower($key)] + $value : $value;
                        }
                    } else {
                        $payload[$data->{$group_by}][strtolower($key)] = isset($payload[$data->{$group_by}][strtolower($key)]) ? $payload[$data->{$group_by}][strtolower($key)] + $value : $value;
                    }
                }
            }
        }
//        print_r( $payload );die;

        return $payload;
    }

    public function getDeviceClinicReport($filters) {
        if (isset($filters['devices_id']) && $filters['devices_id'] > 0) {
            $this->db->join('tbl_device_install', 'tbl_device_install.Clinic_Id = tbl_clinics.Clinic_Id');

            if (is_array($filters['devices_id']) && count($filters['devices_id']) > 0) {
                // $device_where = array();
                // foreach( $filters['devices_id'] as $device ){
                //     $this->db->where('tbl_device_install.Device_Id', $device );
                //     // $device_where[] = 'Device_Id = '.$device;
                // }
                // $where[] = implode(' AND ', $device_where );

                $this->db->where_in('tbl_device_install.Device_Id', $filters['devices_id']);
            } elseif ($filters['devices_id'] > 0) {
                $this->db->where('tbl_device_install.Device_Id', $filters['devices_id']);
            }
        }

        if (isset($filters['city_id'])) {
            $this->db->where('tbl_clinics.City_Id', $filters['city_id']);
        }

        $query = $this->db->get('tbl_clinics');

        return $query->result();
    }

    public function getServiceHubReportByCity($city_id = 0) {

        if (!$city_id > 0)
            return array();

        $this->db->where('City_Id', $city_id);

        $query = $this->db->get('tbl_service_hubs');

        return $query->result();
    }

    /**
     * Fetch all suppliers
     * @return mixed
     */
    public function getSuppliers() {
        $query = $this->db->get('tbl_supplier');

        return $query->result();
    }

    /**
     * Get devices by supplier
     *
     * @param int $supplier_id
     * @return array
     */
    public function getDeviceBySupplier($supplier_id = 0) {
        if ($supplier_id < 1)
            return array();

        $this->db->where('Supplier_Id', $supplier_id);

        $query = $this->db->get('tbl_devices');

        return $query->result();
    }

    protected function setIgnoreCols($additional_cols = array()) {
        if (count($additional_cols)) {
            $this->ignore_report_cols = array_merge($this->ignore_report_cols, $additional_cols);
        }
    }

    protected function removeIgnoreCols($cols = array()) {

        if (count($cols)) {
            foreach ($cols as $col) {
                foreach ($this->ignore_report_cols as $key => $value) {
                    if ($value == $col) {
                        unset($this->ignore_report_cols[$key]);
                    }
                }
            }
        }
    }

    /**
     * Set default report payload values
     */
    protected function setDefaultReportPayload() {


        $this->default_report_payload = array(
            'installation_base' => 0,
            'avg_repair_time' => 0,
            'repair_time_24h' => 0,
            'calls_solved_12h' => 0,
            'fix_rate' => 0,
            'spare_parts_hub' => 0,
            'doctors_trained' => 0,
            'nurses_trained' => 0,
            'elearning_courses' => 0,
            'campaigns_running' => 0,
            'microsite_visitors' => 0,
            'clinic_locator_actions' => 0,
            'patient_leads' => 0,
            'device_name' => '',
            'device_id' => 0,
        );
    }
    
    public function getVersion() {
        $sql = "SELECT * FROM `tbl_version` ORDER BY `tbl_version`.`Version_Id` DESC LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();
    }

}
