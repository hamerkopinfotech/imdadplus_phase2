<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 12:46 PM
 */

class Device_model extends CI_Model
{

    public function getAllDevices(){

        $sql = "SELECT d.Device_Id, d.Device_Name FROM tbl_devices AS d";
        $query = $this->db->query( $sql );
		$result = $query->result();
		
		$device_arr = array();
		foreach($result as $val_device) {
			$device_arr[strtolower($val_device->Device_Name)] = $val_device->Device_Id;
		}

        return $device_arr;
    }
   
	public function isDeviceExist($device_name, $all_devices) {
		$device_id = 0;
		$device_name = strtolower($device_name);
        if (array_key_exists($device_name, $all_devices)) {
			$device_id = $all_devices[$device_name];
		}
        
        return $device_id;
        
   }
       public function getAll(){

        $data = array();
        $this->db->select('tbl_devices.Device_Id as deviceid, tbl_devices.Device_Name as devicename, tbl_devices.date as date, tbl_devices.clustermap_date as clusterdate');
        $result = $this->db->get('tbl_devices');
        $data = $result->result_array();
        return $data; 
    }
    
    public function syncUpdate($id){
        
        $date = date('Y-m-d H:i:s');
        $basedata = array( 
            'date' => $date,
         );
      
        $this->db->where('Device_Id', $id);
        $this->db->update('tbl_devices', $basedata);
          //echo $id; exit;
    }
    
    public function syncClusterUpdate($id){
        
        $date = date('Y-m-d H:i:s');
        $basedata = array( 
            'clustermap_date' => $date,
         );
      
        $this->db->where('Device_Id', $id);
        $this->db->update('tbl_devices', $basedata);
          //echo $id; exit;
    }
    
    public function getDeviceId($device_name){
        $data = array();
        $this->db->select('tbl_devices.Device_Id as deviceid, tbl_devices.Device_Name as devicename');
        // $this->db->where('tbl_devices.Device_Name', $device_name);
        $this->db->where("tbl_devices.Device_Name LIKE '%$device_name%'");
        $result = $this->db->get('tbl_devices');
        $data = $result->result_array();
        if (empty($data)) {
            return false;
        }
//        echo '<pre>';
//            echo $device_name;
//            print_r($data);
//        exit;
        return $data[0]['deviceid'];         
    }
}