<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 12:46 PM
 */
class City_model extends CI_Model {

    public function __construct() {
        $this->city_arr = array();
    }

    public function getAll($filters) {

        $where = array();

        // can be array fo devices
        $device_id = isset($filters['devices_id']) ? $filters['devices_id'] : 0;
        $country_id = isset($filters['country_id']) ? (int) $filters['country_id'] : 0;

        if (is_array($device_id) && count($device_id) > 0) {
            $where[] = 'tdi.Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'tdi.Device_Id = ' . $device_id;
        }

        if ($country_id > 0) {
            $where[] = 'tc.Country_Id = ' . $country_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $city_idx = strtolower(str_replace(" ", "_", $where));
        $city_idx = strtolower(str_replace("=", "_", $city_idx)) . "_";
//		echo $city_idx."<br>";

        if (array_key_exists($city_idx, $this->city_arr)) {
            $result = $this->city_arr[$city_idx];
        } else {
            $sql = "SELECT DISTINCT c.City_Id, c.Country_Id, c.City_Name, c.City_Latitude, c.City_Longitude, c.City_Zoom, c.city_centre_lat, c.city_centre_long,
			c.marker_type FROM tbl_city AS c INNER JOIN tbl_clinics AS tc ON tc.City_Id = c.City_Id INNER JOIN tbl_device_install AS tdi ON tdi.Clinic_Id = tc.Clinic_Id $where";

//			echo $sql."<br>";

            $query = $this->db->query($sql);

            $result = $query->result();

            $this->city_arr[$city_idx] = $result;
        }

        return $result;


        /*        if( isset($filters['devices_id']) && $filters['devices_id'] > 0 ){
          $this->db->join('tbl_map_report', 'tbl_map_report.City_Id = tbl_city.City_Id');

          if( is_array( $filters['devices_id']  ) && count( $filters['devices_id']  ) > 0 ){
          // foreach( $filters['devices_id'] as $device ){
          //     $this->db->where('tbl_map_report.Device_Id', $device );
          // }
          $this->db->where_in('tbl_map_report.Device_Id', $filters['devices_id'] );
          }
          elseif( $filters['devices_id']  > 0 ){
          $this->db->where('tbl_map_report.Device_Id', $filters['devices_id'] );
          }

          }

          if( isset($filters['country_id']) && $filters['country_id'] > 0 ){
          $this->db->where('tbl_city.Country_Id', $filters['country_id'] );
          }

          $query = $this->db->get('tbl_city');

          return $query->result(); */
    }
    
    function getCityName($id){
            $data = array();
            $this->db->select('City_Name');
            $this->db->where('City_Id', $id);
            $result = $this->db->get('tbl_city');
            $data = $result->result_array();
            return $data[0]['City_Name']; 
        
    }
    
    function getCityId($city_name, $country_id){
            $data = array();
            $this->db->select('City_Id');
            $this->db->where('City_Name', $city_name);
            $this->db->where('Country_Id', $country_id);
            $result = $this->db->get('tbl_city');
            $data = $result->result_array();
            
           if(count($data) == 0){               
//                $country_capital = $this->getCountryCapital($country_id);
//                $this->db->select('City_Id');
//                $this->db->where('City_Name', $country_capital);
//                $this->db->where('Country_Id', $country_id);
//                $result = $this->db->get('tbl_city');
//                $data = $result->result_array();
                
//                $data = array('City_Id' => $data[0]['City_Id'],
//                              'City_Name' => $country_capital); 
                
                
                $data = array('City_Id' => 0,
                              'City_Name' => $city_name);
               
//               echo 'City name supplied is not matching';
//               exit;
           } else {
               $data = array('City_Id' => $data[0]['City_Id'],
                              'City_Name' => $city_name);
              
           } 
            return $data;
    }    
    public function getCityLatLng($data) {
        $sql = '';
        $details = array();
        if (isset($data['city_name'])) {
            $sql = "SELECT DISTINCT c.City_Id as id, c.Nearest_city_id as Nearest_city_id,c.Country_Id as Country_Id,c.City_Latitude as lat, 
                c.City_Longitude as lng, c.City_Name as name, c.City_Zoom as city_zoom, 
                c.sitecore_zoom as zoom, c.city_centre_lat as center_lat, c.density_point_1_lat as lat1, c.density_point_1_lng as lng1, 
                c.density_point_2_lat as lat2, c.density_point_2_lng as lng2, c.density_point_3_lat as lat3, c.density_point_3_lng as lng3, 
                c.set_high_density as high, c.set_medium_density as medium, c.set_low_density as low, 
		c.city_centre_long as center_lng FROM tbl_city AS c INNER JOIN tbl_country AS tc ON tc.Country_Id = c.Country_Id where c.City_Name = '".$data['city_name']."'";
        }
        
        if ($sql == '') {
            return $details;
        }

        $query = $this->db->query($sql);

        $result = $query->result();


        if (count($result)) {
            foreach ($result as $det) {
                $details = array(
                    'id' => $det->id,
                    'lat' => $det->lat,
                    'lng' => $det->lng,
                    'label' => $det->name,
                    'slug' => slugify($det->name),
                    'city_zoom' => $det->city_zoom,
                    'zoom' => $det->zoom,
                    'lat1' => $det->lat1,
                    'lat2' => $det->lat2,
                    'lat3' => $det->lat3,
                    'lng1' => $det->lng1,
                    'lng2' => $det->lng2,
                    'lng3' => $det->lng3,
                    'high' => $det->high,
                    'medium' => $det->medium,
                    'low' => $det->low,
                    'center_lat' => $det->center_lat,
                    'center_lng' => $det->center_lng
                );
                if (isset($data['city_name']) && isset($data['country_name'])) {
                    $details['Nearest_city_id'] = $det->Nearest_city_id;
                    $details['Country_Id'] = $det->Country_Id;
                }
            }
        }

        return $details;
    } 
 
    function getCountryCapital($countryid){
            $data = array();
            $this->db->select('capital');
            $this->db->where('Country_Id', $countryid);
            $result = $this->db->get('tbl_country');
            $data = $result->result_array();
            return $data[0]['capital']; 
        
    }
    
    public function getCountryLatLng($data) {
        $sql = '';
        $details = array();

            $sql = "SELECT DISTINCT c.Country_Id as id, c.Country_Latitude as lat, c.Country_Longitude as lng, c.Country_Name as name, c.sitecore_zoom as zoom, c.sitecore_lat as center_lat,
		c.sitecore_lng as center_lng FROM tbl_country AS c where c.Country_Name = '".$data['country_name']."'" ;
        
        if ($sql == '') {
            return $details;
        }

        $query = $this->db->query($sql);

        $result = $query->result();

//echo '<pre>'; print_r($result); exit;
        if (count($result)) {
            foreach ($result as $det) {
                $details = array(
                    'id' => $det->id,
                    'lat' => $det->lat,
                    'lng' => $det->lng,
                    'label' => $det->name,
                    'slug' => slugify($det->name),
                    'zoom' => $det->zoom,
                    'center_lat' => $det->center_lat,
                    'center_lng' => $det->center_lng
                );
//                if (isset($data['city_name']) && isset($data['country_name'])) {
//                    $details['Nearest_city_id'] = $det->Nearest_city_id;
//                    $details['Country_Id'] = $det->Country_Id;
//                }
            }
        }

        return $details;
    }
    
        function getCountryId($country_name){
          
            $data = array();
            $this->db->select('Country_Id');
            $this->db->where('Country_Name', $country_name);
            $result = $this->db->get('tbl_country');
            $data = $result->result_array();
            
           if(count($data) == 0){
               return 0;
           } else {
               return $data[0]['Country_Id'];
           }
                     
    } 
    
}
