<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Densitymap extends CI_Controller {

    /**
     * Report filter params
     * @var array
     */
    protected $filter = array();

    public function __construct() {
        parent::__construct();

        if (isset($_GET['device'])) {
            $this->filter['devices_id'] = $_GET['device'];
        }
        if (isset($_GET['country'])) {
            $this->filter['country_id'] = $_GET['country'];
        }
        if (isset($_GET['city'])) {
            $this->filter['city_id'] = $_GET['city'];
        }
        $this->data = array();
        $this->load->helper('url');
    }

    public function index() {
        $dataMarker = array();
        $secretkey = md5('load imdadplus in iframe: '.date('Y-m-d'));
        $getdata = $this->input->get();
        if (empty($getdata['auth_key'])) {
            echo 'Unauthorized Access Restricted';
            exit;
        } else {
            if ($getdata['auth_key'] != $secretkey) {
                echo 'Unauthorized Access Restricted';
                exit;                
            }
        }
        $country_id = 0;  
        if (!empty($getdata['country_name'])) {
            $country_id = $this->City_model->getCountryId($getdata['country_name']);
//            if (!$country_id) {
//                $country_id = 0;          
//            }
            $dataMarker['country_name'] = $getdata['country_name'];
        } 
        
//        else {
//            
//            echo "Country Parameter Missing";
//            exit;
//        }
        
        
        $city_id = 0;
        if (!empty($getdata['city_name'])) {
            $city_data = $this->City_model->getCityId($getdata['city_name'],$country_id);
            $city_id = $city_data['City_Id'];
            $getdata['city_name'] = $city_data['City_Name'];
        }
       // echo $city_id; exit;
//        else {
//            
//            echo "City Parameter Missing";
//            exit;
//            
//        }
        
        
       // exit;
        
        //$data['json_to_display'] = '[["100",24.48868256,45.16705263],["150",24.55207276,54.219787],["125",29.95639876,47.49615419],["50",24.50846801,51.17429137],["25",21.29094899,56.19732606]]';// Ccontry LAT
        //$data['json_to_display'] = '[{"count":"100","lat":"24.48868256","lng":"45.16705263"},{"count":"150","lat":"24.55207276","lng":"54.219787"},{"count":"125","lat":"29.95639876","lng":"47.49615419"},{"count":"50","lat":"24.50846801","lng":"51.17429137"},{"count":"25","lat":"21.29094899","lng":"56.19732606"}]';
       $data['json_to_display'] = '[{"lat":"24.48868256","lng":"45.16705263"},{"lat":"24.364856","lng":"54.540648"},{"lat":"29.629727","lng":"47.852763"},{"lat":"24.50846801","lng":"51.17429137"},{"lat":"21.29094899","lng":"56.19732606"},{"lat":"26.62932895","lng":"50.57232606"},{"lat":"30.52586528","lng":"36.68560731"},{"lat":"33.854721","lng":"35.86228499"}]';        
        $data['center'] = '{lat: 23.8859, lng: 45.0792}';
        $data['zoom'] = 6;
        
        if (isset($getdata['city_name']) && !empty($getdata['city_name'])) {
          $dataMarker['city_name'] = $getdata['city_name'];  
        } else if(isset($getdata['country_name']) && !empty($getdata['country_name'])){
          $dataMarker['country_name'] = $getdata['country_name'];  
        }
		$data['markertype'] = 'others';

        //echo '<pre>'; print_r($dataMarker); exit;
        $markerDetails = array();
        if (!empty($dataMarker)) {
            if(isset($dataMarker['country_name']) && isset($dataMarker['city_name']) && $city_id != 0){
                $markerDetails = $this->City_model->getCityLatLng($dataMarker);
            } else if((isset($dataMarker['country_name']) && $country_id != 0) || (isset($dataMarker['country_name']) && $city_id == 0)){
                $markerDetails = $this->City_model->getCountryLatLng($dataMarker);
            }
            //echo '<pre>'; print_r($markerDetails); exit;
            if (!empty($markerDetails)) {
                //echo '<pre>'; print_r($markerDetails); exit;
                if (isset($dataMarker['city_name']) && isset($dataMarker['country_name']) && $city_id != 0) {
                    $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                    $data['zoom'] = $markerDetails['city_zoom'];
                    //$data['zoom'] = 5;
                    if (isset($dataMarker['city_name'])) {
                        $device_data = $this->Clinicmap($city_id, $country_id);
                        if (!empty($device_data)) {
                           $data['json_to_display'] = json_encode($device_data);
                        } 
                    }
                } else if((isset($dataMarker['country_name']) && $country_id != 0) || (isset($dataMarker['country_name']) && $city_id == 0)){
                    $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                    $data['zoom'] = $markerDetails['zoom'];
                    //$data['zoom'] = 5;
                    if (isset($dataMarker['country_name'])) {
                        $device_data = $this->Clinics_model->getCityDensity($country_id);
                        if (!empty($device_data)) {
                           $data['json_to_display'] = json_encode($device_data);
                        } 
                    }
                    $markerDetails['high'] = "NO";
                    $markerDetails['medium'] = "NO";
                    $markerDetails['low'] = "NO";
                }
				
				if((isset($getdata['city_name']) && !empty($getdata['city_name'])) && (isset($getdata['country_name']) && !empty($getdata['country_name'])) && !empty($device_data)){
					$data['markertype'] = 'clinic';
                                } else if((isset($getdata['city_name']) && !empty($getdata['city_name'])) && (isset($getdata['country_name']) && !empty($getdata['country_name'])) && empty($device_data)){
                                        $data['markertype'] = 'nothing';
                                }
            
            }
        }
        
        //echo '<pre>'; print_r($data); exit;
        $data['lat1'] = (isset($markerDetails['lat1']))?$markerDetails['lat1']:0;
        $data['lng1'] = (isset($markerDetails['lng1']))?$markerDetails['lng1']:0;
        $data['lat2'] = (isset($markerDetails['lat2']))?$markerDetails['lat2']:0;
        $data['lng2'] = (isset($markerDetails['lng2']))?$markerDetails['lng2']:0;
        $data['lat3'] = (isset($markerDetails['lat3']))?$markerDetails['lat3']:0;
        $data['lng3'] = (isset($markerDetails['lng3']))?$markerDetails['lng3']:0;
        $data['high'] = (isset($markerDetails['high']))?$markerDetails['high']:"YES";
        $data['medium'] = (isset($markerDetails['medium']))?$markerDetails['medium']:"YES";
        $data['low'] = (isset($markerDetails['low']))?$markerDetails['low']:"YES";
        
        //echo '<pre>'; print_r($data); exit;
        
            if($_SERVER['HTTP_HOST'] == "imdadplus.com") { 
                $data['host'] =  $_SERVER['HTTP_HOST'];    
            } else if($_SERVER['HTTP_HOST'] == "ewaantech.com") {
                $data['host']  =  $_SERVER['HTTP_HOST'].'/imdadplus';
            }  else if($_SERVER['HTTP_HOST'] == "localhost") {
                $data['host']  =  $_SERVER['HTTP_HOST'].'/imdadplus_phase2';
            }
        
        
        
            $this->data = $data;
            $this->load->view('densitymap/density_map', $this->data); 
        //}
    }
    
    
    
    function Clinicmap($city_id, $country_id){
        if(isset($city_id)){
            $data = $this->Clinics_model->getClinicDensity($city_id, $country_id);
            return $data;
        } else{
            echo 'City Name is Missing';
        }
    }

}
