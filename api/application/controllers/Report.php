<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    /**
     * Report filter params
     * @var array
     */
    protected $filter = array();

    public function __construct() {
        parent::__construct();

        if (isset($_GET['device'])) {
            $this->filter['devices_id'] = $_GET['device'];
        }
    }

    public function index() {
        $total_cities = count($this->City_model->getAll($this->filter));
        $data = array(
            'label' => 'Middle East',
            'city_count' => $total_cities,
            'report' => $this->fetch_me(),
            'markers' => $this->fetch_country_marker(),
            'zoom' => 4,
            'countries' => $this->fetch_country_data(),
            'service_hubs' => $this->fetch_service_hubs(),
            'clinics' => $this->fetch_clinics()
            // 'clinicslist' => $this->fetch_clinics_data()
        );


        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }
    
    public function fetch_clinics_data() {
        $_clinics = array();
        $clinics = $this->Clinics_model->getAll();
        foreach ($clinics as $clinic) {
            $_clinics[$clinic->Clinic_Name] = array(
                'Clinic_Id' => $clinic->Clinic_Id,
                'Clinic_Name' => $clinic->Clinic_Name,
                'Clinic_Name_Slugify' => slugify($clinic->Clinic_Name),
                'City_Name' => slugify($clinic->City_Name),
                'Country_Name' => slugify($clinic->Country_Name)
            );
        }
        
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($_clinics));     
    }

    public function device() {

        $suppliers = $this->Me_model->getSuppliers();

        $data = array();

        if (count($suppliers)) {
            foreach ($suppliers as $supplier) {

                $devices = $this->Me_model->getDeviceBySupplier($supplier->Supplier_Id);
                $_devices = array();

                if (count($devices)) {
                    foreach ($devices as $device) {
                        $_devices[] = array(
                            'id' => $device->Device_Id,
                            'label' => $device->Device_Name,
                            'logo' => $device->device_logo,
                        );
                    }
                }

                $data[] = array(
                    'id' => $supplier->Supplier_Id,
                    'label' => $supplier->Supplier_Name,
                    'logo' => $supplier->supplier_logo,
                    'devices' => $_devices
                );
            }
        }

        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    private function fetch_me() {

        $payload = array();

        $report = $this->Me_model->getDeviceSummaryReport($this->filter);

        $marketing_report = $this->Me_model->getMarketingReport($this->filter);
        
        $socialmedia_report = $this->Me_model->getSocialMediaReport($this->filter);

        $payload['alldevice'] = array_merge($report, $marketing_report, $socialmedia_report);
        return $payload;
    }

    private function fetch_country_marker() {

        return $this->Country_model->getMarkerData($this->filter);
    }

    private function fetch_country_data() {

        $_countries = array();
        $countries = $this->Country_model->getAll($this->filter);

        if (count($countries)) {
            foreach ($countries as $country) {

                // add country id in filter
                $filter = array_merge($this->filter, array('country_id' => $country->Country_Id));

                $report = $this->Me_model->getDeviceSummaryReport($filter);

                $marketing_report = $this->Me_model->getMarketingReport($filter);

                $report = array_merge($report, $marketing_report);
                $total_cities = count($this->City_model->getAll($filter));

                $_countries[slugify($country->Country_Name)] = array(
                    'label' => $country->Country_Name,
                    'zoom' => $country->Country_Zoom,
                    'report' => $report,
                    'center_lat' => $country->country_centre_lat,
                    'center_lng' => $country->country_centre_long,
                    'cities' => $this->fetch_cities($filter),
                    'city_count' => $total_cities,
                );
            }
        }

        return $_countries;
    }

    private function fetch_cities($filter) {

        $_cities = array();
        $cities = $this->City_model->getAll($filter);

        if (count($cities)) {

            foreach ($cities as $city) {
                // add city id in filter
                $filter = array_merge($this->filter, array('city_id' => $city->City_Id));

                $report = $this->Me_model->getDeviceSummaryReport($filter);

                $marketing_report = $this->Me_model->getMarketingReport(array_merge($this->filter, array('country_id' => $city->Country_Id, 'city_id' => $city->City_Id)));

                $report = array_merge($report, $marketing_report);

                $_cities[slugify($city->City_Name)] = array(
                    'label' => $city->City_Name,
                    'zoom' => $city->City_Zoom,
                    'report' => $report,
                    'center_lat' => $city->city_centre_lat,
                    'center_lng' => $city->city_centre_long,
                    'markers' => array(
                        'lat' => $city->City_Latitude,
                        'lng' => $city->City_Longitude,
                        'slug' => slugify($city->City_Name),
                        'type' => 'city',
                        'label' => $city->City_Name,
                        'marker_type'=> $city->marker_type,
                    ),
                    'clinics' => $this->fetch_city_clinics($city)
//                    'data'      => $this->Me_model->getDeviceSummaryGroupedReport( $filter , 'Device_Id')
                );
            }
        }

        return $_cities;
    }

    private function fetch_city_clinics($city) {

        $filter = array_merge($this->filter, array('city_id' => $city->City_Id));

        $marketing_report = $this->Me_model->getMarketingReport(array_merge($this->filter, array('country_id' => $city->Country_Id)));

        $clinics = $this->Me_model->getDeviceClinicReport($filter);
        $_clinics = array();

        if (count($clinics)) {
            foreach ($clinics as $clinic) {

                $filter = array_merge($filter, array('clinic_id' => $clinic->Clinic_Id));
                $report = $this->Me_model->getDeviceSummaryReport($filter);

                $_clinics[slugify($clinic->Clinic_Name)] = array(
                    'label' => $clinic->Clinic_Name,
                    'slug' => slugify($clinic->Clinic_Name),
                    'lat' => $clinic->Clinic_Latitude,
                    'lng' => $clinic->Clinic_Longitude,
                    'report' => array_merge($report, $marketing_report),
                    'type' => 'clinic',
                    'markers' => array(
                        'label' => $clinic->Clinic_Name,
                        'slug' => slugify($clinic->Clinic_Name),
                        'lat' => $clinic->Clinic_Latitude,
                        'lng' => $clinic->Clinic_Longitude,
                        'type' => 'clinic_single',
                    ),
                    'center_lat' => $clinic->Clinic_Latitude,
                    'center_lng' => $clinic->Clinic_Longitude,
                    'zoom' => 13
                );
            }
        }

        return $_clinics;
    }

    private function fetch_service_hubs() {
        $_countries = array();
        $countries = $this->Country_model->getAll();
        if (count($countries)) {
            foreach ($countries as $country) {

                // add country id in filter
                $filter = array('country_id' => $country->Country_Id);

                $cities = $this->City_model->getAll($filter);
                $_cities = array();

                if (count($cities)) {
                    foreach ($cities as $city) {

                        $service_hubs = $this->Me_model->getServiceHubReportByCity($city->City_Id);
                        $_service_hubs = array();
                        if (count($service_hubs)) {
                            foreach ($service_hubs as $service_hub) {
                                $_service_hubs[] = array(
                                    'label' => $service_hub->Service_Hub_Name,
                                    'lat' => $service_hub->Service_Hub_Latitude,
                                    'lng' => $service_hub->Service_Hub_Longitude,
                                    'icon' => $service_hub->Service_Hub_Marker,
                                    'type' => 'service_hub',
                                );
                            }
                        }
                        $_cities[slugify($city->City_Name)] = $_service_hubs;
                    }
                }
                $_countries[slugify($country->Country_Name)] = $_cities;
            }
        }


        return $_countries;
    }

    private function fetch_clinics() {

        $_countries = array();
        $countries = $this->Country_model->getAll();
        if (count($countries)) {
            foreach ($countries as $country) {

                // add country id in filter
                $filter = array('country_id' => $country->Country_Id);

                $cities = $this->City_model->getAll($filter);
                $_cities = array();

                if (count($cities)) {
                    foreach ($cities as $city) {
                        $filter = array_merge($this->filter, array('city_id' => $city->City_Id));
                        $report = $this->Me_model->getDeviceSummaryReport($filter);
                        $clinics = $this->Me_model->getDeviceClinicReport($filter);
                        $_clinics = array();
                        if (count($clinics)) {
                            foreach ($clinics as $clinic) {
                                $_clinics[] = array(
                                    'label' => $clinic->Clinic_Name,
                                    'slug' => slugify($clinic->Clinic_Name),
                                    'report' => $report,
                                    'lat' => $clinic->Clinic_Latitude,
                                    'lng' => $clinic->Clinic_Longitude,
                                    'type' => 'clinic',
                                );
                            }
                        }
                        $_cities[slugify($city->City_Name)] = $_clinics;
                    }
                }
                $_countries[slugify($country->Country_Name)] = $_cities;
            }
        }

        return $_countries;
    }

    public function imdadversion() {
        $result = $this->Me_model->getVersion();

        $data[] = array(
            'version' => $result->Version,
            'updated_date' => $result->Updated_Date
        );

        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

}
