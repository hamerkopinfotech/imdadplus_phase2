<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loadmap extends CI_Controller {

    /**
     * Report filter params
     * @var array
     */
    protected $filter = array();

    public function __construct() {
        parent::__construct();

        if (isset($_GET['device'])) {
            $this->filter['devices_id'] = $_GET['device'];
        }
        $this->data = array();
        $this->load->helper('url');
    }

    public function index() {
        $secretkey = md5('load imdadplus in iframe: '.date('Y-m-d'));
        $getdata = $this->input->get();
        if (empty($getdata['auth_key'])) {
            echo 'Unauthorized Access Restricted';
            exit;
        } else {
            if ($getdata['auth_key'] != $secretkey) {
                echo 'Unauthorized Access Restricted';
                exit;                
            }
        }
        
        $device_id = 0;
        if (!empty($getdata['device_name'])) {
            $device_id = $this->Device_model->getDeviceId($getdata['device_name']);
            if (!$device_id) {
                $device_id = 0;          
            }
        }
        
        
        $data['json_to_display'] = '[["100",24.48868256,45.16705263],["150",24.55207276,54.219787],["125",29.95639876,47.49615419],["50",24.50846801,51.17429137],["25",21.29094899,56.19732606]]';// Ccontry LAT
        
        //$data['center'] = '{lat: 23.8859, lng: 45.0792}';
        //$data['zoom'] = 6;
        $data['center'] = '{lat: 26.343883, lng: 45.148323}';
        $data['zoom'] = 5;
        $dataMarker = array();
        if (isset($getdata['country_name']) && isset($getdata['city_name'])) {
          $dataMarker['country_name'] = $getdata['country_name']; 
          $dataMarker['city_name'] = $getdata['city_name'];  
        } else if (isset($getdata['country_name'])) {
          $dataMarker['country_name'] = $getdata['country_name'];
        }
        $markerDetails = array();
        if (!empty($dataMarker)) {
            $markerDetails = $this->Country_model->getLatLng($dataMarker);
            if (!empty($markerDetails)) {
                    $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                    $data['zoom'] = $markerDetails['zoom'];
                    if (isset($getdata['country_name']) && isset($getdata['city_name'])) {
                        $device_data = $this->Clinicmap($device_id, $markerDetails['id'], $markerDetails['Country_Id']);
                        if (!empty($device_data)) {
                            $data['json_to_display'] = json_encode($device_data);
                        } else {
                            $device_data = array();
                            $dataMarker['city_name'] = $this->City_model->getCityName($markerDetails['Nearest_city_id']);
                            $markerDetails = $this->Country_model->getLatLng($dataMarker);
                            if(!empty($markerDetails)){
                                $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                                $data['zoom'] = $markerDetails['zoom'];
                                $device_data = $this->Clinicmap($device_id, $markerDetails['id'], $markerDetails['Country_Id']);
                            }
                            if (!empty($device_data)) {
                                $data['json_to_display'] = json_encode($device_data);
                            } else {
                                unset($dataMarker['city_name']);
                                $markerDetails = $this->Country_model->getLatLng($dataMarker);
                                $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                                $data['zoom'] = $markerDetails['zoom'];
                                $device_data = $this->Citymap($device_id, $markerDetails['id']);
                                if (!empty($device_data)) {
                                    $data['json_to_display'] = json_encode($device_data);
                                } else {
                                    $data['center'] = '{lat: 26.343883, lng: 45.148323}';
                                    $data['zoom'] = 5;
                                    $device_data = $this->Gccmap($device_id);
                                    $data['json_to_display'] = json_encode($device_data);
                                }
                            }
                        }
                    } else if (isset($getdata['country_name'])) {
                        $device_data = $this->Citymap($device_id, $markerDetails['id']);
                        if (!empty($device_data)) {
                            $data['json_to_display'] = json_encode($device_data);
                        } else {
                            $data['center'] = '{lat: 26.343883, lng: 45.148323}';
                            $data['zoom'] = 5;
                            $device_data = $this->Gccmap($device_id);
                            $data['json_to_display'] = json_encode($device_data);
                        }
                    }
            } else {
               if (isset($dataMarker['country_name']) && isset($dataMarker['city_name'])) {
                   // echo 'City does not exist for the given country or the country does not exist';
                    unset($dataMarker['city_name']);
                    $markerDetails = $this->Country_model->getLatLng($dataMarker);
                        if (!empty($markerDetails)) {
                        $data['center'] = '{lat: '.$markerDetails['center_lat'].', lng: '.$markerDetails['center_lng'].'}';
                        $data['zoom'] = $markerDetails['zoom'];
                        $device_data = $this->Citymap($device_id, $markerDetails['id']);
                        if (!empty($device_data)) {
                            $data['json_to_display'] = json_encode($device_data);
                        } else {
                            $data['center'] = '{lat: 26.343883, lng: 45.148323}';
                            $data['zoom'] = 5;
                            $device_data = $this->Gccmap($device_id);
                            $data['json_to_display'] = json_encode($device_data);
                        }
                    } else {
                        $data['center'] = '{lat: 26.343883, lng: 45.148323}';
                        $data['zoom'] = 5;
                        $device_data = $this->Gccmap($device_id);
                        $data['json_to_display'] = json_encode($device_data);                        
                    }
               } else if (isset($dataMarker['country_name'])) {
                    $data['center'] = '{lat: 26.343883, lng: 45.148323}';
                    $data['zoom'] = 5;
                    $device_data = $this->Gccmap($device_id);
                    $data['json_to_display'] = json_encode($device_data);
               }  else if (isset($dataMarker['device_name'])) {
                   echo 'Device does not exist';
                   exit;
               }  
            }
            
        } else {
            $data['center'] = '{lat: 26.343883, lng: 45.148323}';
            $data['zoom'] = 5;
            $device_data = $this->Gccmap($device_id);
            $data['json_to_display'] = json_encode($device_data);
        }

        $this->data = $data;
        $this->load->view('loadmap/map_new', $this->data); 
    }
    
    function is_url_exist($url){
        $ch = curl_init($url);    
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
           $status = true;
        }else{
          $status = false;
        }
        curl_close($ch);
       return $status;
    }
    
    function Gccmap($device_id){
        if(isset($device_id)){
            $data = $this->Me_model->getGccMapLoad($device_id);
            return $data;
        } else{
            echo 'Device Id Missing';
        }
    }
    
    function Citymap($device_id, $country_id){
        if(isset($device_id)){
            $data = $this->Me_model->getCityMapLoad($device_id,$country_id);
            return $data;
        } else{
            echo 'Device Id Missing';
        }
    }
    
    function Clinicmap($device_id, $city_id, $country_id){
        if(isset($device_id)){
            $data = $this->Me_model->getClinicMapLoad($device_id, $city_id, $country_id);
            return $data;
        } else{
            echo 'Device Id Missing';
        }
    }

}
