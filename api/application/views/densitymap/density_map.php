<!DOCTYPE html>
<html>

<head>
<!--	<link rel="stylesheet" href="style.css">-->
        <style>
            #map_canvas {
                width: 100%;
                height: 100vh;
            }
        </style>

<!--        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&language=ee"></script>-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&libraries=places"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js">
	</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
	<div id="map_canvas"></div>
</body>

<script>
var map;
var markers = <?php echo $json_to_display; ?>;
var markertype = '<?php echo $markertype; ?>';
console.log(markers);


function initializeMaps() {
	// var myLatLng = {lat: -25.363, lng: 131.044};
         var myLatLng = <?php echo $center; ?>;
        var options = {
            center: myLatLng, //Set center.
            zoom: <?php echo $zoom; ?> //The zoom value.
        };
        map = new google.maps.Map(document.getElementById('map_canvas'), options);
        
//        var styles = {
//			default: [
//				  {
//					"featureType": "road",
//					"stylers": [
//					  {
//						"visibility": "off"
//					  }
//					]
//				  }
//				]
//		  };
//      
//		map.setOptions({styles: styles['default']});
  
	var bounds = new google.maps.LatLngBounds();
        var markerimage;
        var size
        $.each( markers, function( key, value ) {
        var icon;

        //alert(iconsize);
        var pos = new google.maps.LatLng(value.lat, value.lng);
        
        if (typeof value.marker_type != "undefined"){
            markerimage = value.marker_type;
            size = 42;
        } else {
            
            if(markertype == 'clinic'){
                markerimage = 'blue';
                size = 28;
            } else if(markertype == 'nothing') {
                markerimage = '';
                size = 42;
            } else {
                markerimage = 'marker-blue';
                size = 42;                
            }
            
        }
//       if (typeof value.count != "undefined"){
//            icon = 'https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-container-bg_4x.png,icons/onion/SHARED-mymaps-container_4x.png,icons/onion/1738-blank-sequence_4x.png&highlight=ff000000,0288D1,ff000000&scale=1.5&color=ffffffff&psize=8&text='+value.count+'&font=fonts/Roboto-Medium.ttf';
//       
//        } else {
            icon = {
                url: 'http://<?php echo $host; ?>/img/markers/'+markerimage+'.png', // url
                scaledSize: new google.maps.Size(size, size), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(size, size) // anchor
            };
//        }


            bounds.extend(pos);

            marker = new google.maps.Marker({
                    position: pos,
                    // draggable:true,
                      map: map,
                      icon: icon
              });    
        });
        var highMarker;
        var mediumMarker;
        var lowMarker;
        var centerOfHighDensity;
        var centerOfMediumDensity;
        var centerOfLowDensity;
        highMarker = "<?php echo $high; ?>";
        mediumMarker = "<?php echo $medium; ?>";
        lowMarker = "<?php echo $low; ?>";
        centerOfHighDensity = {lat: parseFloat(<?php echo $lat1; ?>), lng: parseFloat(<?php echo $lng1; ?>)};
        centerOfMediumDensity = {lat: parseFloat(<?php echo $lat2; ?>), lng: parseFloat(<?php echo $lng2; ?>)};
        centerOfLowDensity = {lat: parseFloat(<?php echo $lat3; ?>), lng: parseFloat(<?php echo $lng3; ?>)};  
        
        if(highMarker == 'YES'){
            var icon_high = {
            url: 'http://<?php echo $host; ?>/img/markers/high_density.png', // url
                //scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                //anchor: new google.maps.Point(54, 54) // anchor
            }

            markers1 = new google.maps.Marker({
                position: centerOfHighDensity,
                map: map,
                icon: icon_high,
                zIndex: 99999,
                //draggable: true //make it draggable
            });
        }
        if(mediumMarker == 'YES'){
            var icon_medium = {
                url: 'http://<?php echo $host; ?>/img/markers/medium_density.png', // url
                //scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                //anchor: new google.maps.Point(54, 54) // anchor
            }   
            markers2 = new google.maps.Marker({
                position: centerOfMediumDensity,
                map: map,
                icon: icon_medium,
                zIndex: 99999,
                //draggable: true //make it draggable
            });
        }
        if(lowMarker == 'YES'){
            var icon_low = {
                url: 'http://<?php echo $host; ?>/img/markers/low_density.png', // url
                //scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                //anchor: new google.maps.Point(54, 54) // anchor
            } 
            markers3 = new google.maps.Marker({
                position: centerOfLowDensity,
                map: map,
                icon: icon_low,
                zIndex: 99999,
                //draggable: true //make it draggable
            });
        }
}

    initializeMaps();


</script>

</html>