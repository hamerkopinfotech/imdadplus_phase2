<!DOCTYPE html>
<html>

<head>
<!--	<link rel="stylesheet" href="style.css">-->
        <style>
            #map_canvas {
                width: 100%;
                height: 100vh;
            }
        </style>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&language=ee"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js">
	</script>

</head>
<?php 
$host = $_SERVER['HTTP_HOST'];

            if (isset($_SERVER['HTTPS']) &&
                ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
              $protocol = 'https://';
            }
            else {
              $protocol = 'http://';
            }
            if($host == 'ewaantech.com'){
                
                $url_starts = $protocol.$host.'/imdadplus';
            } else if($host == 'imdadplus.com'){
                $url_starts = $protocol.$host;
            } else {
                $url_starts = $protocol.$host.'/imdadplus_phase2';
            }?>
<body>
	<div id="map_canvas"></div>
</body>
    <?php   
//    if(gethostname() == 'imdadplus.com') { 
//                $marker_path  =  '/img/markers/blue.png';    
//            } else { 
//                $marker_path  =  '/imdadplus_phase2/img/markers/blue.png';
//            } 
    ?>

<script>
var map;
var markers = <?php echo $json_to_display; ?>;

function initializeMaps() {
	// var myLatLng = {lat: -25.363, lng: 131.044};
         var myLatLng = <?php echo $center; ?>;

        map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: <?php echo $zoom; ?>,
          //mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false,
          disableDoubleClickZoom: true, // <---
          panControl: false,
          streetViewControl: false,
          // gestureHandling: 'none',
          // zoomControl: false,
          center: myLatLng
        });
        
        var styles = {
			default: [
				  {
					//"featureType": "road",
					"stylers": [
					  {
					//	"visibility": "off"
					  }
					]
				  }
				]
		  };
      
		map.setOptions({styles: styles['default']});
  
	var bounds = new google.maps.LatLngBounds();

	markers.forEach(function(point) {
                var icon;
                if (point.length == 3) {
                    var pos = new google.maps.LatLng(point[1], point[2]);
                    icon = 'https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-container-bg_4x.png,icons/onion/SHARED-mymaps-container_4x.png,icons/onion/1738-blank-sequence_4x.png&highlight=ff000000,0288D1,ff000000&scale=1.5&color=ffffffff&psize=8&text='+point[0]+'&font=fonts/Roboto-Medium.ttf';
                }
                
                if (point.length == 2) {
                    var pos = new google.maps.LatLng(point[0], point[1]);
                    // icon = '/imdadplus_phase2/img/markers/blue-old';
                    icon = {
                        url: '<?php echo $url_starts; ?>/img/markers/blue.png', // url
                        scaledSize: new google.maps.Size(28, 28), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(28, 28) // anchor
                    };
                }
                
		
	  	bounds.extend(pos);
	
		marker = new google.maps.Marker({
		  	position: pos,
                        // draggable:true,
			  map: map,
			  icon: icon
		  });
	});
}

initializeMaps();


</script>

</html>