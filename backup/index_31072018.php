<?php
session_start();

include 'config.php';

//if( ( isset($_GET['code']) && !empty($_GET['code']) ) || ( isset($_GET['i']) && !empty($_GET['i']) ) ) {
//    $_SESSION['logged_in'] = true;
//    header("Location: " . SITE_URL);
//    exit;
//}
//
//if( !isset($_SESSION['logged_in']) || $_SESSION['logged_in'] != true ) {
//	header("Location: https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=3MVG9WtWSKUDG.x4eaHETg6unj71r.ongIx.hU3eQyUWriVRxyXae0B4u4yAGOG0hUlSmNDYupDRhdj_NKKfG&redirect_uri=" . SITE_URL . "/");
//	exit;
//}
?>
<html ver="1.0">
    <head>
        <title>Imdad + | Aesthetics Optimised</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/ionicons.min.css" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/odometer-theme-minimal.css" />
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

    </head>
    <body>

        <!-- zoom level: 5 - countries -->

        <?php

        function slugify($text) {
            // replace non letter or digits by -
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);

            // transliterate
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);

            // trim
            $text = trim($text, '-');

            // remove duplicate -
            $text = preg_replace('~-+~', '-', $text);

            // lowercase
            $text = strtolower($text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        }
        ?>

        <div class="googlemap_info">
            <div class="map_body">

                <div class="side-logo">
                    <img src="img/logo.png">
                </div>
                <div class="new-search">
                <h3 class="c_name" id="mapview">
                    <span class="mapview-label">Middle East</span> <span class="city_count" id="city_count"></span>
    <!--                    <span class="filter_bar">
                        <span id="goback" class="filter_btn" style="display: none">
                                <i class="icon ion-arrow-left-c"></i>
                            <img class="irc_mi" src="img/back-icon.png" width="16" height="16" border="0" />
                        </span>
                        <span id="reset" class="filter_btn">
                            <a href="<?php echo SITE_URL; ?>">
                                <img class="irc_mi" src="img/reset-icon.png" width="16" height="16" border="0" />
                            </a>
                        </span>
                        <span id="reset" class="filter_btn">
                            <a href="http://contactme.imdad.com/imdad-dashboard-2018/" target="_blank">
                                <img class="irc_mi" src="img/link-to-icon.png" width="16" height="16" border="0" />
                            </a>
                        </span>
                        </span>-->
                </h3>
                    <form autocomplete="off" action="">
                        <div class="autocomplete">
                            <!-- <input id="myInput" type="text" name="myCountry"> -->
                            <?php $json = json_decode(file_get_contents(SITE_URL . "/api/report/fetch_clinics_data/")); ?>
                            <?php foreach ($json as $clinic): ?>
                            <input name="clinic_<?php echo $clinic->Clinic_Name; ?>" id="clinic_<?php echo $clinic->Clinic_Name; ?>" type="hidden" value="<?php echo $clinic->Clinic_Name_Slugify; ?>">
                            <input name="country_<?php echo $clinic->Clinic_Name; ?>" id="country_<?php echo $clinic->Clinic_Name; ?>" type="hidden" value="<?php echo $clinic->Country_Name; ?>">
                            <input name="city_<?php echo $clinic->Clinic_Name; ?>" id="city_<?php echo $clinic->Clinic_Name; ?>" type="hidden" value="<?php echo $clinic->City_Name; ?>">
                            <?php endforeach; ?>
                            <template id="resultstemplate">
                            <?php foreach ($json as $clinic): ?>
                                <option><?php echo $clinic->Clinic_Name; ?></option>
                            <?php endforeach; ?>
                            </template>
                            <input id="myInput" list="browsers" name="content" autocomplete="off">
                            <datalist id="browsers">
                            </datalist>
                        </div>
                        <a class="reload" href="<?php echo SITE_URL; ?>"></a>
                        <a id="goback" class="search" href="#" style="display:none;"></a>
                        <a class="soc-share" href="http://contactme.imdad.com/imdad-dashboard-2018/" target="_blank"></a>
                    </form>
                </div>


                <div class="information_block">
                    <div id="map-accordion" role="tablist" aria-multiselectable="true" class="map_accordian">
                        <div class="card blue">
                            <a data-toggle="collapse" href="#tabone" aria-expanded="true" aria-controls="tabone" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>EXPERIENCE :  SHARED</h3>
                                    <h4><img src="img/imdad.knowhow.png" alt="imdad knowhow"></h4>
                                </div>
                            </a>

                            <div id="tabone" class="collapse show" role="tabpanel" aria-expanded="true" style="">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item" id="clients">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Clients</h2>
                                                </div>
                                                <p><span id="clients"><span class="odometer">X,XXX</span></span></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item" id="equipment">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Equipment</h2>
                                                </div>
                                                <p>
                                                    <span id="solutions"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item" id="market_share">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Market share</h2>
                                                </div>
                                                <p>
                                                    <span id="market_share"><span>XX</span></span><sup>%</sup>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card orange">
                            <a data-toggle="collapse" href="#tabtwo" aria-expanded="true" aria-controls="tabtwo" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>UP-TIME : MAXIMIZED</h3>
                                    <h4><img src="img/imdad.biomed.png" alt="imdad biomed"></h4>
                                </div>
                            </a>

                            <div id="tabtwo" class="collapse show" role="tabpanel" aria-expanded="true" style="">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Repair time < 12h<br/><small>(in warranty)</small></h2>
                                                </div>
                                                <p>
                                                    <span id="avg_repair_time_12h_warranty"><span class="odometer">XX</span><sup>%</sup></span> / <span id="total_repair_calls_warranty"><span>XX</span></span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Repair time < 12h<br/><small>(in + out warranty)</small></h2>
                                                </div>
                                                <p>
                                                    <span id="avg_repair_time_12h_all"><span>XX</span></span><sup>%</sup> / <span id="total_repair_calls_all"><span>XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>1st time repair</h2>
                                                </div>
                                                <p>
                                                    <span id="first_time_fix_rate"><span class="odometer">XX</span><sup>%</sup></span> / <span id="ftfr_total_repair_calls_all"><span>XX</span> </span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Up-time</h2>
                                                </div>
                                                <p>
                                                    <span id="up_time"><span>XX</span></span><sup>%</sup>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card red">
                            <a data-toggle="collapse" href="#tabthree" aria-expanded="true" aria-controls="tabthree" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>USERS : EMPOWERED</h3>
                                    <h4><img src="img/imdad.clinical.png" alt="imdad clinical"></h4>
                                </div>
                            </a>

                            <div id="tabthree" class="collapse show" role="tabpanel" aria-expanded="true" style="">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Doctors trained</h2>
                                                </div>
                                                <p>
                                                    <span id="doctors_trained"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Nurses trained</h2>
                                                </div>
                                                <p>
                                                    <span id="nurses_trained"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>e-learning courses</h2>
                                                </div>
                                                <p>
                                                    <span id="e_learning_course_delivered"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card light-blue">
                            <a data-toggle="collapse" href="#tabfour" aria-expanded="true" aria-controls="tabfour" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>TREATMENTS : IN DEMAND</h3>
                                    <h4><img src="img/imdad.patients.png" alt="imdad patients"></h4>
                                </div>
                            </a>

                            <div id="tabfour" class="collapse show" role="tabpanel" aria-expanded="true" style="">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Websites visitors</h2>
                                                </div>
                                                <p>
                                                    <span id="microsite_visitors"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Locator actions</h2>
                                                </div>
                                                <p>
                                                    <span id="locator_actions"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                         <div class="col-lg-4 card-item">
                                            <div class="details">
                                                    <div class="tab-title-wrapper">
                                                <h2>Qualified leads</h2>
                                                </div>
                                                <p>
                                                    <span id="qualified_leads"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-title">
                    <h2>technology: carefully selected</h2>
                </div>
                <?php $json = json_decode(file_get_contents(SITE_URL . "/api/report/device/")); ?>
                <?php // print_r($json); exit;?>
                <div class="tab-block">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs imdad-tab">
                        <?php $tabcounter = 0; ?>
                        <?php foreach ($json as $supplier): ?>
                            <?php //print_r($supplier); ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($tabcounter++ == 0) ? "active" : ""; ?>" data-toggle="tab" href="#<?php echo slugify($supplier->label); ?>" >
                                    <img src="img/suppliers/<?php echo $supplier->logo; ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content imdad-tab-content">
                        <?php $tabcounter = 0; ?>
                        <?php foreach ($json as $supplier): ?>
                            <div class="tab-pane <?php echo ($tabcounter++ == 0) ? "active" : ""; ?> container" id="<?php echo slugify($supplier->label); ?>">
                                <div class="list_item_checkbox">
                                    <?php foreach ($supplier->devices as $device): ?>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" id="<?php echo slugify($device->label); ?>" value="<?php echo slugify($device->id); ?>">
                                            <label class="form-check-label" for="<?php echo slugify($device->label); ?>">
                                                <img src="img/devices/<?php echo $device->logo; ?>">
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="tab-block">
                    <div class="cl-version">
                        <?php $json_ver = json_decode(file_get_contents(SITE_URL . "/api/report/imdadversion/")); ?>
                        <?php print $json_ver['0']->version; ?> | <?php echo date('d F Y', strtotime($json_ver['0']->updated_date)); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="map-canvas"></div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&language=ee"></script>
    <!-- <script src="js/odometer.min.js"></script> -->
    <script src="js/popup.js"></script>
    <script src="js/smoothzoom.js"></script>
    <script src="js/markers.js"></script>
    <script src="js/scripts.js"></script>

</body>
</html>